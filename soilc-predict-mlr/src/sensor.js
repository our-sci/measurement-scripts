import {
  app,
  formparser,
  serial
} from '@oursci/scripts';
// import * as coeffs from './coefficients';
// import * as defs from '../.oursci-surveys/survey';
import {
  device_ids
} from './consts';

export default serial; // expose this to android for calling onDataAvailable

// https://app.our-sci.net/#/survey/by-form-id/build_Soil-C-Prediction-and-Recommendation_1568117894
// if in dev environment, take this survey result as example

// const uuidSnippet = 'uuid:ffbe4cb5-0b29-4033-a416-89a5388337c8';
// const form = 'soil-carbon-threshold-recommendations';

// if (app.initSubmittedMock && uuidSnippet && form) {
//   app.initSubmittedMock(form, uuidSnippet);
//   console.log(`fetched survey result for uuid: ${uuidSnippet}`);
// }

const result = {};
result.log = [];
result.error = [];
// const survey = defs.survey();
// hello!
// export const survey = surveyHelper();
export function err(msg) {
  result.error.push(msg);
}

function appendResult(title, content) {
  result.log.push({
    title,
    content,
  });
}

function errorExit(error) {
  app.error();
  throw Error(error);
}

(async () => {
  try {
    // Use `app.getAnswer('field_id')` to retreive a response from the survey by field ID
    // Call `appendResult(title, content)` to add an entry to the results array
    // Call `app.progress(progressVal)` to set the script progress in the app, where progressVal is an integer from 0 to 100
    // `app.getMeta()` can be used to reference the survey's structure (see docs for more info)

    // show spad 632 of scan 1

    // https://gitlab.com/our-sci/resources/blob/master/resources/soil_classes.csv

    // Coefficients for linear regression model

    const linear_intercept = 1.995428;

    const classCoeffs_lin = {
      clay_loam: 0.85979341018798,
      loam: 0.699975954948688,
      loamy_sand: -0.276474430660692,
      sand: -0.421286142387115,
      sandy_clay_loam: 0.116275524681482,
      silty_clay_loam: 1.12658188339897,
      silt_loam: 0.947400061260916,
      sandy_loam: -0.072395458729348,
      silt: 0.947400061260916,
      sandy_clay: 0.699975954948688, // set to Loam, no matching textures in training dataset
      silty_clay: 0.699975954948688, // set to Loam, no matching textures in training dataset
      clay: 0.699975954948688, // set to Loam, no matching textures in training dataset
    };

    const slopeCoeffs_lin = {
      flat: -0.638068163510493,
      gentle: -0.532311687092869,
      hilly: -0.469187146468346,
      moderate: -0.536219076403391,
      rolling: -0.591120692308023,
    };

    const coeffs_lin = {
      median_365: -0.015099291742293,
      median_385: -0.20378368894257,
      median_450: 0.120748645027738,
      median_500: 0.122843613731032,
      median_530: -0.098691245124978,
      median_587: 0.023852250852373,
      median_632: -0.103471482949587,
      median_850: -0.157737243028772,
      median_880: 0.023547986179283,
      median_940: 0.109575238734458,
    };

    // Coefficients for logistic regression model
    const logistic_intercept = 2.06013427;

    const coeffs_log = {
      median_365: 1.309013735,
      median_385: -2.645940566,
      median_450: 1.221296003,
      median_500: 0.629909132,
      median_530: -1.193211581,
      median_587: 0.397218744,
      median_632: -0.769545815,
      median_850: -1.100753824,
      median_880: 0.445581511,
      median_940: 0.571847615,
    };

    const getMeasurementAnswers = (id) => {
      try {
        const a = app.getAnswer(id);
        if (a === '' || a === undefined || a === null || a === {}) {
          return null;
        }
        return JSON.parse(app.getAnswer(id)).data;
      } catch (exception) {
        console.log(exception);
        return null;
      }
    };

    const getTextAnswer = (a) => {
      let v;
      if (typeof app.getAnswer(a) !== 'undefined') {
        v = app.getAnswer(a);
        if (app.getAnswer(a) === '') {}
      } else {
        v = 'undefined';
      }
      result[a] = v;
      return v;
    };

    const soil_texture_1_meas = getMeasurementAnswers('soil_texture_1');
    const soil_texture_2_meas = getMeasurementAnswers(
      'field_2_group/soil_texture_2',
    );
    const soil_texture_3_meas = getMeasurementAnswers(
      'field_3_group/soil_texture_3',
    );
    const field_1_scan = getMeasurementAnswers('field_1_scan');
    const field_2_scan = getMeasurementAnswers('field_2_group/field_2_scan');
    const field_3_scan = getMeasurementAnswers('field_3_group/field_3_scan');

    // So based on white-black calibration adjustments, we need to adjust the numbers up or down

    // pull in all fields and wavelengths into structure expected by processor.js
    // and we're going to apply a correction based on shifts found in the black / white calibration from 2019 to 2020

    const wlList = ['365', '385', '450', '500', '530', '587', '632', '850', '880', '940'];

    // {\"meta\":{\"filename\":\"measurement_2018-07-24T12:05:28.182-04:00.json\",\"scriptID\":\"standard-2-3\",\"date\":\"2018-07-24T12:05:28.188-04:00\"},\"data\":{\"device_battery\":\"94\",\"device_id\":\"00:00:01:cb\",\"device_version\":\"1B\",\"device_firmware\":\"1.24\",\"calibration\":\"0\",\"median_365\":\"-5.05\",\"median_385\":\"-7.79745\",\"median_450\":\"-5.17155\",\"median_500\":\"9.07972\",\"median_530\":\"9.08254\",\"median_587\":\"5.39426\",\"median_632\":\"4.613\",\"median_850\":\"21.1507\",\"median_880\":\"27.9924\",\"median_940\":\"15.4077\",\"three_stdev_370\":\"0.0922229\",\"three_stdev_395\":\"0.0595679\",\"three_stdev_420\":\"0.0408672\",\"three_stdev_530\":\"0.0681172\",\"three_stdev_605\":\"0.0603521\",\"three_stdev_650\":\"0.0905848\",\"three_stdev_730\":\"0.0710851\",\"three_stdev_850\":\"0.188717\",\"three_stdev_880\":\"0.120446\",\"three_stdev_940\":\"0.0940019\",\"spad_370\":\"35.0431\",\"spad_395\":\"17.2908\",\"spad_420\":\"43.3746\",\"spad_530\":\"5.53414\",\"spad_605\":\"11.9059\",\"spad_650\":\"-0.273496\",\"spad_730\":\"0.494751\",\"spad_850\":\"-4.36741\",\"spad_880\":\"-6.53067\",\"spad_940\":\"0\",\"voltage_370\":\"8865.49\",\"voltage_395\":\"20896.5\",\"voltage_420\":\"25835.7\",\"voltage_530\":\"31393.4\",\"voltage_605\":\"28205.1\",\"voltage_650\":\"34193.2\",\"voltage_730\":\"34453\",\"voltage_850\":\"45382.5\",\"voltage_880\":\"44810.9\",\"voltage_940\":\"46406.6\",\"voltage_three_stdev_370\":\"31.3684\",\"voltage_three_stdev_395\":\"32.4547\",\"voltage_three_stdev_420\":\"27.6275\",\"voltage_three_stdev_530\":\"31.4906\",\"voltage_three_stdev_605\":\"33.4266\",\"voltage_three_stdev_650\":\"21.9316\",\"voltage_three_stdev_730\":\"31.0156\",\"voltage_three_stdev_850\":\"35.6491\",\"voltage_three_stdev_880\":\"34.0796\",\"voltage_three_stdev_940\":\"24.4014\",\"temperature\":\"0\",\"humidity\":\"0\",\"pressure\":\"0\",\"voc\":\"0\"}}

    // first, get the device IDs for each field measurement
    const adjData = {
      'field1': {},
      'field2': {},
      'field3': {},
    };
    console.log('field_2_scan');
    console.log(field_2_scan);

    if (field_1_scan && field_1_scan !== '' && field_1_scan !== '') {
      if (Object.keys(device_ids).includes(field_1_scan['device_id'])) {
        adjData.field1 = device_ids[field_1_scan['device_id']];
      } else {
        adjData.field1 = device_ids.undef_id;
      }
    }
    if (field_2_scan && field_2_scan !== '' && field_2_scan !== null) {
      if (Object.keys(device_ids).includes(field_2_scan['device_id'])) {
        adjData.field2 = device_ids[field_2_scan['device_id']];
      } else {
        adjData.field2 = device_ids.undef_id;
      }
    }
    if (field_3_scan && field_3_scan !== '' && field_3_scan !== '') {
      if (Object.keys(device_ids).includes(field_3_scan['device_id'])) {
        adjData.field3 = device_ids[field_3_scan['device_id']];
      } else {
        adjData.field3 = device_ids.undef_id;
      }
    }

    console.log(adjData);

    wlList.forEach((wl) => {
      // calculate adjustment
      let field_1_scan_adj;
      if (field_1_scan) field_1_scan_adj = adjData.field1.m[wl] * field_1_scan[`median_${wl}`] + adjData.field1.b[wl];
      let field_2_scan_adj;
      if (field_2_scan) field_2_scan_adj = adjData.field2.m[wl] * field_2_scan[`median_${wl}`] + adjData.field2.b[wl];
      let field_3_scan_adj;
      if (field_3_scan) field_3_scan_adj = adjData.field3.m[wl] * field_3_scan[`median_${wl}`] + adjData.field3.b[wl];
      // console log to check
      console.log(`${field_1_scan['device_id']}: ${wl} orig: ${field_1_scan[`median_${wl}`]} adj: ${field_1_scan_adj}`)
      if (field_2_scan) console.log(`${field_2_scan['device_id']}: ${wl} orig: ${field_2_scan[`median_${wl}`]} adj: ${field_2_scan_adj}`)
      if (field_3_scan) console.log(`${field_3_scan['device_id']}: ${wl} orig: ${field_3_scan[`median_${wl}`]} adj: ${field_3_scan_adj}`)
      // save to result
      result[`nm${wl}_field1`] = field_1_scan_adj;
      result[`nm${wl}_orig_field1`] = field_1_scan[`median_${wl}`];
      if (field_2_scan) result[`nm${wl}_field2`] = field_2_scan_adj;
      if (field_2_scan) result[`nm${wl}_orig_field2`] = field_2_scan[`median_${wl}`];
      if (field_3_scan) result[`nm${wl}_field3`] = field_3_scan_adj;
      if (field_3_scan) result[`nm${wl}_orig_field3`] = field_3_scan[`median_${wl}`];
    });

    // Field or Section 1 results
    const name_field1 = getTextAnswer('nickname_1');
    console.log('nickname: ', name_field1);
    result.name_field1 = name_field1;

    const striga_field1 = getTextAnswer('striga_1');
    console.log('striga: ', striga_field1);
    result.striga_field1 = striga_field1;

    const slope_field1 = getTextAnswer('slope_1');
    console.log('slope: ', slope_field1);
    result.slope_field1 = slope_field1;

    // Run the model to determine if fertilizer should be applied

    result.logistic_intercept = logistic_intercept;

    const sum_coeff_log_field1 =
      coeffs_log.median_365 * result.nm365_field1 +
      coeffs_log.median_385 * result.nm385_field1 +
      coeffs_log.median_450 * result.nm450_field1 +
      coeffs_log.median_500 * result.nm500_field1 +
      coeffs_log.median_530 * result.nm530_field1 +
      coeffs_log.median_587 * result.nm587_field1 +
      coeffs_log.median_632 * result.nm632_field1 +
      coeffs_log.median_850 * result.nm850_field1 +
      coeffs_log.median_880 * result.nm880_field1 +
      coeffs_log.median_940 * result.nm940_field1 +
      logistic_intercept;

    console.log('Sum of Coefficients: ', sum_coeff_log_field1);

    const prob_field1 =
      Math.exp(sum_coeff_log_field1) / (1 + Math.exp(sum_coeff_log_field1));

    console.log(Math.round(prob_field1, 2));
    // appendResult('Probabilty SOC over 1:', `${prob.toFixed(3)}`);
    result.prob_field1 = prob_field1;

    // Run the model to predict estimated C range

    let slope_lin_field1 = slopeCoeffs_lin[slope_field1];
    if (!slope_field1) {
      // handle if not defined
      slope_lin_field1 = slopeCoeffs_lin.flat;
    }
    console.log('slope lin Coefficient: ', slope_lin_field1);
    // appendResult('Slope Coefficient', `${slope} ${slope_lin.toFixed(4)}`);
    result.slope_lin_field1 = slope_lin_field1;

    // coeffs.classCoeffs[soilCoeffName]; => classcodeSiCL
    let soilClass_lin_field1 = classCoeffs_lin[soil_texture_1_meas['soil_texture_1']];
    if (!soil_texture_1_meas['soil_texture_1']) {
      // handle if not defined
      soilClass_lin_field1 = classCoeffs_lin.loam;
    }
    console.log('Soil lin Coefficient: ', soilClass_lin_field1);

    result.soilClass_lin_field1 = soilClass_lin_field1;

    result.linear_intercept = linear_intercept;
    const estC_field1 =
      coeffs_lin.median_365 * result.nm365_field1 +
      coeffs_lin.median_385 * result.nm385_field1 +
      coeffs_lin.median_450 * result.nm450_field1 +
      coeffs_lin.median_500 * result.nm500_field1 +
      coeffs_lin.median_530 * result.nm530_field1 +
      coeffs_lin.median_587 * result.nm587_field1 +
      coeffs_lin.median_632 * result.nm632_field1 +
      coeffs_lin.median_850 * result.nm850_field1 +
      coeffs_lin.median_880 * result.nm880_field1 +
      coeffs_lin.median_940 * result.nm940_field1 +
      linear_intercept +
      slope_lin_field1 +
      soilClass_lin_field1;

    console.log('Estimated C: ', estC_field1);
    // const minC_field1 = estC_field1 - 0.1;
    // const maxC_field1 = estC_field1 + 0.1;
    // console.log('Soil C range from: ', minC_field1);
    // console.log('to: ', maxC_field1);

    result.estC_field1 = estC_field1;
    // result.minC_field1 = minC_field1;
    // result.maxC_field1 = maxC_field1;

    // Results for Field 2
    // Field or Section 1 results

    const name_field2 = getTextAnswer('field_2_group/nickname_2');
    console.log('nickname: ', name_field2);
    result.name_field2 = name_field2;

    const striga_field2 = getTextAnswer('field_2_group/striga_2');
    console.log('striga: ', striga_field2);
    result.striga_field2 = striga_field2;

    const slope_field2 = getTextAnswer('field_2_group/slope_2');
    console.log('slope: ', slope_field2);
    result.slope_field2 = slope_field2;

    if (soil_texture_2_meas) {

      // Run the model to determine if fertilizer should be applied
      result.logistic_intercept = logistic_intercept;

      const sum_coeff_log_field2 =
        coeffs_log.median_365 * result.nm365_field2 +
        coeffs_log.median_385 * result.nm385_field2 +
        coeffs_log.median_450 * result.nm450_field2 +
        coeffs_log.median_500 * result.nm500_field2 +
        coeffs_log.median_530 * result.nm530_field2 +
        coeffs_log.median_587 * result.nm587_field2 +
        coeffs_log.median_632 * result.nm632_field2 +
        coeffs_log.median_850 * result.nm850_field2 +
        coeffs_log.median_880 * result.nm880_field2 +
        coeffs_log.median_940 * result.nm940_field2 +
        logistic_intercept;

      console.log('Sum of Coefficients: ', sum_coeff_log_field2);

      const prob_field2 =
        Math.exp(sum_coeff_log_field2) / (1 + Math.exp(sum_coeff_log_field2));

      console.log(Math.round(prob_field2, 2));
      result.prob_field2 = prob_field2;

      // Run the model to predict estimated C range

      let slope_lin_field2 = slopeCoeffs_lin[slope_field2];
      if (!slope_field2) {
        // handle if not defined
        slope_lin_field2 = slopeCoeffs_lin.flat;
      }
      console.log('slope lin Coefficient: ', slope_lin_field2);
      result.slope_lin_field2 = slope_lin_field2;

      // coeffs.classCoeffs[soilCoeffName]; => classcodeSiCL
      let soilClass_lin_field2 = classCoeffs_lin[soil_texture_2_meas['soil_texture_2']];
      if (!soil_texture_2_meas['soil_texture_2']) {
        // handle if not defined
        soilClass_lin_field2 = classCoeffs_lin.loam;
      }
      console.log('Soil lin Coefficient: ', soilClass_lin_field2);

      result.soilClass_lin_field2 = soilClass_lin_field2;
      result.linear_intercept = linear_intercept;
      const estC_field2 =
        coeffs_lin.median_365 * result.nm365_field2 +
        coeffs_lin.median_385 * result.nm385_field2 +
        coeffs_lin.median_450 * result.nm450_field2 +
        coeffs_lin.median_500 * result.nm500_field2 +
        coeffs_lin.median_530 * result.nm530_field2 +
        coeffs_lin.median_587 * result.nm587_field2 +
        coeffs_lin.median_632 * result.nm632_field2 +
        coeffs_lin.median_850 * result.nm850_field2 +
        coeffs_lin.median_880 * result.nm880_field2 +
        coeffs_lin.median_940 * result.nm940_field2 +
        linear_intercept +
        slope_lin_field2 +
        soilClass_lin_field2;

      console.log('Estimated C: ', estC_field2);
      // const minC_field2 = estC_field2 - 0.1;
      // const maxC_field2 = estC_field2 + 0.1;
      // console.log('Soil C range from: ', minC_field2);
      // console.log('to: ', maxC_field2);

      result.estC_field2 = estC_field2;
      // result.minC_field2 = minC_field2;
      // result.maxC_field2 = maxC_field2;
    }

    // Results for Field 3
    // Field or Section 1 results

    const name_field3 = getTextAnswer('field_3_group/nickname_3');
    console.log('name: ', name_field3);
    result.name_field3 = name_field3;

    const striga_field3 = getTextAnswer('field_3_group/striga_3');
    console.log('striga: ', striga_field3);
    result.striga_field3 = striga_field3;

    const slope_field3 = getTextAnswer('field_3_group/slope_3');
    console.log(slope_field3);
    result.slope_field3 = slope_field3;

    if (soil_texture_3_meas) {

      // Run the model to determine if fertilizer should be applied
      result.logistic_intercept = logistic_intercept;

      const sum_coeff_log_field3 =
        coeffs_log.median_365 * result.nm365_field3 +
        coeffs_log.median_385 * result.nm385_field3 +
        coeffs_log.median_450 * result.nm450_field3 +
        coeffs_log.median_500 * result.nm500_field3 +
        coeffs_log.median_530 * result.nm530_field3 +
        coeffs_log.median_587 * result.nm587_field3 +
        coeffs_log.median_632 * result.nm632_field3 +
        coeffs_log.median_850 * result.nm850_field3 +
        coeffs_log.median_880 * result.nm880_field3 +
        coeffs_log.median_940 * result.nm940_field3 +
        logistic_intercept;

      console.log('Sum of Coefficients: ', sum_coeff_log_field3);

      const prob_field3 =
        Math.exp(sum_coeff_log_field3) / (1 + Math.exp(sum_coeff_log_field3));

      console.log(Math.round(prob_field3, 2));
      result.prob_field3 = prob_field3;

      // Run the model to predict estimated C range

      let slope_lin_field3 = slopeCoeffs_lin[slope_field3];
      if (!slope_field3) {
        // handle if not defined
        slope_lin_field3 = slopeCoeffs_lin.flat;
      }
      console.log('slope lin Coefficient: ', slope_lin_field3);
      result.slope_lin_field3 = slope_lin_field3;

      // coeffs.classCoeffs[soilCoeffName]; => classcodeSiCL
      let soilClass_lin_field3 = classCoeffs_lin[soil_texture_3_meas['soil_texture_3']];
      if (!soil_texture_3_meas['soil_texture_3']) {
        // handle if not defined
        soilClass_lin_field3 = classCoeffs_lin.loam;
      }
      console.log('Soil lin Coefficient: ', soilClass_lin_field3);

      result.soilClass_lin_field3 = soilClass_lin_field3;
      result.linear_intercept = linear_intercept;
      const estC_field3 =
        coeffs_lin.median_365 * result.nm365_field3 +
        coeffs_lin.median_385 * result.nm385_field3 +
        coeffs_lin.median_450 * result.nm450_field3 +
        coeffs_lin.median_500 * result.nm500_field3 +
        coeffs_lin.median_530 * result.nm530_field3 +
        coeffs_lin.median_587 * result.nm587_field3 +
        coeffs_lin.median_632 * result.nm632_field3 +
        coeffs_lin.median_850 * result.nm850_field3 +
        coeffs_lin.median_880 * result.nm880_field3 +
        coeffs_lin.median_940 * result.nm940_field3 +
        linear_intercept +
        slope_lin_field3 +
        soilClass_lin_field3;

      console.log('Estimated C: ', estC_field3);
      // const minC_field3 = estC_field3 - 0.1;
      // const maxC_field3 = estC_field3 + 0.1;
      // console.log('Soil C range from: ', minC_field3);
      // console.log('to: ', maxC_field3);

      result.estC_field3 = estC_field3;
      // result.minC_field3 = minC_field3;
      // result.maxC_field3 = maxC_field3;
    }

    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.result(result);
  }
})();