// ///////////////////////////////////////////////////////

/*
  info(median)
  info(median_raw)
  info(absorbance)
  info(stdev)
  info(stdev_raw)
  info(three_stdev)
  info(three_stdev)
  info(bits)
  */

// ///////////////////////////////////////////////////////
// Time to check for errors and send warnings if values are too close to max 64435 or zero or too much noise
for (var i = 0; i < median.length; i++) {
  if (median_raw[i] < 100 && calibration != 0) {
    var error_text = `error: LED number ${i} at wavelength ${
      wavelengths[i]
    }is causing a very low response ( ${
      median_raw[i]
    } ) from the detector.  Something is probably wrong with the protocol or the device`;
    error(error_text);
    csv('error', error_text);
  }
  if (median_raw[i] > 64434 && calibration != 0) {
    var error_text = `error: LED number ${i} at wavelength ${
      wavelengths[i]
    }is causing a very high response ( ${
      median_raw[i]
    } ) from the detector.  Something is wrong with the protocol or the device`;
    error(error_text);
    csv('error', error_text);
  }
  if (median[i] < 0 && calibration == 0) {
    var error_text = `warning: LED number ${i} at wavelength ${
      wavelengths[i]
    }is causing a very low response ( ${
      median[i]
    } ) from the detector.  This is either a very transparent sample, or something is probably wrong with the protocol or the device`;
    error(error_text);
    csv('error', error_text);
  }
  if (median[i] > 100 && calibration == 0) {
    var error_text = `warning: LED number ${i} at wavelength ${
      wavelengths[i]
    }is causing a very high response ( ${
      median[i]
    } ) from the detector.  This is either a very shiney sample, or something is wrong with the protocol or the device`;
    error(error_text);
    csv('error', error_text);
  }
  if (three_stdev[i] > 0.15 && calibration == 0) {
    var error_text = `warning: LED number ${i} at wavelength ${
      wavelengths[i]
    }is very noisy, with three standard deviations of ( ${
      three_stdev[i]
    } ).  Stabilize the sample, check for sources of noise, or check the equipment for problems.`;
    warning(error_text);
    csv('warning', error_text);
  }
  if (three_stdev_raw[i] > 70 && calibration == 0) {
    var error_text = `warning: LED number ${i} at wavelength ${
      wavelengths[i]
    }is very noisy, with three standard deviations of ( ${
      three_stdev[i]
    } ).  Stabilize the sample, check for sources of noise, or check the equipment for problems.`;
    warning(error_text);
    csv('warning', error_text);
  }
  /*
      if (bits_actual[i] < 7) {
          var error_text = "warning: LED number " + i + " at wavelength " + wavelengths[i] + "is very noisy, with actual bits less than 8 ( " + bits[i] + " ).  Stabilize the sample, check for sources of noise, or check the equipment for problems."
          warning(error_text)
          csv("warning", error_text)
      }
      */
}

// ///////////////////////////////////////////////////////
// Now we can optionally print graphs...
// for all of the final average values at each wavelength...
const all_outputs = [
  median,
  median_raw,
  absorbance,
  stdev,
  stdev_raw,
  three_stdev,
  three_stdev_raw,
  bits,
]; // removed  bits_actual
const all_outputs_names = [
  'median',
  'median_raw',
  'absorbance',
  'stdev',
  'stdev_raw',
  'three_stdev',
  'three_stdev_raw',
  'bits',
]; // removed "bits_actual"
const all_outputs_cal = [median_raw, stdev_raw, three_stdev_raw, bits];
const all_outputs_cal_names = ['median_raw', 'stdev_raw', 'three_stdev_raw', 'bits'];

// ///////////////////////////////////////////////////////
// save values to CSV
const medianList = [];
if (calibration == 0) {
  for (var i = 0; i < wavelengths.length; i++) {
    csv(`median_${i}`, median[i]);
    medianList.push(MathMore.MathROUND(median[i], 3));
  }
  for (var i = 0; i < wavelengths.length; i++) {
    csv(`median_raw_${i}`, median_raw[i]);
  }
}

// info("median" + ": " + medianList.toString())

if (calibration == 0) {
  for (var i = 0; i < wavelengths.length; i++) {
    csv(`absorbance_${i}`, absorbance[i]);
  }
}
if (calibration == 0) {
  for (var i = 0; i < wavelengths.length; i++) {
    csv(`stdev_${i}`, stdev[i]);
    //        csv("reflectance_range_" + i, max_reflectance[i]);
    //        csv("bits_actual_" + i, bits_actual[i])
  }
}

// DELETE //info("ratios: " + ratios.toString())
// csv("ratios", ratios.toString())

// DELETE
// ///////////////////////////////////////////////////////
// graph the ratios of each median value to each other median value
/*
  if (calibration == 0) {
      var plot = document.createElement("div")
      plot.setAttribute("class", "plot");
      document.body.appendChild(plot)

      var layout = {
          title: "Ratios",
          yaxis: { title: "reflectance" }
      }
      var trace = {
          y: ratios,
          mode: "lines"
      }
      var plotData = [trace]
      Plotly.plot(plot, plotData, layout);
  }
  */

// ///////////////////////////////////////////////////////
// graph the raw trace for the entire measurement (detector response by time)...
const fullTime = [];
for (var i = 0; i < data.length; i++) {
  fullTime[i] = i * pulse_distance;
}
