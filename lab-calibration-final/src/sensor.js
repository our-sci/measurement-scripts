/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import {
  sleep,
} from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');
  //  serial.write("print_memory+");

  const csvResults = JSON.parse(app.getAnswer('cal_model')).data;

  serial.write(csvResults.toDevice);
  console.log(csvResults.toDevice);
  //  serial.write(csvResults.toDevice_heatcal3);
  //  serial.write(csvResults.toDevice_heatcal4);
  //  serial.write(csvResults.toDevice_heatcal5);
  serial.write('print_memory+');

  // ///////////////////////////////////////////////////////
  // Mark the progress of pulling in the data
  let count = 0;
  try {
    app.progress(0.1);
    const result = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1;
      app.progress(0.1 + count / 617.0 * 100.0 * 0.9);
    });
    app.progress(100);

    console.log(result);

    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();