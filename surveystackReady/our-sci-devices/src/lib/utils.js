/**
   * allows you to sleep for a while
   * @param {number} duration time to sleep
   */
export function sleep (duration) {
  return new Promise(resolve =>
    setTimeout(() => {
      resolve()
    }, duration))
};

/**
   * based on survey reference as text, finds the object in the survey
   * @param {string} thing the string reference to the location
   * @param {object} submission the full submission
   */
export function checkSurveyRef (thing, submission) {
  if (thing.includes('submission.')) { // if it contains submission., then get the reference
    //  console.log(location);
    // NOTE!! TO DO: this should use a local reference, otherwise we have to pase the whole submission every time...
    // ALSO! this prevents this chunk of code to be reusable in others people's surveys
    const location = thing.split('.').splice(1)
    console.log(location)
    let finalLocation = JSON.parse(JSON.stringify(submission)) // copy submission into final location
    console.log(finalLocation)
    try {
      console.log('looking for info to send to device in...')
      for (let a = 0; a < location.length; a++) {
        finalLocation = finalLocation[location[a]]
        console.log(location[a])
        // console.log(location[a]);
        // console.log(finalLocation);
      }
    } catch (err) {
      console.error('error finding location from which to get data to send to device')
    }
    console.log('found it...')
    console.log(finalLocation)
    return finalLocation
  } else { // if it doesn't contain submission. just return the original thing
    return thing
  }
}

/*
 * Find a nested item inside a JSON object.
 * Pass the object and the string referencing the location.
 * Returns the object in that location.
 *
 * ### Example
* let submission = { this: { that: { theOther: 55} } }
* dig(submission, 'this.that.theOther'); // returns 55
* dig(submission, 'this.that[theOther]'); // returns 55 (this notation also works!)
* now imagine we are passed the full reference and want to use dig.  Let's use getRoot and removeRoot also -->
* refText = submission.this.that.theother;
* dig(getRoot(refText), removeRoot(refText)); // returns 55
* @param {object} the JSON object your looking in - this is usually 'submission' or 'parent'
* @param {string} the location in the object.
 * robust, can accept null, undefined and will always pass back null
 */
export function dig (object, path, defaultValue = null) {
  const chunks = path
    .split(/\.|\[|\]/)
    .filter(d => d.length !== 0)

  try {
    const value = chunks.reduce((current, chunk) => current[chunk], object)
    if (value !== undefined) {
      return value
    } else {
      return defaultValue
    }
  } catch (e) {
    return defaultValue
  }
};

/*
 * identify object root as referencing parent or the submission
 * return parent or submission objects
 * robust, can accept null, undefined and will always pass back null
 * @param {string} the location in the object.
 */
export function getRoot (path, submission, parent, defaultValue = null) {
  if (typeof path !== 'string') {
    return defaultValue
  } else if ((/^parent+/g).test(path)) {
    return parent
  } else if ((/^submission+/g).test(path)) {
    return submission
  } else { // if neither, assume parent.  add error checking here also
    return parent
  }
}

/*
 * remove the survey root and return the text
 * useful when using dig() function
 * robust, can accept null, undefined and will always pass back null
 * @param {string} the string you want the root removed from.
 */
export function removeRoot (path, defaultValue = null) {
  try {
    path = path.replace(/(^submission\.|^submission|^parent\.|^parent)+/g, '')
    return path
  } catch (e) {
    return defaultValue
  }
}

/*
 * Get and return a value, using previous functions
 * robust, can accept null, undefined and will always pass back null
 * @path {string} the string to get the value from.
 * @submission {object} survey submission object
 * @parent {object} survey parent object
 */
export function get (path, submission, parent, defaultValue = null) {
  try {
    const obj = dig(getRoot(path, submission, parent), removeRoot(path))
    return obj
  } catch (e) {
    return defaultValue
  }
}
