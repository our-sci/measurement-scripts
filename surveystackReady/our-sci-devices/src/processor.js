/* eslint-disable no-inner-declarations */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

// ///////////////////////////////////////////////////////
// Import libraries and get the 'result' information from sensor.js
import {
  sprintf
} from 'sprintf-js'
import app from './lib/app'
import * as ui from './lib/ui'
import * as utils from './lib/utils'
import * as MathMore from './lib/math'
import wavelengthProcessor from './lib/process-wavelengths'

/**
 * flatten works for objects and arrays
 * @param {object} data the object you pass
 */
Object.flatten = function (data) {
  const out = {}

  function recurse (cur, prop) {
    if (Object(cur) !== cur) {
      out[prop] = cur
    } else if (Array.isArray(cur)) {
      for (var i = 0, l = cur.length; i < l; i++)
        { recurse(cur[i], prop + '[' + i + ']') }
      if (l == 0)
        { out[prop] = [] }
    } else {
      let isEmpty = true
      for (const p in cur) {
        isEmpty = false
        recurse(cur[p], prop ? prop + '->' + p : p)
      }
      if (isEmpty && prop)
        { out[prop] = {} }
    }
  }
  recurse(data, '')
  return out
}

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json')
    }
    return JSON.parse(processor.getResult())
  })()

  if (result.error) {
    ui.error('Error', JSON.stringify(result.error))
    app.save()
    return
  }
  // ///////////////////////////////////////////////////////

  // Create variable to store anything you will want to save to the device from this measurement (usually used for calibrations)
  let toDevice = ''

  if (typeof result !== 'object') { // it's not an object, so just print it
    //    console.log('its not an object, so just print it');
    ui.success('Success!', 'No Errors')
    ui.info('Details', `<details><summary>Click to view details</summary>
    <p>
    ${JSON.stringify(result)}      
    </p>
    </details>
      <p>`) //
    app.csvExport('custom_result', JSON.stringify(result))
  } else if (typeof result.sample === 'undefined' || // sample exists
    result.device_name === 'Soil Respiration' || // it's a soil respiration device
    result.device_name === 'MultispeQ' || // it's a soil respiration device
    typeof result.sample[0] !== 'object' // or it just doesn't have the standard 'sample' field: [{}] (so it's not a normal measurement somehow)...
  ) {
    ui.success('Success!', 'No Errors')
    ui.info('Details', `<details><summary>Click to view details (JSON)</summary>
    <p>
    ${JSON.stringify(result)}      
    </p>
    </details>
      <p>`) //
    //    console.log('its an object, but not a measurement - save dot separated values from result');
    const stringResult = JSON.stringify(result)
    app.csvExport('result', stringResult)
    // const flatResult = Object.flatten(result);
    // console.log('flatResult');
    // console.log(flatResult);
    // Object.keys(flatResult).forEach((item) => {
    //   app.csvExport(item, flatResult[item]);
    // });
  } else { // it's an object, has result: [{}] structure, so it's a measurement - ok let's parse it out!
    //    console.log('its an object and a measurement.');

    // ////////////// PRINT BASIC DEVICE INFO ///////////////

    // ///////////////////////////////////////////////////////
    // Device info, but if there are several measurements, just print this once (not every time)

    // Color code the battery info, so the user can plan appropriately and battery status is obvious.
    let device_color = '<battery-ok>'
    let device_color_close = '</battery-ok> OK'
    if (result.device_battery <= 15) {
      device_color = '<battery-low>'
      device_color_close = '</battery-low> Battery low, charge soon!'
    } else if (result.device_battery > 15 && result.device_battery <= 30) {
      device_color = '<battery-med>'
      device_color_close = '</battery-med> OK but consider charging'
    }
    let info = device_color +
    'Battery: ' +
    result.device_battery + '%' +
    device_color_close +
    '<br>Device ID: ' +
    result.device_id +
    '<br>' +
    result.device_name +
    ', V.' +
    result.device_version +
    ', fw V.' +
    result.device_firmware
    // print out the measurement type if it's custom
    if (result?.protocol && ['standardSoil', 'standard5mlcuvette', 'standard1mlcuvette', 'standardCustom'].includes(result.protocol)) {
      info += `'<br>${result.protocol.replace(/standard/g, '')} Normalized Sample`
    }

    ui.info(
      'Device Info',
      info
    )

    if (typeof result.device_battery !== 'undefined') {
      app.csvExport('device_battery', result.device_battery)
    }
    app.csvExport('device_id', result.device_id)
    app.csvExport('device_version', result.device_version)
    app.csvExport('device_firmware', result.device_firmware)

    // /////////////////LOOK THRU MEASUREMENTS/////////////////////////
    // Loop through all of the measurements that were taken (if there were multiple measurements)
    // and calculate the median values for each wavelength
    // if there are > 2 measurements, then add the measurement number to the end of the csvExport name (median_650.0, median_650.1... etc.).  Otherwrise, just report the name (median_650)

    const numProtocols = result.sample.length
    app.csvExport('protocols', numProtocols) // save the total number of protocols (makes it easier if you're calculating calibrations or other stuff and you need to know it)

    for (let j = 0; j < numProtocols; j++) {
      if (numProtocols > 2) {
        // only show which measurement this is if there is more than two, otherwise it's an annoying info box!
        ui.info('Results of measurement ' + (j + 1) + ' of ' + numProtocols, '')
      }

      // for the sake of simplicty, let's rename some common variables we're going to use for each loop...
      const meas = result.sample[j] // full response from device for this protocol j
      let processed = {} // the raw device response processed into median, stdev, etc., populated below by process-wavelengths.js
      const passed = meas.passed // information passed from sensor.js (local environment variables, questions, etc.)
      const calibration = meas.calibration // infomation passed from device about calibration status of this measurement.
      let voltage = null
      if (meas.voltage_raw) {
        voltage = meas.voltage_raw // only show if voltage is outputted by device, used to check heat calibration
      }
      const data = meas.data_raw // the detector response only from the device
      if (typeof data[0] !== 'undefined') { // if there is data_raw to be processed and shown, show it... otherwise skip to the end)
        // Determine what the wavelengths for each pulse set are and add it to the result.
        let wavelengths = []
        let detectors = []
        let master_recall = []
        let master_wavelengths = []
        let master_detectors = []
        if (
          calibration === 1 || // Cuvette Standard calibration
          calibration === 2 || // open blank user calibration
          calibration === 3 || // blank calibration saving to EEPROM memory on device
          calibration === 4 || // Local Cuvette Standard calibration
          calibration === 5 // Local Cuvette Standard check
        ) {
          wavelengths = [
            850,
            880,
            365,
            385,
            450,
            530,
            587,
            632,
            500,
            940,
            850,
            880,
            365,
            385,
            450,
            530,
            587,
            632,
            500,
            940
          ]
          detectors = [
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_'
          ] //          wavelengths = [365, 365, 365, 365, 385, 385, 385, 385, 450, 450, 450, 450, 500, 500, 500, 500, 530, 530, 530, 530, 587, 587, 587, 587, 632, 632, 632, 632, 850, 850, 850, 850, 880, 880, 880, 880, 940, 940, 940, 940];
          // master_detectors = [null, '_0', '_0', '_1', '_1', '_1', '_1', '_1', '_1', '_1', '_0']; // what are the main detectors used
          // master_recall = [null, 'ir_high_master', 'ir_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'ir_high_master']
          // master_wavelengths = [null, 850, 880, 365, 385, 450, 530, 587, 632, 500, 940];
        } else if (calibration === 7) {
          wavelengths = [365, 365, 365, 365, 385, 385, 385, 385, 450, 450, 450, 450, 500, 500, 500, 500, 530, 530, 530, 530, 587, 587, 587, 587, 632, 632, 632, 632, 850, 850, 850, 850, 880, 880, 880, 880, 940, 940, 940, 940]
          detectors = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
        } else {
          // includes if it's undefined
          wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940]
          detectors = ['', '', '', '', '', '', '', '', '', '']
          master_detectors = [null, '_0', '_0', '_1', '_1', '_1', '_1', '_1', '_1', '_1', '_0'] // what are the main detectors used
          master_recall = [null, 'ir_high_master', 'ir_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'vis_high_master', 'ir_high_master']
          master_wavelengths = [null, 850, 880, 365, 385, 450, 530, 587, 632, 500, 940]
        }

        // ///////////////////////////////////////////////////////
        // Process the results - clean up outliers, straighten due to heating, and produce medians, etc. for each wavelength and see if it's a calibration script
        processed = wavelengthProcessor(meas, wavelengths, detectors)

        // enter highLowBlank '_high', '_low', '_blank';
        function toDeviceCal (highLowBlank) {
          let master = ''
          let saveText = ''
          const calVals = {
            _ir: {
              save: [1, 2, 10]
            },
            _vis: {
              save: [3, 4, 5, 6, 7, 8, 9]
            }
          }
          const irSaves = [1, 2, 10] // LEDs to save NIR calibrations (NIR LEDs only)
          const visSaves = [3, 4, 5, 6, 7, 8, 9] // LEDs to save VIS calibrations (VIS LEDs only)
          if (calibration === 4) master = '_master'
          Object.keys(calVals).forEach((range) => {
            saveText += `set${range}${highLowBlank}${master}+` // eg set_ir_high_master+ or set_ir_low+...
            calVals[range].save.forEach((led) => {
              saveText += led + '+'
              const add = (range === '_vis') ? 9 : -1 // vis detector is the first 10 values, ir detector is the second 10 values
              saveText += processed.median[led + add] + '+'
            })
            saveText += '-1+' // close out the command
          })
          return `${saveText}print_memory+` // eg set_ir_high+0+0+1+25.25....+print_memory+
        }

        // ///////////////////////////////////////////////////////
        // If it's a calibration script, figure out which and save data back to device accordingly
        // Assemble the data to save to device and save back to device
        if (calibration === 1 || calibration === 4) {
          toDevice += toDeviceCal('_high')
        } else if (calibration === 2) {
          toDevice += toDeviceCal('_low')
        } else if (calibration === 3) {
          toDevice += toDeviceCal('_blank')
        }

        // if (calibration === 1 || calibration === 4) { // calibration 4 saves the calibrated values to vis/ir_high_master (10,100, 9,900, etc.).  Calibration 1 saves raw values to vis/ir_high (55000000, 532626116, etc)
        //   let set_ir_high = 'set_ir_high+'
        //   if (calibration === 4) set_ir_high = 'set_ir_high_master+';
        //   for (let i = 0; i < 10; i++) {
        //     set_ir_high += i + 1 + '+'
        //     set_ir_high += processed.median[i] + '+'
        //   }
        //   set_ir_high += -1 + '+'
        //   let set_vis_high = 'set_vis_high+'
        //   if (calibration === 4) set_vis_high = 'set_vis_high_master+';
        //   for (let i = 0; i < 10; i++) {
        //     set_vis_high += i + 1 + '+'
        //     set_vis_high += processed.median[i + 10] + '+'
        //   }
        //   set_vis_high += -1 + '+'
        //   console.log(set_ir_high)
        //   console.log(set_vis_high)
        //   // Send the data to the device in the next measurement
        //   toDevice += set_ir_high + set_vis_high + 'print_memory+'
        // } else if (calibration === 2) {
        //   // Assemble the data to save to device and save back to device
        //   let set_ir_low = 'set_ir_low+'
        //   for (let i = 0; i < 10; i++) {
        //     set_ir_low += i + 1 + '+'
        //     set_ir_low += processed.median[i] + '+'
        //   }
        //   set_ir_low += -1 + '+'
        //   let set_vis_low = 'set_vis_low+'
        //   for (let i = 0; i < 10; i++) {
        //     set_vis_low += i + 1 + '+'
        //     set_vis_low += processed.median[i + 10] + '+'
        //   }
        //   set_vis_low += -1 + '+'
        //   console.log(set_ir_low)
        //   console.log(set_vis_low)
        //   // Send the data to the device
        //   toDevice += set_ir_low + set_vis_low + 'print_memory+';
        //   // Also identify the QR code which contains the card's master values, and save them to the device
        //   // toDevice += passed.cardQr + 'print_memory+'
        // } else if (calibration === 3) {
        //   // blank used for cuvettes
        //   // Assemble the data to save to device and save back to device
        //   let set_ir_blank = 'set_ir_blank+'
        //   for (let i = 0; i < 10; i++) {
        //     set_ir_blank += i + 1 + '+'
        //     set_ir_blank += processed.median[i] + '+'
        //   }
        //   set_ir_blank += -1 + '+'
        //   let set_vis_blank = 'set_vis_blank+'
        //   for (let i = 0; i < 10; i++) {
        //     set_vis_blank += i + 1 + '+'
        //     set_vis_blank += processed.median[i + 10] + '+'
        //   }
        //   set_vis_blank += -1 + '+'
        //   console.log(set_ir_blank)
        //   console.log(set_vis_blank)
        //   toDevice += set_ir_blank + set_vis_blank + 'print_memory+'
        // }

        console.log(JSON.stringify('Processed data: '))
        console.log(JSON.stringify(processed))
        console.log(JSON.stringify('Passed data from sensor.js: '))
        console.log(JSON.stringify(passed))
        console.log(JSON.stringify('Calibration status sensor.js: '))
        console.log(JSON.stringify(calibration))

        /*
        for (let k = 0; k < wavelengths.length; k++) {
          ui.info('sample values flat', processed.sample_values_flat[k]);
          ui.info('sample values perc', processed.sample_values_perc[k]);
          ui.info('sample values flat', processed.sample_values_flat[k]);
          ui.info('sample values raw', processed.sample_values_raw[k]);
          ui.info('sample values voltage', processed.sample_voltage_raw[k]);
          ui.info('sample values adjustments', processed.sample_values_adjustments[k]);

          ui.info('sample values perc', JSON.parse(processed.sample_values_perc[k]));
          ui.info('sample values raw', JSON.parse(processed.sample_values_raw[k]));
          ui.info('sample values voltage', JSON.parse(processed.sample_voltage_raw[k]));
          ui.info('sample values adjustments', JSON.parse(processed.sample_values_adjustments[k]));

          ui.info('sample values slope', JSON.stringify(processed.sample_values_slope[k]));
          ui.info('sample values yint', JSON.stringify(processed.sample_values_yint[k]));
          ui.info('sample values center', JSON.stringify(processed.sample_values_center[k]));
          ui.info('sample values time', JSON.stringify(processed.sample_values_time[k]));
        }
          */

        // ///////////////////////////////////////////////////////
        // Output the data, save the CSV

        // ///////////////////////////////////////////////////////
        // Error checking
        // Make sure this is the right device for this protocol.  Save errors to the warnings array to be displayed and saved later
        if (typeof result.device_name === 'undefined') {
          const warningText =
            'device_name is expected from the device but I dont see it in the output result.  Check the protocol and ensure it is requested'
          processed.warnings.push(warningText)
        }
        if (result.device_name.toString() !== 'Reflectometer') {
          const warningText =
            'This script was intended to be used on the reflectometer!  This device is a ' +
            result.device_name.toString()
          processed.warnings.push(warningText)
        }
        if (typeof result.device_version === 'undefined') {
          const warningText =
            'device_version is expected from the device but I dont see it in the output result.  Check the protocol and ensure it is requested'
          processed.warnings.push(warningText)
        }
        if (typeof result.device_id === 'undefined') {
          const warningText =
            'device_id is expected from the device but I dont see it in the output result.  This device may have been modified from its original version or is not built by Our Sci or something is wrong.  If using Our Sci firmware use the set_device_info+<deviceID>+ command to assigned ID'
          processed.warnings.push(warningText)
        }
        if (typeof result.device_firmware === 'undefined') {
          const warningText =
            'device_firmware is expected from the device but I dont see it in the output result.  This device may have been modified from its original version or is not built by Our Sci or something is wrong.'
          processed.warnings.push(warningText)
        }

        // Print all errors and info statements's up to this point and save them to the database
        if (typeof processed.warnings[0] !== 'undefined') {
          for (let i = 0; i < processed.warnings.length; i++) {
            ui.warning('Warning', processed.warnings[i])
            if (numProtocols <= 2) {
              app.csvExport('warnings', processed.warnings.join('/n'))
            } else {
              app.csvExport('warnings_' + j, processed.warnings.join('/n'))
            }
          }
        }

        if (typeof processed.info[0] !== 'undefined') {
          for (let i = 0; i < processed.info.length; i++) {
            ui.info('Info', processed.info[i])
            if (numProtocols <= 2) {
              app.csvExport('info' + j, processed.info.join('/n'))
            } else {
              app.csvExport('info_' + j, processed.info.join('/n'))
            }
          }
        }
        // ///////////////////////////////////////////////////////
        result.testing = {}

        // ///////////////////////////////////////////////////////
        // identify calibration type for this measurement (if any), and save to the database
        if (calibration === 1) {
          ui.info('Local Cuvette Standard calibration', 'Recalibrate to your Local Cuvette Standard')
        } else if (calibration === 2) {
          ui.info('Open Blank calibration', '')
        } else if (calibration === 3) {
          ui.info('Custom Blank calibration', '')
        } else if (calibration === 4) {
          ui.info('Local Cuvette Standard', 'Determine the values for this Local Cuvette Standard')
        } else if (calibration === 5) {
          ui.info('Local Cuvette Standard Check', '')
        } else if (calibration === 8) {
          ui.info('Standard Check', 'This checks this measurement against your stored local calibration and tells you how far off they are')
        }
        if (typeof calibration !== 'undefined') {
          app.csvExport('calibration', calibration)
        }
        // ///////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////
        // Display and save the data
        // If it's a calibration 1 or 2, then it's 0 - 65535 for the results
        // If it's a calibration 3 or 0/undefined (normal measurement), it's -20 - 120
        // If master calibration of cards, display the calibration values so they can be copied to a QR code and printed on the card.

        if (typeof passed.blank !== 'undefined' && passed.blank !== '' && calibration === 0) {
          for (let i = 0; i < processed.median.length; i++) {
            processed.median[i] -= passed.blank[`median_${wavelengths[i]}`]
          }
          if (j === 0) {
            // if its the first protocol, let the user know that the blank was found and applied.
            ui.info(
              'Blank found and applied',
              //              'A blank was found in the local environment variables and it was applied to this measurement.  The blank has now been deleted from memory, so if you measure again you will need to re-blank.'
              'A blank was found and applied to this measurement.'
            )
          }
        }

        // recall saved values for comparison to master if it's a standardCheck (compare to saved master values)
        if (calibration === 8) {
          const dif_allow = 0.025
          for (let i = 0; i < processed.median.length; i++) {
            if (numProtocols <= 2) {
              const masterId = master_wavelengths.indexOf(processed.wavelengths[i])
              const masterRecall = master_recall[masterId]
              const masterVal = meas.recall[`${masterRecall}[${masterId}]`]
              const dif = Math.round(10000 * (masterVal - processed.median[i]) / masterVal) * 0.0001
              if (Math.abs(dif) > dif_allow) {
                const warningText = `Your value on ${processed.wavelengths[i]}nm LED is ${Math.round(processed.median[i])} and saved calibration is ${masterVal}.  It's ${Math.round(100 * dif)}% off from the saved calibration which is high.  Retry the calibration to get closer.`
                processed.warnings.push(warningText)
                ui.warning(`${processed.wavelengths[i]} Warning`, warningText)
                app.csvExport('warnings', processed.warnings.join('/n'))
              }
              app.csvExport(`master_${processed.detectors[i]}` + processed.wavelengths[i], masterVal)
              app.csvExport(`master_dif_${processed.detectors[i]}` + processed.wavelengths[i], dif)
            } else {
              const masterId = master_wavelengths.indexOf(processed.wavelengths[i])
              const masterRecall = master_recall[masterId]
              const masterVal = meas.recall[`${masterRecall}[${masterId}]`]
              const dif = Math.round(10000 * (masterVal - processed.median[i]) / masterVal) * 0.0001
              if (Math.abs(dif) > dif_allow) {
                const warningText = `Your value on ${processed.wavelengths[i]}nm LED is ${Math.round(processed.median[i])} and saved calibration is ${masterVal}.  It's ${Math.round(100 * dif)}% off from the saved calibration which is high.  Retry the calibration to get closer.`
                processed.warnings.push(warningText)
                ui.warning(`${processed.wavelengths[i]} Warning`, warningText)
                app.csvExport('warnings', processed.warnings.join('/n'))
              }
              app.csvExport(`master_${processed.detectors[i]}` + processed.wavelengths[i] + ',' + j, masterVal)
              app.csvExport(`master_dif_${processed.detectors[i]}` + processed.wavelengths[i] + ',' + j, dif)
            }
          }
        }

        // give positive feedback if no warnings found so far
        if (processed.warnings.length === 0) {
          ui.success('Success!', 'No Errors')
        }

        // Save the median and stdev from each wavelength
        for (let i = 0; i < processed.median.length; i++) {
          if (numProtocols <= 2) {
            app.csvExport(`median_${processed.detectors[i]}` + processed.wavelengths[i], processed.median[i])
          } else {
            app.csvExport(`median_${processed.detectors[i]}` + processed.wavelengths[i] + '_' + j, processed.median[i])
          }
        }
        for (let i = 0; i < processed.three_stdev.length; i++) {
          if (numProtocols <= 2) {
            //            app.csvExport(`three_stdev_${processed.detectors[i]}` + processed.wavelengths[i], processed.three_stdev[i])
          } else {
            // app.csvExport(
            //   `three_stdev_${processed.detectors[i]}` + processed.wavelengths[i] + '_' + j,
            //   processed.three_stdev[i]
            // )
          }
        }
        if (calibration === 0) {
          for (let i = 0; i < processed.spad.length; i++) {
            if (numProtocols <= 2) {
              //              app.csvExport(`spad_${processed.detectors[i]}` + processed.wavelengths[i], processed.spad[i])
            } else {
              //              app.csvExport(`spad_${processed.detectors[i]}` + processed.wavelengths[i] + '_' + j, processed.spad[i])
            }
          }
        }

        // VOLTAGE - Save the median and stdev from each wavelength
        if (voltage) {
          for (let i = 0; i < processed.median.length; i++) {
            if (numProtocols <= 2) {
              app.csvExport(`voltage_${processed.detectors[i]}` + processed.wavelengths[i], processed.voltage_median[i])
            } else {
              app.csvExport(
                `voltage_${processed.detectors[i]}` + processed.wavelengths[i] + '_' + j,
                processed.voltage_median[i]
              )
            }
          }
          for (let i = 0; i < processed.three_stdev.length; i++) {
            if (numProtocols <= 2) {
              app.csvExport(
                `voltage_three_stdev_${processed.detectors[i]}` + processed.wavelengths[i],
                processed.voltage_three_stdev[i]
              )
            } else {
              app.csvExport(
                `voltage_three_stdev_${processed.detectors[i]}` + processed.wavelengths[i] + '_' + j,
                processed.voltage_three_stdev[i]
              )
            }
          }
        }

        // FOR NOW this is auto-scaling... but that makes readability hard for users... better to pre-scale consistently.
        // problem is, we need another identifier for the scale (we can base it on calibration, but when we request raw normal measurements it gets out of scale)
        // needs some work here.
        const minAvg = Math.min(...processed.median)
        const maxAvg = Math.max(...processed.median)

        // If this is a question indicating a antioxidant or polyphenol calibration should be applied and there are some calibration values stored...
        const sample_type_list = {
          polyphenols: 'userdef[8]',
          antioxidants: 'userdef[6]',
          proteins: 'userdef[6]',
          lowry_proteins: 'userdef[7]'
          //          tocopherols: 'userdef[8]',
        }

        if (typeof passed.sample_type !== 'undefined' && Object.keys(sample_type_list).includes(passed.sample_type)) {
          if (calibration === 0 &&
            typeof meas.recall !== 'undefined') {
            if (meas.recall[sample_type_list[passed.sample_type]] !== null) {
              // userdef1-10 is y intercepts, userdef11-20 are the slopes.  They are collected using the same set of wavelengths as the standard measurement
              // FYI - wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940];
              // - 587nm antioxidents / protein ;    850nm polyphenols    632 lowry protein
              // 2nd order polynomial fit (yint, and 2 slopes).  smallest slope is stored on device as value = stored_value/1000, so must multiple by 1000 here to correct.

              // if we know the ranges, we can let folks know when things are too high or too low... make sure to update this!
              const ranges = {
                polyphenols: [undefined, 12.5, 25, 50, 75, 125, 150, 200], // ug gallic acid per ml
                antioxidants: [undefined, 12.5, 25, 50, 100, 200], // uM iron equivalents
                proteins: [undefined, 250, 125, 62.5, 31.25, 15.63], // uL protein per ml
                lowry_proteins: [undefined, 50, 150, 250, 400, 500] // ug protein per mL
              }

              // Useful function to check ranges, notify user if out of range, and save results to csv
              // eslint-disable-next-line no-loop-func
              function check_range_save (value, name, saveName, units) {
                let color = 'green'
                let note = ' OK'
                if (value < 0) {
                  color = 'orange'
                  note = ' <-- if way below zero, rerun'
                } else if (value < MathMore.MathMIN(ranges[name])) {
                  color = 'orange'
                } else if (value > MathMore.MathMAX(ranges[name])) {
                  const dilute = MathMore.MathROUND(value / ((MathMore.MathMAX(ranges[name]) + MathMore.MathMIN(ranges[name])) / 2), 0)
                  app.csvExport('high_range_error', 'high_range_error')
                  color = 'red'
                  note = ` <-- redo at ${dilute}:1 dilution`
                }

                const style = `<span style="background-color:${color};">` // apply the default style
                ui.info('Results and Range', `This is a raw value.  This value should ALWAYS be within the range shown below.  The final value (adjusted for dilution and moisture) will be returned in the next survey.<br><br>
                ${passed.sample_type}<br><br>
                Range Min: &nbsp;${MathMore.MathMIN(ranges[name])}<br>
                This Value: ${style}${value}</span>${note}<br>
                Range Max: &nbsp;${MathMore.MathMAX(ranges[name])}`)
                app.csvExport(`${saveName}`, value)

                // recheck these to output the text... helps make the actual range min and max more visible to user this way.
                if (value < 0) {
                  ui.warning('Below zero', 'Sample value is below zero.  If slightly below zero no action is required, this may be within measurement error of zero.  If way below zero, re-run sample.')
                } else if (value < MathMore.MathMIN(ranges[name])) {
                  ui.info('Lower than range', 'Sample is lower than the lowest calibration standard.  No action needed, just FYI!')
                } else if (value > MathMore.MathMAX(ranges[name])) {
                  const dilute = MathMore.MathROUND(value / ((MathMore.MathMAX(ranges[name]) + MathMore.MathMIN(ranges[name])) / 2), 0)
                  ui.error('Higher than range', `Sample is higher than the highest calibration standard.  Unless sample is very very close to the highest standard, you should dilute roughy ${dilute}:1 and re-run.`)
                }
              }

              // Ok - now let's calculate each of the test types, check ranges, show results + save results
              if (passed.sample_type === 'polyphenols') {
                const nm850 = processed.median[7]
                const polyphenols = MathMore.MathROUND( // userdef[8]/18/28 is slopes for 850nm
                  (meas.recall['userdef[28]'] / 1000) * nm850 * nm850 +
                  meas.recall['userdef[18]'] * nm850 +
                  meas.recall['userdef[8]'],
                  3
                )
                check_range_save(polyphenols, 'polyphenols', 'polyphenols', 'ug gallic acid per ml')
              } else if (passed.sample_type === 'antioxidants') {
                const nm587 = processed.median[5]
                const antioxidants = MathMore.MathROUND( // userdef[6]/16/26 is slopes for 587nm
                  (meas.recall['userdef[26]'] / 1000) * nm587 * nm587 +
                  meas.recall['userdef[16]'] * nm587 +
                  meas.recall['userdef[6]'],
                  3
                )
                // antioxidants = 2; // use for testing
                check_range_save(antioxidants, 'antioxidants', 'antioxidants', 'uM iron equivalents')
              } else if (passed.sample_type === 'proteins') {
                const nm587 = processed.median[5]
                const proteins = MathMore.MathROUND( // userdef[6]/16/26 is slopes for 587nm
                  (meas.recall['userdef[26]'] / 1000) * nm587 * nm587 +
                  meas.recall['userdef[16]'] * nm587 +
                  meas.recall['userdef[6]'],
                  3
                )
                check_range_save(proteins, 'proteins', 'proteins', 'ug protein per mL')
              } else if (passed.sample_type === 'lowry_proteins') {
                const nm632 = processed.median[6]
                const lowry_proteins = MathMore.MathROUND( // userdef[7]/17/27 is slopes for 632nm
                  (meas.recall['userdef[27]'] / 1000) * nm632 * nm632 +
                  meas.recall['userdef[17]'] * nm632 +
                  meas.recall['userdef[7]'],
                  3
                )
                check_range_save(lowry_proteins, 'lowry_proteins', 'proteins', 'ug protein per mL')
              }
            } else {
              ui.error(`Cant find calibration for ${passed.sample_type}`, `Looked for ${sample_type_list[passed.sample_type]} on the device to apply the calibration, but couldn't find it.  Did you forget to do wet chem calibration or did you not save it?`)
            }
          }
        }

        // ///////////////////////////////////////////////////////
        // If and only if the user has a questions saying that this is a chlorophyll measurement, then display the chlorophyll + related info at the top + save info to CSV
        if (passed.leaf === 1) {
          const leaf_names = ['chlorophyll', 'carotenoids', 'anthocyanins', 'ndvi', 'cvi']
          const leaf_results = [
            Math.round(100 * processed.spad[6]) / 100,
            Math.round(100 * processed.spad[3]) / 100,
            Math.round(100 * processed.spad[4]) / 100,
            Math.round(100 * (processed.median[9] - processed.median[6]) / (processed.median[9] + processed.median[6])) / 100,
            Math.round(100 * processed.median[9] * (processed.median[6] / processed.median[4])) / 100
          ]
          let table = ''
          for (let i = 0; i < leaf_results.length; i++) {
            if (Number.isNaN(leaf_results[i])) {
              leaf_results[i] = -1
              ui.warning(
                'Warning',
                leaf_names[i] + ' is out of range.  Leaf may be too thick.  Value set to -1'
              )
            }
            const add = `${leaf_names[i]} : ${leaf_results[i]}<br>`
            table += add
            if (numProtocols <= 2) {
              app.csvExport(`${leaf_names[i]}`, `${leaf_results[i]}`)
            } else {
              app.csvExport(`${leaf_names[i]}_` + j, `${leaf_results[i]}`)
            }
          }
          ui.info('Leaf Results', table)
        }
        // ///////////////////////////////////////////////////////

        ui.plot({
            series: [processed.median]
          },
          'Average Spectral Values', {
            min: minAvg,
            max: maxAvg,
            axisX: {
              labelInterpolationFnc (value, index) {
                return `${wavelengths[index]}`
              }
            }
          }
        )

        let assembleData = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ave., 3 st. dev., SPAD<br>'
        for (let i = 0; i < wavelengths.length; i++) {
          assembleData += `${wavelengths[i]}, ${sprintf('%.2f', processed.median[i])},&nbsp; ${sprintf('%.3f', processed.three_stdev[i])}, &nbsp;&nbsp;${sprintf('%.2f', processed.spad[i])}<br>`
        }
        ui.info('Spectral values', assembleData)

        ui.plot({
            series: [data]
          },
          'Raw Spectral Results', {
            //    min: 0,
            //    max: 66000,
          }
        )
        if (voltage) {
          ui.plot({
              series: [meas.voltage_raw]
            },
            'Raw Voltage Results', {
              //    min: 0,
              //    max: 66000,
            }
          )
        }
        /*
        const minStd = Math.min(...processed.three_stdev/*
        const maxStd = Math.max(...processed.three_stdev);
        ui.plot({
            series: [processed.three_stdev],
          },
          'Spectral Noise (3 st. dev.)', {
            min: minStd,
            max: maxStd,
            axisX: {
              labelInterpolationFnc(value, index) {
                return `${wavelengths[index]}`;
              },
            },
          },
        );
        */
        //        ui.info('Medians', processed.median.map(e => sprintf('%.2f,', e)).join('<br>'));
        //        ui.info('Three st. dev.', processed.three_stdev.map(e => sprintf('%.2f,', e)).join('<br>'));

        if (voltage) {
          ui.info(
            'Voltage Medians',
            processed.voltage_median.map(e => sprintf('%.2f,', e)).join('<br>')
          )
          ui.info(
            'Voltage Three st. dev.',
            processed.voltage_three_stdev.map(e => sprintf('%.2f,', e)).join('<br>')
          )
        }
        /*
                if (calibration === 0) {
                  ui.info('Spad', processed.spad.map(e => sprintf('%.2f,', e)).join('<br>'));
                }
        */
      } // if data_raw exists
      if (typeof meas.temperature !== 'undefined') {
        ui.info(
          'Environmental conditions',
          sprintf(
            '%.2f C, %.2f %% humidity, %.2f mB pressure, %.2f IAQ VOCs',
            meas.temperature,
            meas.humidity,
            meas.pressure,
            meas.voc
          )
        )
        if (numProtocols <= 2) {
          app.csvExport('temperature', meas.temperature)
          app.csvExport('humidity', meas.humidity)
          app.csvExport('pressure', meas.pressure)
          app.csvExport('voc', meas.voc)
        } else {
          app.csvExport('temperature_' + j, meas.temperature)
          app.csvExport('humidity_' + j, meas.humidity)
          app.csvExport('pressure_' + j, meas.pressure)
          app.csvExport('voc_' + j, meas.voc)
        }
      }
      // display and save toDevice if there is anything in it
    }
    if (toDevice) {
      ui.info(
        'Information about toDevice',
        'Some measurements save data to send back to the device, to update calibrations or blanks.  The following is what is currently available to save.  This is provided for information only, and your device information is not changed unless you run another measurement after this one to actually save it.'
      )
      ui.info('Contents of toDevice', `<details><summary>Click to view details</summary>
      <p>
      ${toDevice}
      </p>
      </details>
        <p>`) //
      app.csvExport('toDevice', toDevice)
      //      app.setEnv('toDevice', toDevice, 'information to pass to device')
    }
  }
})()
// final save of all app.csvExport calls -- make sure this is always at the very end!
app.save()
