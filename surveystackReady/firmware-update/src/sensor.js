/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/// ////////////// required for surveystack ///////////////////////
// update references to submission, survey, and params

import app from './lib/app'
import serial from './lib/serial'
import * as utils from './lib/utils'

/// /////////////////// FIRMWARE UPDATING SPECIFIC CODE /////////////////////////
let fw = ''
// THIS IS BREAKING ABILITY TO COMPILE
// ON DESKTOP IT'S (APPARENTLY) APP.ISANDROID = TRUE
// it shouldn't be hitting the require statement, that's why I think it's failing.
if (app.isAndroid) {
  console.log('I think this is an android')
  fw = require('./reflectance-spec-firmware.ino.hex')
} else {
  console.log('I think this not an android')
  const fs = require('fs')
  fw = fs.readFileSync('./src/reflectance-spec-firmware.ino.hex', 'utf-8').toString()
}

fw = fw.trim()
/// /////////////////////////////////////////////////////////////////////////////

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none'
  })

  const {
    submission,
    survey,
    params
  } = app.getInputs().props

  // SOME EXAMPLES USING PARAMETERS IN SURVEYSTACK
  // example 'param' data
  // { "id": "standard-6", "protocol": "calibrateBlankLocal"}
  // serial.write(JSON.stringify(protocol));
  // how to pull in 'param' data into this script
  //  if (params.device_id) device_id = checkSurveyRef(params.device_id); // find place in submission which has the device ID and get it
  //  if (params.chlorophyll) chlorophyll = true; // set true, show chlorophyll at the top

  const result = {} // where we put the results to pass to processor
  result.error = {} // where we put the results to pass to processor
  result.warning = {} // where we put the results to pass to processor
  result.info = {} // where we put the results to pass to processor

  /// //////////////////////////////// SEND FIRMWARE + CONFIRM SUCCESS /////////////////////////////////
  await utils.sleep(1000)

  // make sure there's firmware available to load
  if (!fw) {
    console.log('unable to load firmware')
  }
  console.log(`fw len: ${fw.length}`)

  // get device information and current device_firmware
  let check = {}
  let count = 0
  serial.write('print_memory+')
  try {
    check = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1
      app.progress(0.1 + (count / 617.0) * 100.0 * 0.9)
    })
  } catch (err) {
    result.error.checkVersion = 'Error checking firmware version on the device'
    console.error(`error reading json: ${err.message} ${check}`)
  }

  // check the device version, compare to new verson (stored in params)

  // version information must be in format X.X.X (major.minor.patch)
  result.device_firmware_old = check.device_firmware
  result.device_firmware_new = params.device_firmware
  let update = false // set to true if update is needed
  for (let i = 0; i < params.device_firmware.split('.').length; i++) {
    if (Number(params.device_firmware.split('.')[i]) > Number(check.device_firmware.split('.')[i])) update = true
  }

  // update device if it is needed
  if (update === true) {

    result.memory_old = JSON.stringify(check);
    result.toDevice = migrateMemory(check);

    count = 0
    try {
      app.progress(10)

      const lines = fw.split('\n')

      // start flash
      // serial.write('[{}]');
      // serial.write('print_memory+');
      serial.write('1078+')

      for (; ;) {
        const r = await serial.readLine()
        console.log(r)
        if (r === 'waiting for intel hex lines') {
          break
        }
      }

      for (let i = 0; i < lines.length; i++) {
        serial.write(`${lines[i]}\n`)
        await utils.sleep(2)
        console.log(`sending ${lines[i]}`)
        app.progress(10 + (i * 80.0) / lines.length)
      }

      for (; ;) {
        const r = await serial.readLine()
        console.log(r)
        if (r.startsWith(`done, ${lines.length} hex lines`)) {
          break
        }
      }

      console.log(`:flash ${lines.length}`)
      serial.write(`:flash ${lines.length}\n`)

      await utils.sleep(5000)
      serial.write('device_info+\r')

      result.status = 'ok'
      result.info_updated = `v${result.device_firmware_old} -> v${result.device_firmware_new} - Firmware successfully updated`
      result.len = lines.length
    } catch (err) {
      result.error_fail = 'failure reading JSON'
      console.error(`error reading json: ${err}`)
    }
    /// //////////////////////////////////////////////////////////////////////////////////
  } else {
    result.status = 'ok'
    result.info_upToDate = `Device up to date at v${result.device_firmware_old} - Firmware not updated.`
  }
  console.log('result:')
  console.log(result)
  app.result(result)
})()

function migrateMemory(mem) {
//  mem = example_mem; // for testing
  let toDevice = '';
  let firmware_3 = ['ir_high', 'ir_low', 'vis_high', 'vis_low', 'ir_high_master', 'ir_low_master', 'vis_high_master', 'vis_low_master', 'ir_blank', 'vis_blank', 'heatcal1', 'heatcal2', 'heatcal3'];
  let firmware_4 = ['normalized_soil', 'normalized_5mlcuvette', 'normalized_1mlcuvette', 'normalized_custom'];
  toDevice += pushAll(firmware_3); // first, upload firmware_3 values
  if (mem?.normalized_custom) { // then upload firmware_4 values if they exist (normalized_custom was created in version 4, so let's check that)
    toDevice += pushAll(firmware_4);
  } else { // if they don't, set them to 1 as default value.
    toDevice += pushAll(firmware_4, 1);
  }
  // if additional firmware changes to eeprom are made, add them as if statements here (firmware_5, etc. etc.)
  toDevice += 'set_user_defined+'; // finally upload all userdefs
  for (let i = 0; i < 70; i++) {
    toDevice += `${i}+${mem[`userdef${i}`]}+`
    if (i === 69) toDevice += '-1+'
  }
  toDevice += 'print_memory+'
  return toDevice;

  // function to upload all the values in the LED array strings
  // option to setAll - reset vals to the setAll parameter if provided
  function pushAll(list, setAll) {
    let str = '';
    list.forEach((name) => {
      console.log(name);
      str += `set_${name}+`;
      for (let i = 0; i < 11; i++) {
        if (setAll !== 0 && setAll) {
          str += `${i}+${setAll}+`;
        } else {
          str += `${i}+${mem[name][i]}+`;
        }
      }
      str += `-1+`; // close out this list, move on to the next
    })
    return str;
  }
}

let example_mem = {"device_name":"Reflectometer","device_version":"1.1.0","device_id":"ff:ff:ff:ff","device_battery":99,"device_firmware":"3.0.1","shutdown":420000,
"ir_high": ["22.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000"],
"ir_low": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"vis_high": ["655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000","655350016.000000"],
"vis_low": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"ir_high_master": ["10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000"],
"ir_low_master": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"vis_high_master": ["10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000","10000.000000"],
"vis_low_master": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"ir_blank": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"vis_blank": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"heatcal1": ["44","1.000000","1.000000","1.000000","1.000000","1.000000","1.000000","1.000000","1.000000","1.000000","55"],
"heatcal2": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"heatcal3": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"colorcal_blank1": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"colorcal_blank2": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"colorcal_blank3": ["0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000","0.000000"],
"userdef0": "0.000000",
"userdef1": "0.000000",
"userdef2": "0.000000",
"userdef3": "0.000000",
"userdef4": "18",
"userdef5": "0.000000",
"userdef6": "0.000000",
"userdef7": "0.000000",
"userdef8": "0.000000",
"userdef9": "0.000000",
"userdef10": "0.000000",
"userdef11": "0.000000",
"userdef12": "908",
"userdef13": "0.000000",
"userdef14": "0.000000",
"userdef15": "0.000000",
"userdef16": "0.000000",
"userdef17": "0.000000",
"userdef18": "0.000000",
"userdef19": "0.000000",
"userdef20": "0.000000",
"userdef21": "0.000000",
"userdef22": "0.000000",
"userdef23": "0.000000",
"userdef24": "0.000000",
"userdef25": "0.000000",
"userdef26": "0.000000",
"userdef27": "0.000000",
"userdef28": "0.000000",
"userdef29": "0.000000",
"userdef30": "0.000000",
"userdef31": "0.000000",
"userdef32": "0.000000",
"userdef33": "0.000000",
"userdef34": "0.000000",
"userdef35": "0.000000",
"userdef36": "0.000000",
"userdef37": "0.000000",
"userdef38": "0.000000",
"userdef39": "0.000000",
"userdef40": "0.000000",
"userdef41": "0.000000",
"userdef42": "0.000000",
"userdef43": "0.000000",
"userdef44": "0.000000",
"userdef45": "0.000000",
"userdef46": "0.000000",
"userdef47": "0.000000",
"userdef48": "0.000000",
"userdef49": "0.000000",
"userdef50": "0.000000",
"userdef51": "0.000000",
"userdef52": "0.000000",
"userdef53": "0.000000",
"userdef54": "0.000000",
"userdef55": "0.000000",
"userdef56": "0.000000",
"userdef57": "0.000000",
"userdef58": "0.000000",
"userdef59": "0.000000",
"userdef60": "0.000000",
"userdef61": "0.000000",
"userdef62": "0.000000",
"userdef63": "0.000000",
"userdef64": "0.000000",
"userdef65": "0.000000",
"userdef66": "0.000000",
"userdef67": "0.000000",
"userdef68": "0.000000",
"userdef69": "0.000000"
}
