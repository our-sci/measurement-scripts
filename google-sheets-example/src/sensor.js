/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This example demonstrates how to fetch data in the sensor script
 * from google sheets.
 * Using the sheet from
 * https://docs.google.com/spreadsheets/d/1clTpjjA7Bb-lSKMlOcub219zdlQEYBnWOyVIOHg_RZ0/edit#gid=236866563
 *
 * Which contains multiple pages (makes processing a bit more challenging)
 */

import moment from 'moment';

import app from './lib/app';
import serial from './lib/serial';
import sheets from './lib/sheets';

export default serial; // expose this to android for calling onDataAvailable

const res = {};
(async () => {
  try {
    const gsheet = await sheets('1clTpjjA7Bb-lSKMlOcub219zdlQEYBnWOyVIOHg_RZ0');
    // console.log(gsheet);
    console.log('success');
    console.log('This is data as json encoded with column name information');
    console.log(gsheet.ta.sheets('food, no helium heavy metals').all());
    console.log('This is an array');
    console.log(gsheet.ta.sheets('food, no helium heavy metals').toArray());
    res.data = gsheet.ta.sheets('food, no helium heavy metals').toArray();
  } catch (e) {
    res.error = e;
    console.log(e);
  }
  app.result(res);
})();