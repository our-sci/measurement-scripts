/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This example demonstrates how to fetch data in the sensor script
 * from google sheets.
 * Using the sheet from
 * https://docs.google.com/spreadsheets/d/1WtgJxsVK3f0bmyup2Dqm_QTLRgP3RC3nzM2V3YT3XTw/edit?usp=sharing
 * Which contains multiple pages (makes processing a bit more challenging)
 */

import moment from 'moment';

import app from './lib/app';
import serial from './lib/serial';
import sheets from './lib/sheets';

export default serial; // expose this to android for calling onDataAvailable

const result = {};

result.sample_number = app.getAnswer('sample_number');

(async () => {
  const sampleID = app.getAnswer('sampleid');
  const gsheet = await sheets('1WtgJxsVK3f0bmyup2Dqm_QTLRgP3RC3nzM2V3YT3XTw');
  result.data = gsheet.ta.sheets('Sheet1').toArray();

  app.result(result);
})();
