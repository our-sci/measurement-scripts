/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This example demonstrates how to fetch data in the sensor script
 * from google sheets.
 * Using the sheet from
 * https://docs.google.com/spreadsheets/d/16f-BMZ7O1G3H27fEZEjo43S5U9gclv4Qdu0v8qq6a1U/edit?usp=sharing
 * Which contains multiple pages (makes processing a bit more challenging)
 */

import app from './lib/app';
import serial from './lib/serial';
import sheets from './lib/sheets';

export default serial; // expose this to android for calling onDataAvailable

//let data1 = [];
//let data2 = [];
const result = {};
result.error = {};
const sheetId = '16f-BMZ7O1G3H27fEZEjo43S5U9gclv4Qdu0v8qq6a1U'; // stays the same unless we change google doc location
let sheet1 = '';
let sheet2 = '';
const data = {};
const requiredAnswers = ['sample_id', 'sample_type', 'soil_group/Depth'];
// get info from the sheet with the lower set of elements (Na, P, S...)
(async () => {
  requiredAnswers.forEach((a) => {
    let v;
    //    console.log(typeof app.getAnswer('sample_id'));
    if (typeof app.getAnswer(a) !== 'undefined') {
      // does it exist?
      v = app.getAnswer(a);
    }
    console.log(v);
    result[a] = v;
  });


  // go see what type of sample it is (if sample_type is carrot or spinach then we pull data from one sheet, if depth is 3 (0 - 6'') or 9 (6 - 12'') we pull data from a different sheet
  // if there is no sample_type, then it check to see if it's soil.  If there is neither, then let the user know

  // there is no sample_type, then it check to see if it's soil.  If there is neither, then let the user know

  const params = result.sample_type.split(' '); // for some reason " " is converted to "/" in android app... maybe by ODK... so split on both!

  const sample_id = result.sample_id;
  let has_sample = false;
  if (params.includes('food')) {
    let he = [];
    let nohe = [];
    has_sample = true;
    sheet1 = 'food, helium';
    sheet2 = 'food, no helium heavy metals';
    console.log(result.sample_type);
    try {
      const gsheet = await sheets(sheetId);
      console.log('success');
      he = gsheet.ta.sheets(sheet1).toArray();
      nohe = gsheet.ta.sheets(sheet2).toArray();
      // filter results to only those with current survey id, and combine both normal and heavy metals data (data1 + data2)
      // Get rid of the extra info the XRF adds to the element names so that it's easier to sort and combine metals
      for (let i = 0; i < he.length; i++) {
        he[i][0] = he[i][0].slice(0, 2);
      }
      for (let i = 0; i < nohe.length; i++) {
        nohe[i][0] = nohe[i][0].slice(0, 2);
      }
      he = he.filter(a => a[3] === sample_id);
      nohe = nohe.filter(a => a[3] === sample_id);
      // remove overlapping metals... these are measured in both XRF runs, but results are more accurate in the non-heavy metals measurement
      he = he.filter(
        a =>
        a[0] === 'Na' ||
        a[0] === 'Mg' ||
        a[0] === 'Al' ||
        a[0] === 'Si' ||
        a[0] === 'P ' ||
        a[0] === 'S ' ||
        a[0] === 'Cl' ||
        a[0] === 'Rh' ||
        a[0] === 'K ' ||
        a[0] === 'Ca' ||
        a[0] === 'Mn' ||
        a[0] === 'Fe' ||
        a[0] === 'Ni' ||
        a[0] === 'Cu' ||
        a[0] === 'Zn' ||
        a[0] === 'Fe',
      );
      nohe = nohe.filter(
        a =>
        a[0] === 'As' ||
        a[0] === 'Pb' ||
        a[0] === 'Se' ||
        a[0] === 'Mo',
      );
    } catch (e) {
      result.error = e;
      console.log(e);
    }
    data.food = he.concat(nohe);
    console.log('data.food');
    console.log(data.food);
  }
  if (params.includes('soil9')) {
    has_sample = true;
    let he = [];
    let nohe = [];
    sheet1 = 'soil, helium';
    sheet2 = 'soil, no helium heavy metals';
    console.log(result.sample_type);
    try {
      const gsheet = await sheets(sheetId);
      console.log('success');
      he = gsheet.ta.sheets(sheet1).toArray();
      nohe = gsheet.ta.sheets(sheet2).toArray();
      // filter results to only those with current survey id, and combine both normal and heavy metals data (data1 + data2)
      // Get rid of the extra info the XRF adds to the element names so that it's easier to sort and combine metals
      for (let i = 0; i < he.length; i++) {
        he[i][0] = he[i][0].slice(0, 2);
      }
      for (let i = 0; i < nohe.length; i++) {
        nohe[i][0] = nohe[i][0].slice(0, 2);
      }
      he = he.filter(a => a[3] === `${sample_id}b`);
      nohe = nohe.filter(a => a[3] === `${sample_id}b`);
      nohe = nohe.filter(
        a =>
        a[0] === 'Na' ||
        a[0] === 'Mg' ||
        a[0] === 'Al' ||
        a[0] === 'Si' ||
        a[0] === 'P ' ||
        a[0] === 'S ' ||
        a[0] === 'Rh' ||
        a[0] === 'K ' ||
        a[0] === 'Ca' ||
        a[0] === 'Ba' ||
        a[0] === 'Ti' ||
        a[0] === 'V' ||
        a[0] === 'Cr' ||
        a[0] === 'Mn' ||
        a[0] === 'Fe' ||
        a[0] === 'Ni' ||
        a[0] === 'Cu' ||
        a[0] === 'Zn',
      );
      nohe = nohe.filter(
        a =>
        a[0] === 'As' ||
        a[0] === 'Pb' ||
        a[0] === 'Se' ||
        a[0] === 'Rb' ||
        a[0] === 'Sr' ||
        a[0] === 'Mo',
      );
      // the data coming from the XRF calibration for soil only was in % for the following elements.
      const inPercent = ['Mg', 'Al', 'P ', 'S ', 'K ', 'Ca', 'Fe'];
      // for consistency, we're going to print everything in parts per million, so let's convert them back...
      for (let i = 0; i < he.length; i++) { // go through all values in i
        for (let j = 0; j < inPercent.length; j++) { // cross check with values in inPercent
          if (he[i][0] === inPercent[j]) { // if you find a match, then convert to ppm (multiple by 10,000)
            he[i][2] = `${he[i][2] * 10000}`;
            console.log(`converted ${he[i][0]}`);
          }
        }
      }
      data.soil9 = he.concat(nohe);
      console.log('data.soil9');
      console.log(data.soil9);
    } catch (e) {
      result.error = e;
      console.log(e);
    }
  }
  if (params.includes('soil3')) {
    has_sample = true;
    let he = [];
    let nohe = [];
    sheet1 = 'soil, helium';
    sheet2 = 'soil, no helium heavy metals';
    console.log(result.sample_type);
    try {
      const gsheet = await sheets(sheetId);
      console.log('success');
      he = gsheet.ta.sheets(sheet1).toArray();
      nohe = gsheet.ta.sheets(sheet2).toArray();
      // filter results to only those with current survey id, and combine both normal and heavy metals data (data1 + data2)
      // Get rid of the extra info the XRF adds to the element names so that it's easier to sort and combine metals
      for (let i = 0; i < he.length; i++) {
        he[i][0] = he[i][0].slice(0, 2);
      }
      for (let i = 0; i < nohe.length; i++) {
        nohe[i][0] = nohe[i][0].slice(0, 2);
      }
      he = he.filter(a => a[3] === `${sample_id}a`);
      nohe = nohe.filter(a => a[3] === `${sample_id}a`);
      he = he.filter(
        a =>
        a[0] === 'Na' ||
        a[0] === 'Mg' ||
        a[0] === 'Al' ||
        a[0] === 'Si' ||
        a[0] === 'P ' ||
        a[0] === 'S ' ||
        a[0] === 'Rh' ||
        a[0] === 'K ' ||
        a[0] === 'Ca' ||
        a[0] === 'Ba' ||
        a[0] === 'Ti' ||
        a[0] === 'V' ||
        a[0] === 'Cr' ||
        a[0] === 'Mn' ||
        a[0] === 'Fe' ||
        a[0] === 'Ni' ||
        a[0] === 'Cu' ||
        a[0] === 'Zn',
      );
      nohe = nohe.filter(
        a =>
        a[0] === 'As' ||
        a[0] === 'Pb' ||
        a[0] === 'Se' ||
        a[0] === 'Rb' ||
        a[0] === 'Sr' ||
        a[0] === 'Mo',
      );
      // the data coming from the XRF calibration for soil only was in % for the following elements.
      const inPercent = ['Mg', 'Al', 'P ', 'S ', 'K ', 'Ca', 'Fe'];
      // for consistency, we're going to print everything in parts per million, so let's convert them back...
      for (let i = 0; i < he.length; i++) { // go through all values in i
        for (let j = 0; j < inPercent.length; j++) { // cross check with values in inPercent
          if (he[i][0] === inPercent[j]) { // if you find a match, then convert to ppm (multiple by 10,000)
            he[i][2] = `${he[i][2] * 10000}`;
            console.log(`converted ${he[i][0]}`);
          }
        }
      }
      data.soil3 = he.concat(nohe);
      console.log('data.soil3');
      console.log(data.soil3);
    } catch (e) {
      result.error = e;
      console.log(e);
    }
  }
  result.data = data;
  console.log('filtered + combined');
  console.log(result.data);
  app.result(result);
})();