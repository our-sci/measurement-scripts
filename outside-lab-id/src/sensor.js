import {
  app,
  serial
} from '@oursci/scripts';

export default serial; // expose this to android for calling onDataAvailable

const result = {};
result.log = [];
result.error = [];
// const survey = defs.survey();
// hello!
// export const survey = surveyHelper();
export function err(msg) {
  result.error.push(msg);
}

(async () => {
  try {

    // const getMeasurementAnswers = (id) => {
    //   try {
    //     const a = app.getAnswer(id);
    //     if (a === '' || a === undefined || a === null || a === {}) {
    //       return null;
    //     }
    //     return JSON.parse(app.getAnswer(id)).data;
    //   } catch (exception) {
    //     console.log(exception);
    //     return null;
    //   }
    // };

    const getTextAnswer = (a) => {
      let v;
      if (typeof app.getAnswer(a) !== 'undefined') {
        v = app.getAnswer(a);
        if (app.getAnswer(a) === '') {}
      } else {
        v = 'undefined';
      }
      result[a] = v;
      return v;
    };
    result.soil_id = getTextAnswer('soil/soil_1');
    result.produce_id = getTextAnswer('produce/produce_1');

    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.result(result);
  }
})();