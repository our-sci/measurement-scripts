/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

import app from './lib/app';
import serial from './lib/serial';
import sendDevice from './lib/sendDevice';

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyUSB0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 9600,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  console.log('start');
  // Mark the progress of pulling in the data
  const result = {};

  try {
    const r = await serial.readLine2();
    result.raw = r;
  } catch (err) {
    console.error(`error reading: ${err}`);
  }
  const reg = /^.*WT:(.*?)g.*$/;
  result.weight = reg.exec(result.raw)[1];

  console.log('result');
  console.log(result.raw);
  console.log('weight');
  console.log(result.weight);
  app.result(result);
})();