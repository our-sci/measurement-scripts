/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
import { unique } from './lib/utils';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  const sample_number = result.sample_number;
  ui.info('Sample ID', result.sample_number);

  const columnNames = ['sampleid', 'lat', 'long'];

  let outputBox = '';
  result.data.forEach((a) => {
    if (result.sample_number === a[0]) {
      a.forEach((b, indexValue) => {
        outputBox += `${columnNames[indexValue]}: ${b}<br>`;
        //        ui.info(columnNames[indexValue], b);
        app.csvExport(columnNames[indexValue], b);
      });
    }
  });

  ui.info('All Output', outputBox);

  app.save();
})();
