/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
// import _ from 'lodash';

import { app } from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  // Process your results from sensor.js here
  // ...

  if (result.log) {
    // Display each entry from the results log array with an info card
    result.log.forEach((log) => {
      ui.info(log.title, log.content);
    });
  }

  let box = '';
  box += `<p style="font-size: 140%">Soil texture: ${result.soil_texture_1}</p><br>`;
  box += `Form a ball? ${result.ball}<br>`;
  box += `Form a ribbon? ${result.ribbon}<br>`;
  box += `Length of ribbon? ${result.ribbon_length}<br>`;
  box += `Feel when wet? ${result.excessive_wetting}`;

  ui.info(result.name_field1, box);
  box = '';

  app.csvExport('soil_texture_1', result.soil_texture_1); // will create soiltype_quesstion.soil_texture

  if (result.error && result.error.length > 0) {
    // <------ this is new, check if length is 0
    // Display each entry from the results error array with an error card
    app.error(); // this is new, indicating that there is an error
    result.error.forEach((msg) => {
      ui.error('Error', msg);
    });
    app.save();
    return;
  }

  app.save();
})();
