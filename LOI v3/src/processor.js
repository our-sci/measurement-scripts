/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

// import { sprintf } from 'sprintf-js';
// import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  // if it came from the balance, use it
  // if not, use numerical output
  // if neither, report an error
  // info and csv output
  let crucible_wt = 0;
  if (
    result.loi_crucible_ohaus !== '' &&
    typeof result.loi_crucible_ohaus.data !== 'undefined' &&
    typeof result.loi_crucible_ohaus.data.weight_grams !== 'undefined'
  ) {
    crucible_wt = result.loi_crucible_ohaus.data.weight_grams;
    ui.info('Crucible Weight (ohaus)', MathMore.MathROUND(crucible_wt, 2));
  } else if (Number(result.loi_crucible_manual)) {
    crucible_wt = result.loi_crucible_manual;
    ui.info('Crucible Weight (manual)', MathMore.MathROUND(crucible_wt, 2));
  } else {
    ui.error(
      'Crucible Weight Missing',
      'The crucible weight is missing both from the scale and from manual entry, so LOI Carbon cannot be calculated.  Go back and fill in the weight.',
    );
  }

  let moist_wt = 0;
  if (
    result.loi_pre_ohaus !== '' &&
    typeof result.loi_pre_ohaus.data !== 'undefined' &&
    typeof result.loi_pre_ohaus.data.weight_grams !== 'undefined'
  ) {
    moist_wt = result.loi_pre_ohaus.data.weight_grams;
    ui.info('Moist Weight (ohaus)', MathMore.MathROUND(moist_wt, 2));
  } else if (Number(result.loi_pre_manual)) {
    moist_wt = result.loi_pre_manual;
    ui.info('Moist Weight (manual)', MathMore.MathROUND(moist_wt, 2));
  } else {
    ui.error(
      'Moist Weight Missing',
      'The crucible + soil weight is missing both from the scale and from manual entry, so LOI Carbon cannot be calculated.  Go back and fill in the weight.',
    );
  }

  let burned_wt = 0;
  if (
    result.loi_post_ohaus !== '' &&
    typeof result.loi_post_ohaus.data !== 'undefined' &&
    typeof result.loi_post_ohaus.data.weight_grams !== 'undefined'
  ) {
    burned_wt = result.loi_post_ohaus.data.weight_grams;
    ui.info('Final Weight (ohaus)', MathMore.MathROUND(burned_wt, 2));
  } else if (Number(result.loi_post_manual)) {
    burned_wt = result.loi_post_manual;
    ui.info('Final Weight (manual)', MathMore.MathROUND(burned_wt, 2));
  } else {
    ui.error(
      'Final Weight Missing',
      'The final weight is missing, so LOI Carbon cannot be calculated.  Go back and fill in the weight.',
    );
  }
  if (moist_wt !== 0 && burned_wt !== 0 && crucible_wt !== 0) {
    const Organic_Matter = 100 * ((moist_wt - burned_wt) / (moist_wt - crucible_wt));
    // Traditional conversion factor for %OM to %TOC is OM = TOC x 1.724, may vary based on soil type and type of OM;
    const Total_C = Organic_Matter / 1.724;

    if (Total_C < 0) {
      ui.error('Total Carbon < 0!', 'something is wrong, check the weights or sample IDs');
    }
    if (Total_C < 0.4) {
      ui.warning('Total Carbon < 0.5!', 'for very poor soils this could be correct, but is very low.  Confirm before continuing');
    }
    if (Total_C > 10) {
      ui.warning('Total Carbon > 10!', 'this is very unlikely for minerals soils.  Check the weights or sample IDs to ensure there wasnt an error');
    }

    ui.info('Pre Weight', MathMore.MathROUND(moist_wt - crucible_wt, 3));
    app.csvExport('pre_weight', MathMore.MathROUND(moist_wt - crucible_wt, 3));
    ui.info('Post Weight', MathMore.MathROUND(burned_wt - crucible_wt, 3));
    app.csvExport('post_weight', MathMore.MathROUND(burned_wt - crucible_wt, 3));
    ui.info('Organic Matter %', MathMore.MathROUND(Organic_Matter, 2));
    app.csvExport('Organic_Matter', MathMore.MathROUND(Organic_Matter, 2));
    ui.info('Total Organic C %', MathMore.MathROUND(Total_C, 2));
    app.csvExport('Total Organic C %', MathMore.MathROUND(Total_C, 2));
  }

  app.save();
})();