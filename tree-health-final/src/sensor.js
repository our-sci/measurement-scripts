/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import moment from 'moment';
import _ from 'lodash';
import mathjs from 'mathjs';

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import * as utils from './lib/utils'; // use: await sleep(1000);
import soilgrid from './lib/soilgrid';
import weather from './lib/weather';
import oursci from './lib/oursci';

export default serial; // expose this to android for calling onDataAvailable

const questions = [
  'species',
  'tree_id',
  'location',
  'dbh',
  'age',
  'crown_density',
  'crown_dieback',
  'crown_light_exposure',
  'crown_position',
  'is_chlorophyll',
  'chlorophyll/chlorophyll_1',
  'chlorophyll_2',
  'chlorophyll_3',
  'chlorophyll_4',
  'chlorophyll_5',
  'chlorophyll_6',
  'chlorophyll_7',
  'chlorophyll_8',
  'tree_health',
  'tpz_line_bare',
  'tpz_line_grass',
  'tpz_line_impervious',
  'tpz_line_mulch',
  'compaction_subsurface_1',
  'compaction_surface_1',
  'dig_info_15_1',
  'moisture_15_1',
  'worms_15_1',
  'fungi_15_1',
  'compaction_subsurface_2',
  'compaction_surface_2',
  'dig_info_15_2',
  'moisture_15_2',
  'worms_15_2',
  'fungi_15_2',
  'soil_carbon_15',
  'carbon_mineralization_15',
  'total_organic_carbon',
  'predict',
];

const compareFields = [
  'chlorophyll_1.data.chlorophyll_spad',
  'chlorophyll_1.data.carotenoid_spad',
  'chlorophyll_1.data.anthocyanin_spad',
  'dbh',
  'age',
  'tpz_line_bare',
  'tpz_line_grass',
  'tpz_line_impervious',
  'tpz_line_mulch',
  'crown_density',
  'crown_dieback',
  'crown_light_exposure',
  'crown_position',
];

const res = {};

(async () => {
  const errors = {};
  res.errors = {};

  let data;
  const averages = {};
  const variances = {};
  const current = {};

  res.answers = {};
  questions.forEach((q) => {
    res.answers[q] = app.getAnswer(q);
  });

  try {
    data = await oursci('Tree-Health-Survey_5');
  } catch (error) {
    errors['oursci'] = `Unable to fetch oursci survey results: ${error}`;
    res.errors = errors;
    app.result(res);
  }

  const bySpecies = utils.groupBy(data, 'species');

  _.forEach(bySpecies, (s) => {
    const name = s.species[0];
    averages[name] = {};
    variances[name] = {};

    _.forEach(compareFields, (f) => {
      const values = _.map(_.filter(s[f], v => !Number.isNaN(v)), a => Number.parseFloat(a));
      if (values.length > 0) {
        averages[name][f] = mathjs.mean(values);
        variances[name][f] = mathjs.var(values);
      }
    });
  });

  res.averages = averages;
  res.variances = variances;

  /*
  let summary = [];

  try {
    summary = await googleSheets('1qYwGx7lieQQt7W55FZ2f5z7G31k-hVIHhn3wIIAxObQ');
  } catch (error) {
    errors['google sheets'] = `Unable to fetch summary from sheets: ${error}`;
    res.errors = errors;
    app.result(res);
  }
  */

  console.log('result so far');
  console.log(JSON.stringify(res, null, 2));

  let date = '';
  if (res.answers['chlorophyll/chlorophyll_1']) {
    try {
      const parsed = JSON.parse(res.answers['chlorophyll/chlorophyll_1']);
      console.log(parsed);
      date = Date.parse(parsed.meta.date);
    } catch (error) {
      errors['chlorophyl'] = `First chlorophyl measurement missing: ${error}`;
      res.errors = errors;
      app.result(res);
      return;
    }
  }

  if (!date) {
    console.log('date missing');
    errors['date missing'] = 'First chlorophyl measurement missing date';
    res.errors = errors;
    app.result(res);
    return;
  }

  if (!res.answers['location']) {
    errors['location'] = 'Location missing, cannot run APIs';
    res.errors = errors;
    app.result(res);
    return;
  }

  const [lat, lon] = res.answers['location'].split(';');
  if (!lat || !lon) {
    errors['format'] = `lat / lon not formated properly<br>${lat} / ${lon} `;
    res.errors = errors;
    app.result(res);
    return;
  }

  if (Number.isNaN(lat) || Number.isNaN(lon)) {
    errors['format'] = `lat / lon not formated properly<br>${lat} / ${lon} `;
    res.errors = errors;
    app.result(res);
    return;
  }

  try {
    res.soilGrid = await soilgrid(lat, lon);
  } catch (error) {
    errors['soilgrid'] = error;
    res.errors = errors;
    app.result(res);
    return;
  }

  const from = moment(date)
    .subtract(10, 'days')
    .format('YYYY-MM-DD');
  const to = moment(date).format('YYYY-MM-DD');
  console.log(`from ${from} to ${to}`);

  try {
    res.weather = await weather(lat, lon, from, to);
    console.log(res.weather);
  } catch (error) {
    res.errors.weather = error;
  }

  app.result(res);
})();
