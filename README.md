# Collecting data with Our Sci

Greetings and welcome to the Our Sci platform. The goal of this project is to enable everyone with motivation and a basic knowledge of software development in Javascript, to get started to build their own sensor and code to collect and visualize data samples on an Android device.

No knowledge of Android or mobile software engineering knowledge is required to use the Our Sci development environment and prototyping is designed to be quick and effective. The best part is that the platform and code scales to a production grade application that can be used by many concurrent users.

In this readme you will find generic information regarding how to acquire, process and store measurements using the Android **Our Scikit** application. The [hello world example](./hello-world/README.md) guides you through a first application from scratch.

> This guide describes how *Measurement Scripts* are developed, tested and deployed. They can then be used inside a *Survey* in order to build entire datasets for later evaluation. The following sections will focus on *Measurement Scripts* as standalone feature for gathering and displaying data on an Android device.

Measurements can be acquired from any hardware supporting either the USB-Serial or Bluetooth-Serial protocol.

The *Our Sci* Android application *(Our Scikit)* serves as container for running so called *Measurement Scripts*.

These *Measurement Scripts* are written in the Javascript language and make use of the *NodeJS* environment. They define how the Android Application interacts with an external sensor or device and are responsible for storing and visualizing measurements.

Using the Android application as container for *Measurement Scripts* allow for defining custom behaviour without having to alter the Android application and code base. Furthermore this brings the benefit of having almost instant deployment of custom scripts during development. On top of these benefits, the *Measurement Scripts* can be run and tested on a host machine using the command line and web browser.

## How Measurement Devices, Android and Scripts fit together

The vision of the *Our Sci* platform is to create tools for everyone with motivation and very basic knowledge of software engineering and some experience with electronics to create an application for collecting, sharing and evaluating data with their own instrument, sensor or device.

Let's look at the process of how this is all achieved:

1. First a device is needed to create an actual measurement, for instance this could be an Arduino connected with a sensor for pressure, humitdity, magnetic fields, a push button, an AD Converter, a spectroscipical sensor or many more.
    > *as an additional option the data can also be requested without an actual device over HTTP GET / POST requests..*

2. In order to be able to take measurements on the go, a device running Android OS is at the core of the platform. The Our Scikit Android application connects to the hardware via Bluetooth or USB OTG and fetches, processes, visualizes and stores the measurement for the user.

3. Last but not least, the data then can be exported either to 
    - the Our Sci web application, where it is archived and made accessible for later evaluation
    - the external storage of the phone as CSV where it can be later downloaded and evaluated by the user

Writing an Android application from scratch that ticks all of the above boxes is a daunting task, and also writing drivers and code to process / visualize data for each individual sensor is additional effort on top of it all.

This is where the Our Sci Platform shines, the *Our Scikit* Android application serves as a foundation to do all these things.

This way the user is able to write *Measurement Script* which defines the Android *Our Scikit* behaviour and therefore does not have to deal with anything Android related.

![Running a Measurement Script on Android](./.readme-assets/our-scikit-measurement.gif "Running a Measurement Script on Andriod")

# Before getting started with the tutorial

For understanding [the following Tutorial](./hello-world/README.md) you will need to have basic experience with *JavaScript* and *NodeJS*, as these two components build the foundation for writing code to acquire and process and visualize data from external hardware.

Furthermore, if you want to build your own first instrument, you will need the following hardware components:

- Arduino compatible device or devkit (a teensy works too)
- USB-OTG converter or BT Serial Adapter such as the HC-06 or HC-05
- Jumper Wires for your board
- Basic understanding of electrical engineering

|![HC-06 BT Serial adapter](.readme-assets/hc06.jpg)|![HC-05 BT Serial adapter](.readme-assets/hc05.jpg)| ![USB-OTG Converter](.readme-assets/usb-otg.jpg) |
|--|--|--|
| *HC-06 BT Serial adapter* | *HC-05 BT Serial adapter* | *USB-OTG Converter* |


> If you are intending to use USB OTG, make sure your phone supports USB Host Mode as well as the Android USB Host Mode API, also beware that not every USB OTG seems to work for every phone.

Alternatively, have a look at the (our-sci.net) hardware that is available to take measurements with already a large amount of *Measurement Scripts* available.


# Getting started writing Measurement Scripts

Welcome to the entry point to data acquisition and processing within the Our Sci platform. This first example will show, how data can be sent from an Arduino compatible development device and further processed inside an Android application.

The Our Sci platform supports quick hard- and software development for the purpose of collecting, processing and interpreting data of measurements. As a key feature, "measurement scripts" allow for rapid prototyping and at the same time keeping processing platform independant.

## What is a "Measurement Script"?

The goal of "measurement scripts" is to describe to the Android application how to acquire data from a source (for instance an Arduino board with a sensor in this case). Once the data has been received by the "sensor" part of the script, it is then passed to the "processor" part, which visualizes the data to the user.

This measurement script is executed inside the Android application. Therefore with the Our Sci platform, it is not necessary to write or modify an Android application for the sole purpose of acquiring, processing and storing arbitrary data collected from external hardware.

### Sensor Script (sensor.js)

The sensor script is the first entry point into a measurement. It's sole purpose is to read out data, store it within a JS JSON object and tell the Android application when it is finished, passing the result with `app.result(data)`. The contents of the `data` object passed, will then be stored into a file, so that it remains available for later processing.

The stored `data` onbject serves as the basis for the processor script `processor.js`.


### Processor Script (processor.js)

The processor script has the task of visualizing and computing results from the data stored by the `sensor.js` script. Therefore it is **stateless**.

Attached to the processor script is a WebView on Android, allowing for full customization of visualizing data. Possibilites range from a simple graph to a custom dashboard like experience, allowing for further user interaction.

### Manifest, adding meta information

In order for the user to see what the script does and how it is called, the `manifest.json` file allows to define additional information to the measurement script and builds the third and last pillar beside `sensor.js` and `processor.js`.

```js
{
    // the ID which is used inside other components such as surveys
    // in order to reference the measurement script
    "id" : "hello-simple-world",

    // The name which is used to display the script
    "name" : "A Hello World Script",

    // The description for visualization
    "description" : "Simple Script that connects to an Arduino to 
    acquire a measurement",

    // a version to distinguish updates
    "version" : "1.0",

    // the text for the button in the Android app
    // which starts the sensor.js script
    "action" : "Run Measurement",

    // wether or not the sensor scripts prompts the user
    // to first connect to a device before running
    // (for instance, to do a POST or GET request to acquire data
    // a device connection is not necessary)
    "requireDevice" : "true"
}
```

### Example for a sensor and processor script

Let's take a look at a possible example for a `measurement script` consisting of `sensor.js` and `processor.js`.

*Measuring RMS (root mean square) of a Volatage Signal with an Arduino*

Use the Arduino to sample Voltage at an ADC input, calculate RMS and display it / save it to a measurement and survey.

*Tasks of the *sensor script*, this is only pseudo code*

```js
// write some data to serial in order to tell the Arduino to start measureing
serial.write("start");

// read from serial and parse data as JSON
// contents would be something like this {"samples" : [0, 1023, 500, 720 ...]}
var data = serial.readJson();


// tell the application that we are done
// and store the output

app.result(data);
```

The collected data samples are now stored inside a file and made accessible in the processor.

*Task of the *processor script*, again only pseudo code*

```js
// First show a plot with the samples inside the webview
addPlot(result.samples);

// Now process and calculate RMS
var rms = calculateRMS(result.data);

// Display value inside the webview
info("The RMS is "+rms);

// export the value into a CSV for later download
app.csv("rms", rms);

// tell the Android app we are done
app.save();
```

To see the full example of the code, have a look at the [hello world tutorial](./hello-world/README.md) and try it out yourself!


