[{"environmental_array":[["digital_write",3,1],["analog_write",4,30000,16,6300,16,0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]},{"environmental_array":[["analog_write",4,30000,16,0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]}]


[{"environmental_array":[["digital_write",3,0],["analog_write",4,30000,16,6300,16,0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]},{"environmental_array":[["analog_write",4,30000,16,0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]}]


// save values to the eeprom
[{"save": [[2, "5.15"]]}] // save to userdef[2] the value "5.15"

// recall values to the eeprom
[{"recall": [[2]]}] // recalls userdef[2] with value "5.15"
// returns {"recall": {"2": 5.150000} in json output

// for weight, save slope in userdef[0] and yint in userdef[1]

// get raw weight from scale
[{"environmental_array":[["weight_raw"]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]}]

// get calibrated weight from scale using userdef[0] for slope, and userdef[1] for yint
[{"environmental_array":[["weight"]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]}]

// get calibrated weight until its hit a predetermined value (5 below), with timout (10,000ms below)
[{"environmental_array":[["weight_until", 5, 10000]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]}]

// analog_write now includes a pin to stop, and a high/low (0/1) setting to stop on.
// used for the limit switches.
[{"environmental_array":[["analog_write", 4, 30000, 16, 0, 16, 0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]}]




[{"environmental_array":[["digital_write",3,0],["analog_write",4,30000,16,6300,15,0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]},{"environmental_array":[["digital_write",3,0],["analog_write",4,30000,16,666]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]},{"environmental_array":[["co2"]],"pulses":[2],"pulse_distance":[6000000],"pulsed_lights":[[1]]},{"environmental_array":[["digital_write",3,1],["analog_write",4,30000,16,6300,16,0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]},{"environmental_array":[["analog_write",4,30000,16,0]],"pulses":[1],"pulse_distance":[1500],"pulsed_lights":[[1]]}]
