/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

// import {
//   sprintf
// } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
// import * as MathMore from './lib/math';

(() => {

  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.status !== 'ok') {
    ui.error('error', 'unable to update firmware');
  } else {
    ui.info('success updating firmware', '');
    ui.info('fw len', result.len);
  }

  app.save();
})();