/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import moment from 'moment';
import _ from 'lodash';
import mathjs from 'mathjs';

import serial from './lib/serial';
import app from './lib/app';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import * as utils from './lib/utils'; // use: await sleep(1000);
import soilgrid from './lib/soilgrid';
import weather from './lib/weather';
import oursci from './lib/oursci';

export default serial; // expose this to android for calling onDataAvailable

const res = {};

function errorExit(error) {
  app.result({
    error,
  });
}

const requiredAnswers = [
  'EPA',
  'land_total',
  'land_maize',
  'cattle_number',
  'cattle_manure',
  'goats_number',
  'goat_manure',
  'pigs_number',
  'pig_manure',
  'chickens_manure',
  'bags_fertilizer',
  'fertilizer_types',
  'fertilizer_timing',
  'Labor',
  'field1_name',
  'field1_size',
  'field1_soil',
  'field1_drainage',
  'field1_slope',
  'field1_previous_crop',
  'field1_yield',
  'field1_tillage',
  'field1_residue',
  'field2_name',
  'field2_size',
  'field2_soil',
  'field2_drainage',
  'field2_slope',
  'field2_previous_crop',
  'field2_yield',
  'field2_tillage',
  'field2_residue',
  'field3_name',
  'field3_size',
  'field3_soil',
  'field3_drainage',
  'field3_slope',
  'field3_previous_crop',
  'field3_yield',
  'field3_tillage',
  'field3_residue',
];

(async () => {
  const errors = {};

  requiredAnswers.forEach((a) => {
    const v = app.getAnswer(a);
    if (!v) {
      errorExit(`Answer for ${a} missing`);
    }
    res[a] = v;
  });

  app.result(res);
})();
