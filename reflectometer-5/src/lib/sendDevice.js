/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

export default () => {
  const calibrateAllWhite = [{
      calibration: 1,
      averages: 3,
      //      pulses: [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10],
      pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
      data_type: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
      pulse_length: [
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
      ],
      pulse_distance: [
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
      ],
      pulsed_lights: [
        [1],
        [2],
        [3],
        [4],
        [5],
        [6],
        [7],
        [8],
        [9],
        [10],
        [1],
        [2],
        [3],
        [4],
        [5],
        [6],
        [7],
        [8],
        [9],
        [10],
      ],
      pulsed_lights_brightness: [
        [35],
        [50],
        [550],
        [680],
        [500],
        [300],
        [875],
        [925],
        [250],
        [65],
        [35],
        [50],
        [550],
        [680],
        [500],
        [300],
        [875],
        [925],
        [250],
        [65],
      ],
      detectors: [
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
      ],
    },
    {
      environmental: [
        ['temperature_humidity_pressure_voc'],
      ],
    },
  ];

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)
  const calibrateAllBlack = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlack[0]['calibration'] = 2;

  const calibrateAllBlank = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlank[0]['calibration'] = 3;
  calibrateAllBlank[0]['data_type'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  // calibrateAllBlank[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];

  const calibrateAllWhiteMaster = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllWhiteMaster[0]['calibration'] = 4;
  calibrateAllWhiteMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  const calibrateAllBlackMaster = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlackMaster[0]['calibration'] = 5;
  calibrateAllBlackMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  const standard = [{
      averages: 3,
      pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
      pulse_length: [
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
      ],
      pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
      pulsed_lights: [
        [3],
        [4],
        [5],
        [9],
        [6],
        [7],
        [8],
        [1],
        [2],
        [10],
      ],
      pulsed_lights_brightness: [
        [550],
        [680],
        [500],
        [250],
        [300],
        [875],
        [925],
        [35],
        [50],
        [65],
      ],
      detectors: [
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [0],
        [0],
        [0],
      ],
    },
    {
      environmental: [
        ['temperature_humidity_pressure_voc'],
      ],
    },
  ];

  const testIntensity = [{
      calibration: 7,
      data_type: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
      show_voltage: 1,
      //      averages: 3,
      pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
      pulse_length: [
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
      ],
      pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
      pulsed_lights: [
        [3],
        [3],
        [3],
        [3],
        [4],
        [4],
        [4],
        [4],
        [5],
        [5],
        [5],
        [5],
        [9],
        [9],
        [9],
        [9],
        [6],
        [6],
        [6],
        [6],
        [7],
        [7],
        [7],
        [7],
        [8],
        [8],
        [8],
        [8],
        [1],
        [1],
        [1],
        [1],
        [2],
        [2],
        [2],
        [2],
        [10],
        [10],
        [10],
        [10],
      ],
      // after evaluation of actual range, calculated range is this...
      //       360,475,591,707
      // 579,637,695,753
      // 513,540,566,593
      // 208,234,261,287
      // 281,292,303,314
      // 847,862,876,891
      // 827,875,923,971
      // 29,32,35,38
      // 38,45,51,58
      // 56,61,66,71

      pulsed_lights_brightness: [
        [360],
        [475],
        [591],
        [707],
        [579],
        [637],
        [695],
        [753],
        [513],
        [540],
        [566],
        [593],
        [208],
        [234],
        [261],
        [287],
        [260],
        [280],
        [300],
        [320],
        [847],
        [862],
        [876],
        [891],
        [827],
        [875],
        [923],
        [971],
        [29],
        [32],
        [35],
        [38],
        [45],
        [48],
        [51],
        [58],
        [56],
        [61],
        [66],
        [71],
        // [510],
        // [530],
        // [550],
        // [570],
        // [640],
        // [660],
        // [680],
        // [700],
        // [460],
        // [480],
        // [500],
        // [520],
        // [230],
        // [240],
        // [250],
        // [260],
        // [260],
        // [280],
        // [300],
        // [320],
        // [825],
        // [850],
        // [875],
        // [900],
        // [875],
        // [900],
        // [925],
        // [950],
        // [33],
        // [34],
        // [35],
        // [36],
        // [46],
        // [48],
        // [50],
        // [52],
        // [55],
        // [60],
        // [65],
        // [70],
      ],
      detectors: [
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
      ],
    },
    {
      environmental: [
        ['temperature_humidity_pressure_voc'],
      ],
    },
  ];

  const testIntensityIr = JSON.parse(JSON.stringify(testIntensity));
  testIntensityIr[0].data_type.splice(0, 28);
  testIntensityIr[0].pulses.splice(0, 28);
  testIntensityIr[0].pulse_length.splice(0, 28);
  testIntensityIr[0].pulse_distance.splice(0, 28);
  testIntensityIr[0].pulsed_lights.splice(0, 28);
  testIntensityIr[0].pulsed_lights_brightness.splice(0, 28);
  testIntensityIr[0].detectors.splice(0, 28);

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)

  const standardBlank = JSON.parse(JSON.stringify(standard));
  standardBlank[0]['data_type'] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

  const calibrateBlankLocal = JSON.parse(JSON.stringify(standard));
  calibrateBlankLocal[0]['calibration'] = 6;

  const standardApplyCalibration = JSON.parse(JSON.stringify(standard));
  standardApplyCalibration[0]['recall'] = [];
  for (let i = 0; i < 31; i++) {
    standardApplyCalibration[0]['recall'].push('userdef[' + i + ']');
  }

  const standardRaw = JSON.parse(JSON.stringify(standard));
  standardRaw[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
  standardRaw[0]['show_voltage'] = 1;

  const standardRawNoHeat = JSON.parse(JSON.stringify(standard));
  standardRawNoHeat[0]['data_type'] = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3];
  standardRawNoHeat[0]['show_voltage'] = 1; // make sure that voltage is outputted

  const standardHeatTest = JSON.parse(JSON.stringify(standardRaw));
  standardHeatTest[0]['show_voltage'] = 1; // make sure that voltage is outputted
  standardHeatTest[0]['protocols'] = 12;
  standardHeatTest[0]['protocols_delay'] = 240000;
  standardHeatTest[0]['environmental'] = [ // take temp measurements every time (not just at the end)
    ['temperature_humidity_pressure_voc'],
  ];
  standardHeatTest.splice(1, 1); // get rid of temp measurement at the end since now we're taking it every time.

  const standardRawHeatCal = JSON.parse(JSON.stringify(standardRawNoHeat));
  standardRawHeatCal[0]['protocols'] = 12;
  standardRawHeatCal[0]['protocols_delay'] = 240000;
  standardRawHeatCal[0]['environmental'] = [ // take temp measurements every time (not just at the end)
    ['temperature_humidity_pressure_voc'],
  ];
  standardRawHeatCal.splice(1, 1); // get rid of temp measurement at the end since now we're taking it every time.

  const environmental_only = [{
    environmental: [
      ['temperature_humidity_pressure_voc'],
    ],
  }];

  const oldReflectometer2_flat = [{
    environmental: [
      ['temperature_humidity_pressure_voc'],
    ],
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
    ],
    pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
    pulsed_lights: [
      [3],
      [4],
      [5],
      [9],
      [6],
      [7],
      [8],
      [1],
      [2],
      [10],
    ],
    pulsed_lights_brightness: [
      [550],
      [1200],
      [700],
      [250],
      [450],
      [1200],
      [1250],
      [40],
      [80],
      [65],
    ],
    detectors: [
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [0],
      [0],
    ],
    dac_lights: 1,
    averages: 5,
    object_type: 'object',
    calibration: 'no',
  }];

  // old  const resetAll = 'set_ir_high+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_ir_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_vis_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_user_defined+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+11+0+12+0+13+0+14+0+15+0+16+0+17+0+18+0+19+0+20+0+21+0+22+0+23+0+24+0+25+0+26+0+27+0+28+0+29+0+30+0+31+0+32+0+33+0+34+0+35+0+36+0+37+0+38+0+39+0+40+0+41+0+42+0+43+0+44+0+45+0+46+0+47+0+48+0+49+0+50+0+51+0+52+0+53+0+54+0+55+0+56+0+57+0+58+0+59+0+60+0+61+0+62+0+63+0+64+0+65+0+66+0+67+0+68+0+69+0+70+0+-1+set_ir_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal1+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_heatcal2+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal3+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_ir_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+print_memory+';
  const resetAll = 'set_ir_high+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_ir_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_vis_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_user_defined+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+11+0+12+0+13+0+14+0+15+0+16+0+17+0+18+0+19+0+20+0+21+0+22+0+23+0+24+0+25+0+26+0+27+0+28+0+29+0+30+0+31+0+32+0+33+0+34+0+35+0+36+0+37+0+38+0+39+0+40+0+41+0+42+0+43+0+44+0+45+0+46+0+47+0+48+0+49+0+50+0+51+0+52+0+53+0+54+0+55+0+56+0+57+0+58+0+59+0+60+0+61+0+62+0+63+0+64+0+65+0+66+0+67+0+68+0+69+0+70+0+-1+set_ir_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal1+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_heatcal2+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal3+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_ir_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+print_memory+';
  const configure_bluetooth = 'configure_bluetooth';
  const configure_bluetooth_115200 = 'configure_bluetooth';
  const set_device_info = 'set_device_info+';

  return {
    calibrateAllWhite,
    calibrateAllBlack,
    calibrateAllBlank,
    // calibrateAllWhiteMaster,
    // calibrateAllBlackMaster,
    testIntensity,
    testIntensityIr,
    standard,
    standardBlank,
    standardRaw,
    standardRawNoHeat,
    standardRawHeatCal,
    standardHeatTest,
    standardApplyCalibration,
    calibrateBlankLocal,
    environmental_only,
    resetAll,
    configure_bluetooth,
    configure_bluetooth_115200,
    set_device_info,
    oldReflectometer2_flat,
  };
};