(async () => {
  if (!serverId || !fieldId) {
    errorExit(
      `Cannot parse fieldId and FarmOS server, answer is: ${fieldAnswer}`
    );
  }
  appendResult(
    "FarmOS IDs",
    `using field ID ${fieldId} and server ID ${serverId}`
  );

  let amendmentAnswer;

  try {
    amendmentAnswer = formparser.weeklyAmendmentParser(app);
  } catch (error) {
    errorExit(`error parsing, ${error.message}`);
  }

  const weeksAgo = app.getAnswer("which_week");

  const weeksMap = {
    this_week: 0,
    last_week: 1,
    "2_weeks_ago": 2,
    "3_weeks_ago": 3,
    "1_month": 4
  };

  const weeks = weeksMap[weeksAgo];
  if (weeks === undefined) {
    errorExit(
      `unable to map amount of weeks: ${weeksAgo}, choices are (${Object.keys(
        weeksMap
      ).join(",")})`
    );
  }

  const meta = app.getMeta();
  const started = moment.unix(Number.parseFloat(meta.started) / 1000);
  if (weeks > 0) {
    started.subtract(weeks, "week");
  }

  appendResult("Estimated Date", started.format("YYYY-MM-DD"));

  app.progress(10);

  console.log(`serverId: ${serverId}`);
  console.log(`plantingId: ${plantingId}`);

  const {
    url,
    username,
    password,
    farmosCookie,
    farmosToken
  } = app.getCredentials(serverId);

  // appendResult('Authenticating', `Authenticating on farmos with url ${url}`);
  app.progress(20);

  let farm;
  try {
    farm = await farmos(url, username, password, farmosToken, {
      configInstanceId: meta.instanceId,
      configPlantingId: plantingId,
      configClearEntrieswithInstanceId: true
    });
  } catch (error) {
    throw Error(
      `unable to identify planting, check credentials / server information: ${
        error.message
      }`
    );
  }

  app.progress(40);
  appendResult("Authentication", "success");

  const managementPractices = app.getAnswer("management_practices");
  let management = [];
  if (managementPractices) {
    management = managementPractices.split(" ");
    if (management.includes("irrigation")) {
      const rate = app.getAnswer("irrigation/rate");
      const type = app.getAnswer("irrigation/type");
      const irrigation = await farm.submitIrrigation(
        started.format("YYYY-MM-DD"),
        rate,
        type
      );
      appendResult(
        "Irrigation",
        `submitted irigation to farmos with id: ${irrigation.id}`
      );
    }
  }

  
      const inputs = app.getAnswer('amendments/amendments_type');
      let amendments;
      console.log(inputs);
      if (inputs) {
        amendments = inputs.split(' ');
        for (let i = 0; i < amendments.length; i++) {
          console.log(`submitting ${amendments[i]}`);
          const a = await farm.submitSimpleAmendment(amendments[i], null, started.unix());
          appendResult(
            `Amendment ${i + 1}`,
            `submitted amendment (${amendments[i]}) to farmos,  with id ${a.id}`,
          );
        }
      }
  
  
      for (let i = 0; i < amendmentAnswer.length; i++) {
        const a = amendmentAnswer[i];
  
        let type = a.type;
        const amendmentControl = app
          .getMeta()
          .controls.find(c => c.name === `/data/amendments/amendment_type_${i + 1}:label`);
        console.log(amendmentControl);
  
        if (amendmentControl) {
          const c = amendmentControl.options.find(o => o.value === type);
          type = c ? c.text : type;
        }
  
        console.log(`type is: ${type}`);
  
        const amendment = await farm.submitAmendment(
          a.name,
          null,
          started.unix(),
          a.method,
          type,
          a.nutrients,
          a.n,
          a.p,
          a.k,
          a.trace,
        );
        appendResult(
          `Amendment ${i + 1}`,
          `submitted amendment (${a.name}) to farmos,  with id ${amendment.id}`,
        );
      }
  
      const weedControlType = app.getAnswer('weed_control/weed_control_type');
      if (weedControlType) {
        const activities = weedControlType.split(' ');
        let hasHerbicide = false;
        for (let i = 0; i < activities.length; i++) {
          const a = activities[i];
          if (a === 'herbicide') {
            hasHerbicide = true;
            continue;
          }
  
          const activity = await farm.submitSimpleActivity(a, null, started.unix());
          appendResult(
            `Weed Control ${i + 1}`,
            `submitted weed control (${a}) to farmos, with id ${activity.id}`,
          );
        }
  
        if (hasHerbicide) {
          const rate = app.getAnswer('weed_control/herbicide_rate');
          const type = app.getAnswer('weed_control/herbicide_type');
          const h = await farm.submitHerbicide(started.unix(), null, type, rate);
          appendResult(
            'Application of Herbicide',
            `submitted herbicide (${type}) to farmos, with id ${h.id}`,
          );
        }
      }
  
      const pressure = app.getAnswer('pest_disease_scouting/pest_disease_pressure');
      if (pressure) {
        const p = await farm.submitDiseasePressure(started.unix(), null, pressure);
      }
  
      const reason = app.getAnswer('pest_disease_control/pest_disease_reason');
      if (reason) {
        const typeAnswer = app.getAnswer('pest_disease_control/pest_disease_type');
        if (typeAnswer) {
          let types = [];
          try {
            types = formparser
              .getItemLabels('pest_disease_control/pest_disease_type')
              .filter(i => i.toLowerCase() !== 'hand-picking/pruning');
          } catch (error) {
            appendResult('Pest Control Types', 'unable to extract pest control types, ignoring.');
          }
  
          const rate = app.getAnswer('pest_disease_control/pest_disease_rate');
          const name = `Pest / Disease Control due to ${reason}`;
  
          const d = {
            methods: types,
            pest_disease_controlled_1: app.getAnswer(
              'pest_disease_control/pest_disease_controlled_1',
            ),
            pest_disease_controlled_2: app.getAnswer(
              'pest_disease_control/pest_disease_controlled_2',
            ),
            pest_disease_controlled_3: app.getAnswer(
              'pest_disease_control/pest_disease_controlled_3',
            ),
          };
  
          const pruning = typeAnswer.includes('hand_removal');
  
          const h = await farm.submitPestDiseaseControl(
            started.unix(),
            name,
            types,
            pruning,
            rate,
            null,
            d,
          );
          appendResult(
            'Pest / Disease Control',
            `submitted control (${name}) to farmos, with id ${h.id}`,
          );
        }
      }
  
      if (management && management.includes('harvest')) {
        const harvest = {
          weightUnit: app.getAnswer('harvest_group/harvest_weight_units'),
          weight: Number.parseFloat(app.getAnswer('harvest_group/harvest_weight')),
          areaUnit: app.getAnswer('harvest_group/harvest_area_units'),
          area: Number.parseFloat(app.getAnswer('harvest_group/harvest_area')),
        };
  
        let plantDensity;
        const densityAnswer = app.getAnswer('harvest_group/plant_density');
        const rowSpacingUnit = app.getAnswer('harvest_group/row_spacing_unit');
  
        if (densityAnswer) {
          plantDensity = {
            name: 'Plant Density',
            type: 'count',
            value: Number.parseFloat(densityAnswer),
            unit: `plants per ${harvest.areaUnit}`,
          };
        } else if (rowSpacingUnit) {
          const rowSpacing = app.getAnswer('harvest_group/row_spacing');
          plantDensity = {
            name: 'Row Spacing',
            type: 'length',
            value: Number.parseFloat(rowSpacing),
            unit: rowSpacingUnit,
          };
        }
  
        if (harvest.weightUnit && harvest.weight && harvest.area && harvest.areaUnit) {
          const density = harvest.weight / harvest.area;
          appendResult('Harvest Density', `${density} ${harvest.weightUnit} per ${harvest.areaUnit}`);
          const quantities = {
            area: {
              value: harvest.area,
              unit: harvest.areaUnit,
            },
            weight: {
              value: harvest.weight,
              unit: harvest.weightUnit,
            },
            density: {
              value: density,
              unit: `${harvest.weightUnit} per ${harvest.areaUnit}`,
            },
          };
  
          if (plantDensity) {
            Object.assign(quantities, { plantDensity });
          }
  
          console.log('plant density: ');
          console.log(quantities);
  
          const h = await farm.submitHarvest(started.unix(), quantities);
          appendResult('Harvest', `submitted to farmos, with id ${h.id}`);
        } else {
          console.error(`missing harvest parameter${JSON.stringify(harvest)}`);
        }
      }
  
      const pruning = app.getAnswer('thin_prune_group/pruning_activity');
      if (pruning) {
        const prune = pruning
          .split(' ')
          .map((p) => {
            const pruningControl = app
              .getMeta()
              .controls.find(c => c.name === '/data/thin_prune_group/pruning_activity:label');
            const option = pruningControl.options.find(o => o.value === p);
            return option.text || '';
          })
          .join(', ');
  
        const p = await farm.submitPruning(started.unix(), prune, pruning.split(' '));
        appendResult('Pruning', `submitted to farmos, with id ${p.id}`);
      }
      app.result(result);
    } catch (error) {
      console.error(error);
      result.error = error.message;
      app.result(result);
    }
    
})();
