/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-constant-condition */

import app from './lib/app';
import serial from './lib/serial';
import sendDevice from './lib/sendDevice';
import { sleep } from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  await sleep(1000);

  const result = {};
  const count = 0;
  try {
    app.progress(0.1);
    result.type = app.getAnswer('type');
    if (!result.type) {
      result.error = 'no valid type selected';
      app.progress(1);
      app.result(result);
      return;
    }

    if (result.type === 'callibrate_max') {
      serial.write('{ "command": "callibrate_max" }\n');
    } else if (result.type === 'callibrate_min') {
      serial.write('{ "command": "callibrate_min" }\n');
    } else if (result.type === 'callibrate_short') {
      serial.write('{ "command": "callibrate_short" }\n');
    } else if (result.type === 'status') {
      serial.write('{ "command": "get_status" }\n');
    } else if (result.type === 'get_callibration') {
      serial.write('{ "command": "get_callibration" }\n');
    } else {
      result.error = 'no valid command';
      app.progress(100);
      app.result(result);
      return;
    }

    while (true) {
      try {
        const line = await serial.readLine();
        if (line.includes('command')) {
          break;
        }
      } catch (error) {
        console.log(error);
        result.error = 'timeout from device';
        app.progress(100);
        app.result(result);
        return;
      }
    }

    const start = Date.now();
    while (true) {
      try {
        result.from_device = await serial.readJson();
        break;
      } catch (error) {
        console.log(error);
      }
      if (Date.now() - start > 10000) {
        result.error = 'timeout from device';
        app.progress(100);
        app.result(result);
        return;
      }
    }

    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();
