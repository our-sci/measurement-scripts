/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

export default () => {
  const calibrateAllWhite = [{
    calibration: 1,
    averages: 3,
    //      pulses: [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10],
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    data_type: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
    ],
    pulse_distance: [
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
    ],
    pulsed_lights: [
      [1],
      [2],
      [3],
      [4],
      [5],
      [6],
      [7],
      [8],
      [9],
      [10],
      [1],
      [2],
      [3],
      [4],
      [5],
      [6],
      [7],
      [8],
      [9],
      [10],
    ],
    pulsed_lights_brightness: [
      [35],
      [80],
      [550],
      [1200],
      [600],
      [450],
      [1200],
      [1250],
      [250],
      [100],
      [35],
      [80],
      [550],
      [1200],
      [600],
      [450],
      [1200],
      [1250],
      [250],
      [100],
    ],
    detectors: [
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
    ],
  }];

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)
  const calibrateAllBlack = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlack[0]['calibration'] = 2;

  const calibrateAllBlank = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlank[0]['calibration'] = 3;
  calibrateAllBlank[0]['data_type'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  // calibrateAllBlank[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];

  const calibrateAllWhiteMaster = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllWhiteMaster[0]['calibration'] = 4;
  calibrateAllWhiteMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  const calibrateAllBlackMaster = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlackMaster[0]['calibration'] = 5;
  calibrateAllBlackMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  const standard = [{
    averages: 3,
    environmental: [
      ['temperature_humidity_pressure_voc'],
    ],
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
    ],
    pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
    pulsed_lights: [
      [3],
      [4],
      [5],
      [9],
      [6],
      [7],
      [8],
      [1],
      [2],
      [10],
    ],
    pulsed_lights_brightness: [
      [550],
      [1200],
      [600],
      [250],
      [450],
      [1200],
      [1250],
      [35],
      [80],
      [100],
    ],
    detectors: [
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [0],
      [0],
    ],
  }];

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)

  const standardBlank = JSON.parse(JSON.stringify(standard));
  standardBlank[0]['data_type'] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

  const standardRaw = JSON.parse(JSON.stringify(standard));
  standardRaw[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2];

  const standardRawHeatCal = JSON.parse(JSON.stringify(standard));
  standardRawHeatCal[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
  standardRawHeatCal[0]['protocols'] = 13;
  standardRawHeatCal[0]['protocols_delay'] = 0;

  const oldReflectometer2_flat = [{
    environmental: [
      ['temperature_humidity_pressure_voc'],
    ],
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
    ],
    pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
    pulsed_lights: [
      [3],
      [4],
      [5],
      [9],
      [6],
      [7],
      [8],
      [1],
      [2],
      [10],
    ],
    pulsed_lights_brightness: [
      [550],
      [1200],
      [700],
      [250],
      [450],
      [1200],
      [1250],
      [40],
      [80],
      [100],
    ],
    detectors: [
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [0],
      [0],
    ],
    dac_lights: 1,
    averages: 5,
    object_type: 'object',
    calibration: 'no',
  }];

  return {
    calibrateAllWhite,
    calibrateAllBlack,
    calibrateAllBlank,
    calibrateAllWhiteMaster,
    calibrateAllBlackMaster,
    standard,
    standardBlank,
    standardRaw,
    standardRawHeatCal,
    oldReflectometer2_flat,
  };
};