/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

// import { sprintf } from 'sprintf-js';
// import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  Object.keys(result.error).forEach((a) => {
    ui.error(`Answer '${a}' is ${result.error[a]}`, 'Return to the answer and fix it.');
  });

  // set the final antioxidant values to reference...
  let antioxidants = {
    value: '',
    dilution: '',
  };
  let polyphenols = {
    value: '',
    dilution: '',
  };
  let proteins = {
    value: '',
    dilution: '',
  };

  // First - let's parse out results from get-rfc-sample-data and make it available in a simpler form
  const get_chemistry = JSON.parse(get_chemistry);
  get_chemistry.data.found.sample.forEach((a, index) => {
    if (a === 'antioxidants') {
      antioxidants.value = get_chemistry.data.found.sample.antixidants[index];
      antioxidants.dilution = get_chemistry.data.found.sample.dilution[index];
    }
    if (a === 'polyphenols') {
      polyphenols.value = get_chemistry.data.found.sample.polyphenols[index];
      polyphenols.dilution = get_chemistry.data.found.sample.dilution[index];
    }
    if (a === 'proteins') {
      proteins.value = get_chemistry.data.found.sample.proteins[index];
      proteins.dilution = get_chemistry.data.found.sample.dilution[index];
    }
  });

  console.log(`anti: ${antioxidants}, poly: ${polyphenols}, prot: ${proteins}`)


  // Define parameters to calculate polyphenol and antioxidant;
  /*
  const spinach_norm = 0.914; // Fresh weight for spinach, based on USDA nutritional database;
  const carrot_norm = 0.8829; // Fresh weight for carrot, based on USDA nutritional database;
  const kale_norm = 0.9; // Need to find
  const lettuce_norm = 0.9; // Need to find
  const cherry_tom_norm = 0.9; // Need to find
  const grape_norm = 0.9; // Need to find
  */
  const poly_conv = 0.1; // needed to convert ug ml-1 to mg 100 g FW for polyphenol;

  const types = {
    carrot: {
      norm_weight: 0.883,
    },
    spinach: {
      norm_weight: 0.914,
    },
    kale: {
      norm_weight: 0.896,
    },
    lettuce: {
      norm_weight: 0.95,
    },
    cherry_tom: {
      norm_weight: 0.95,
    },
    grape: {
      norm_weight: 0.82,
    },
  };

  // Calculate final Polyphenol, Antioxidant and Protein Content;
  Object.keys(types).forEach((food) => {
    if (food === result.sample_type) {
      // Calculates moisture content;
      let moisture_content = 0;
      if (
        isNaN(result.pre_weight.data.weight_grams) ||
        isNaN(result.post_weight.data.weight_grams) ||
        isNaN(result.cup_wt.data.weight_grams)
      ) {
        moisture_content = types[food].norm_weight;
        ui.warning(
          `Weight is missing, default of ${types[food].norm_weight} is applied`,
          'A sample weight is missing, so moisture cannot be calculated.  Go back and fill in all sample weights.  If sample weights are not present, an estimated moisture of will be applied.',
        );
      } else {
        moisture_content =
          (result.pre_weight.data.weight_grams - result.post_weight.data.weight_grams) /
          (result.pre_weight.data.weight_grams - result.cup_wt.data.weight_grams);
        if (moisture_content < 0 || moisture_content > 1) {
          ui.error(
            'The moisture content was out of range',
            'Please check that the entered weights are correct, otherwise we will set the correction factor to 1',
          );
        }

        if (moisture_content < 0.6 && moisture_content > 0) {
          ui.warning(
            'The moisture content was very low',
            'If the vegetable was very dry this may be fine, otherwise please check that the entered weights are correct',
          );
        }
        ui.info('Moisture Content', MathMore.MathROUND(moisture_content, 2));
        app.csvExport('moistureContent', MathMore.MathROUND(moisture_content, 2));
      }
      /*
      const CF = types[food].norm_weight / moisture_content;
      ui.info('Correction Factor', MathMore.MathROUND(CF, 2));
      app.csvExport('correctionFactor', MathMore.MathROUND(CF, 2));
  */

      let CF = 0;
      if (moisture_content < 0 || moisture_content > 1) {
        CF = 1;
        ui.info('Correction Factor', MathMore.MathROUND(CF, 2));
        app.csvExport('correctionFactor', MathMore.MathROUND(CF, 2));
      } else {
        CF = types[food].norm_weight / moisture_content;
        ui.info('Correction Factor', MathMore.MathROUND(CF, 2));
        app.csvExport('correctionFactor', MathMore.MathROUND(CF, 2));
      }

      if (
        result.polyphenols !== 'undefined' &&
        typeof result.polyphenols.data.output !== 'undefined'
      ) {
        const polyphenols =
          ((result.polyphenols.data.output * result.extractant) /
            result.sample_weight.data.weight_grams) *
          poly_conv *
          CF *
          result.dilution.data.dilution;
        ui.info('Total Polyphenols mg GAE 100g FW', MathMore.MathROUND(polyphenols, 2));
        app.csvExport('polyphenolsMgGae100gFw', MathMore.MathROUND(polyphenols, 2));
      } else {
        ui.error(
          'Polyphenols missing or undefined',
          'Check rfclab environmental variable, make sure it is set to 1',
        );
      }

      if (
        result.antioxidants !== 'undefined' &&
        typeof result.antioxidants.data.output !== 'undefined'
      ) {
        const antioxidants =
          ((result.antioxidants.data.output * result.extractant) /
            result.sample_weight.data.weight_grams) *
          CF *
          result.dilution.data.dilution;
        ui.info('Total Antioxidants FRAP value', MathMore.MathROUND(antioxidants, 2));
        app.csvExport('antioxidentsFrap', MathMore.MathROUND(antioxidants, 2));
      } else {
        ui.error(
          'Antioxidants missing or undefined',
          'Check rfclab environmental variable, make sure it is set to 1',
        );
      }
    }
  });
  app.save();
})();