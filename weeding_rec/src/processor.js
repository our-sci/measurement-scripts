/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */
/* eslint-disable linebreak-style */
/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  const weeding_intensity = {};
  if (result.weeding_intensity == '0' || result.weeding_intensity == '1') {
    ui.info(
      'It is best to keep fields weed free by weeding 2 to 3 times during the season. Fertilizer should be used only in combination with good weeding. Fertilizer is expensive. Do not let weeds to compete for fertilizer and water with your maize',
    );
  } else if (result.weeding_intensity == '2' || result.weeding_intensity == '3') {
    ui.info('Great Job!');
    ui.info(
      'Fertilizer should be used only in combination with good weeding. Fertilizer is expensive. Do not let weeds to compete for fertilizer and water with your maize',
    );
  }

  app.save();
})();
