import { err, survey } from './sensor';

function seqCheck(seq) {
  seq.forEach((s) => {
    if (s === null || s === undefined || s === '') {
      err(`Missing answer for: ${s}`);
    }
  });
}

function sel(name, eq = 'yes') {
  if (survey[name] && survey[name].toLowerCase() === eq.toLowerCase()) {
    return true;
  }
  return false;
}

function checkCashCropPlanting() {
  seqCheck([
    'cash_planting_group/cash_crop1',
    'cash_planting_group/cultivar1',
    'cash_planting_group/seed_gmo1',
  ]);

  for (let i = 1; i < 4; i++) {
    const k = `cash_planting_group/seed_treatment${i}`;
    const treatment = survey[k];
    if (treatment) {
      if (treatment.split(' ').includes('other')) {
        seqCheck([`cash_planting_group/seed_treatement_other${i}`]);
      }
    }
  }

  if (sel(['cash_planting_group/crop2'])) {
    seqCheck([
      'cash_planting_group/cash_crop2',
      'cash_planting_group/cultivar2',
      'cash_planting_group/seed_gmo2',
    ]);
  }

  if (sel(['cash_planting_group/crop3'])) {
    seqCheck([
      'cash_planting_group/cash_crop3',
      'cash_planting_group/cultivar3',
      'cash_planting_group/seed_gmo3',
    ]);
  }
}

function checkWeedControl() {
  seqCheck(['weed_control/weed_control_type']);
  const weedControl = survey['weed_control/weed_control_type'];
  const w = weedControl.split(' ');

  if (w.includes('herbicide')) {
    seqCheck([
      'weed_control/herbicide_type',
      'weed_control/herbicide_name1',
      'weed_control/herbicide_units1',
      'weed_control/herbicide_rate1',
    ]);

    if (sel('weed_control/herbicide2')) {
      seqCheck([
        'weed_control/herbicide_name2',
        'weed_control/herbicide_units2',
        'weed_control/herbicide_rate2',
      ]);
    }
  }

  if (w.includes('cultivation')) {
    seqCheck(['weed_control/type_1', 'weed_control/depth_1']);

    if (sel('weed_control/tillage_2')) {
      seqCheck(['weed_control/type_2', 'weed_control/depth_2']);
    }

    if (sel('weed_control/tillage_3')) {
      seqCheck(['weed_control/type_3', 'weed_control/depth_3']);
    }
  }
}

function checkAmendments() {
  seqCheck([
    'amendments/amendments_type1',
    'amendments/name_1',
    'amendments/units_1',
    'amendments/rate_1',
    'amendments/application-method-1',
    'amendments/nutrients_1',
  ]);

  const nutrients1 = survey['amendments/nutrients_1'].split(' ');
  seqCheck(nutrients1.map(n => `amendments/${n.toLowerCase()}_1`));

  if (survey['amendments/nutrients_1'].includes('other')) {
    seqCheck(['amendments/nutrients_trace_1']);
  }

  for (let i = 2; i < 5; i++) {
    if (sel(`amendments/amendment_${i}`)) {
      seqCheck([
        `amendments/amendments_type${i}`,
        `amendments/name_${i}`,
        `amendments/units_${i}`,
        `amendments/rate_${i}`,
        `amendments/application-method-${i}`,
        `amendments/nutrients_${i}`,
      ]);

      const nutrients = survey[`amendments/nutrients_${i}`].split(' ');
      seqCheck(nutrients.map(n => `amendments/${n.toLowerCase()}_${i}`));

      if (survey[`amendments/nutrients_${i}`].includes('other')) {
        seqCheck([`amendments/nutrients_trace_${i}`]);
      }
    }
  }
}

function checkPestDiseaseControl() {
  seqCheck([
    'pest_disease_control_group/pest_disease_reason',
    'pest_disease_control_group/pest_disease_type1',
    'pest_disease_control_group/pest_disease_controlled_1',
    'pest_disease_control_group/pest_disease_product1',
    'pest_disease_control_group/pest_disease_units1',
    'pest_disease_control_group/pest_disease_rate1',
  ]);

  if (sel('pest_disease_control_group/pest_disease2')) {
    seqCheck([
      'pest_disease_control_group/pest_disease_type2',
      'pest_disease_control_group/pest_disease_controlled_2',
      'pest_disease_control_group/pest_disease_product2',
      'pest_disease_control_group/pest_disease_units2',
      'pest_disease_control_group/pest_disease_rate2',
    ]);
  }
}

function checkCoverCrop() {
  seqCheck([
    'cover_crop_group/covercrop_diversity',
    'cover_crop_group/covercrop_species',
    'cover_crop_group/covercrop_seeding_method',
    'cover_crop_group/covercrop_seed_cost',
    'cover_crop_group/covercrop_planting_cost',
    'cover_crop_group/covercrop_termination',
    'cover_crop_group/covercrop_termination_date',
  ]);
}

function checkHarvest() {
  seqCheck([
    'harvest_group/crop_harvest1',
    'harvest_group/crop_units1',
    'harvest_group/crop_yield1',
    'harvest_group/preharvest_desiccant',
    'harvest_group/harvest_cost',
    'harvest_group/cleaning_other_cost',
    'harvest_group/residue_removal',
  ]);

  if (sel('harvest_group/residue_removal')) {
    seqCheck(['harvest_group/residue_removal_method']);
  }

  for (let i = 2; i < 10; i++) {
    if (!sel(`harvest_group/crop_harvest${i}`)) {
      continue;
    }
    seqCheck([
      `harvest_group/crop_harvest${i}`,
      `harvest_group/crop_units${i}`,
      `harvest_group/crop_yield${i}`,
    ]);
  }
}

const management = {
  cash_crop_planting: checkCashCropPlanting,
  amendment: checkAmendments,
  weed_control: checkWeedControl,
  pest_disease_control: checkPestDiseaseControl,
  cover_crop_planting: checkCoverCrop,
  harvest: checkHarvest,
};

export default function errorCheck() {
  if (!survey.which_week) {
    err('Which week not answered');
  }

  if (!survey.management_practices) {
    err('Management Practices not selected');
  }

  const practices = survey.management_practices.split(' ');
  Object.keys(management).forEach((m) => {
    if (practices.includes(m)) {
      management[m](err);
    }
  });
}
