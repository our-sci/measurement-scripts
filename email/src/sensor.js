import { app } from '@oursci/scripts';

(async () => {
  app.email('someone@our-sci.net', 'Hello from script', 'Hope you enjoy the email feature!');
  app.result({ status: 'done' });
})();
