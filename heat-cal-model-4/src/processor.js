/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf,
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  // ///////////////////////////////////////////////////////
  // create fit data for the temperature and median response, display the results, save them to CSV, and prepare them to be saved back to the device
  ui.info(
    'Heat Calibration Information',
    'In order to account for the effects of heat on the light intensity of the device, a correction factor is applied.  This measurement calculates that correction factor for each light, and saves the correction factor to the device.  The correction factors are based on measuring the voltage supplied to the LED, which varies inversely with the temperature because the circuit is current controlled (so intensity ~ voltage * current... we know that intensity varies with temperature, and we know that current is constant... so we can estimate the temperature change by measuring voltage.  This is intended to address short term temperature effects.  Impacts of intensity like LED degradation, scratches in the light guide, etc should be addressed by recalibrating the device using a calibration card.',
  );

  // use midpoint of all values as normalization point
  // if you re-calibrate, the normalization point can change, it has no effects because the calibration card forces everything to 1 and 0 anyway
  // this calibration will apply only to these intensities - changing the intensity requires a calibration at that intensity.
  // it appears to be a 2nd order poly fit.

  const regData = [];
  const regFit = [];
  const temperatureData = [];
  const voltageData = [];
  const reflectanceData = [];
  const predictedData = [];
  const correctedData = [];
  const slopesNormalized = [];
  const slopes = [];
  const data = result.measurement.data;
  const protocols = Number(result.measurement.data.protocols);
  //  const wavelengths = result.sample[0].processed.wavelengths; // we use this a lot below, so declare here to make things less ugly.
  //  const wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940];
  const wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940];
  const wavelengths_range = [280000000, 100000000, 260000000, 230000000, 260000000, 320000000, 240000000, 230000000, 170000000, 260000000]; // estimate of wavelength range - not absolute, based on experience, helps provide rough estimate of noise caused by temperature changes
  const ledNumber = [3, 4, 5, 9, 6, 7, 8, 1, 2, 10];
  let toDevice_heatcal1 = 'set_heatcal1+';
  let toDevice_heatcal2 = 'set_heatcal2+';
  let toDevice_heatcal3 = 'set_heatcal3+';
  let toDevice_heatcal4 = 'set_heatcal4+';
  let toDevice_heatcal5 = 'set_heatcal5+';
  let toDevice_heatcal6 = 'set_heatcal6+';
  let range = '';
  let theError = '';
  //  const toDevice_heatcal4 = 'set_heatcal4+';
  //  const toDevice_heatcal5 = 'set_heatcal5+';
  // loop through all of the wavelengths to run regressions, display, and save to csv
  for (let z = 0; z < wavelengths.length; z++) {
    regData.push([]);
    voltageData.push([]);
    temperatureData.push([]);
    reflectanceData.push([]);
    predictedData.push([]);
    correctedData.push([]);
    slopes.push([]);
    slopesNormalized.push([]);
    // loop through the number of measurements collected to put into the heat correction model
    for (let i = 1; i <= protocols; i++) {
      const voltage = data[`voltage_${wavelengths[z]}.${i - 1}`];
      const detector = data[`median_${wavelengths[z]}.${i - 1}`];
      const temperature = MathMore.MathROUND(data[`temperature.${i - 1}`], 0);
      regData[z].push([Number(voltage), Number(detector)]);
      voltageData[z].push(Number(voltage));
      temperatureData[z].push(temperature);
      reflectanceData[z].push(Number(detector));
    }
    // run the model (linear regression)
    /*
        regression = {
          m,
          b,
          r,
          r2: r * r
        };
    */
    const regResults1 = MathMore.MathPOLYREG(regData[z], 1);
    const regResults = MathMore.MathPOLYREG(regData[z], 2);
    const regResults3 = MathMore.MathPOLYREG(regData[z], 3);
    const regResults4 = MathMore.MathPOLYREG(regData[z], 4);
    const regResults5 = MathMore.MathPOLYREG(regData[z], 5);
    const regResults6 = MathMore.MathPOLYREG(regData[z], 6);
    const regResults7 = MathMore.MathPOLYREG(regData[z], 7);
    //    /*
    ui.info(`${wavelengths[z]}: <br>
        ${MathMore.MathROUND(regResults1.rsquared, 3)}<br>
        ${MathMore.MathROUND(regResults.rsquared, 3)}<br>
        ${MathMore.MathROUND(regResults3.rsquared, 3)}<br>
        ${MathMore.MathROUND(regResults4.rsquared, 3)}<br>
        ${MathMore.MathROUND(regResults5.rsquared, 3)}<br>
        ${MathMore.MathROUND(regResults6.rsquared, 3)}<br>
        ${MathMore.MathROUND(regResults7.rsquared, 3)}`);
    //        */
    // created normalized outputs so results at 25C == 1
    //    const slopesNormalized = [];
    const meanVoltage = MathMore.MathMEAN(voltageData[z]);
    const ResultAtMeanVoltage =
      //      regResults.slopes[7] * meanVoltage ** 7 +
      //      regResults.slopes[6] * meanVoltage ** 6 +
      //      regResults.slopes[5] * meanVoltage ** 5 +
      //      regResults.slopes[4] * meanVoltage ** 4 +
      //      regResults.slopes[3] * meanVoltage ** 3 +
      regResults.slopes[2] * meanVoltage ** 2 +
      regResults.slopes[1] * meanVoltage +
      regResults.slopes[0];
    //    ui.info('25c', ResultAtMeanVoltage);
    slopes[z][0] = regResults.slopes[0];
    slopes[z][1] = regResults.slopes[1];
    slopes[z][2] = regResults.slopes[2];
    //    slopes[z][3] = regResults.slopes[3];
    //    slopes[z][4] = regResults.slopes[4];
    //    slopes[z][5] = regResults.slopes[5];
    //    slopes[z][6] = regResults.slopes[6];
    //    slopes[z][7] = regResults.slopes[7];
    slopesNormalized[z][0] = regResults.slopes[0] / ResultAtMeanVoltage;
    slopesNormalized[z][1] = regResults.slopes[1] / ResultAtMeanVoltage;
    slopesNormalized[z][2] = regResults.slopes[2] / ResultAtMeanVoltage;
    //    slopesNormalized[z][3] = regResults.slopes[3] / ResultAtMeanVoltage;
    //    slopesNormalized[z][4] = regResults.slopes[4] / ResultAtMeanVoltage;
    //    slopesNormalized[z][5] = regResults.slopes[5] / ResultAtMeanVoltage;
    //    slopesNormalized[z][6] = regResults.slopes[6] / ResultAtMeanVoltage;
    //    slopesNormalized[z][7] = regResults.slopes[7] / ResultAtMeanVoltage;
    // sanity check --> this should always be == 1
    //    const ResultAtMeanVoltage = slopesNormalized[4] * normalTemp ** 4 + slopesNormalized[3] * normalTemp ** 3 + slopesNormalized[2] * normalTemp ** 2 + slopesNormalized[1] * normalTemp + slopesNormalized[0];
    // save results to csv and display
    regFit.push(MathMore.MathROUND(regResults.rsquared, 3));
    app.csvExport(
      'heatcal_raw_' + wavelengths[z],
      `${regResults.slopes[0]},${regResults.slopes[1]},${regResults.slopes[2]}`,
      //      `${regResults.slopes[0]},${regResults.slopes[1]},${regResults.slopes[2]},${regResults.slopes[3]},${regResults.slopes[4]},${regResults.slopes[5]},${regResults.slopes[6]},${regResults.slopes[7]}`,
    );
    app.csvExport(
      'heatcal_norm_' + wavelengths[z],
      `${slopesNormalized[z][0]},${slopesNormalized[z][1]},${slopesNormalized[z][2]}`,
      //      `${slopesNormalized[z][0]},${slopesNormalized[z][1]},${slopesNormalized[z][2]},${slopesNormalized[z][3]},${slopesNormalized[z][4]},${slopesNormalized[z][5]},${slopesNormalized[z][6]},${slopesNormalized[z][7]}`,
    );
    app.csvExport('heatcal_r2_' + wavelengths[z], regFit[z]);
    // loop through the number of measurements and apply the final corrected output and the fitted (predicted) output (should be linear now)
    for (let i = 1; i <= protocols; i++) {
      //      const heatMod = 1 / (voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][2] + voltageData[z][i] * slopesNormalized[z][1] + slopesNormalized[z][0]);
      const heatMod = 1 / (voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][2] + voltageData[z][i] * slopesNormalized[z][1] + slopesNormalized[z][0]);
      const fitLine = (voltageData[z][i] * voltageData[z][i] * slopes[z][2] + voltageData[z][i] * slopes[z][1] + slopes[z][0]);
      //      const heatMod = 1 / (voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][7] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][6] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][5] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][4] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][3] + voltageData[z][i] * voltageData[z][i] * slopesNormalized[z][2] + voltageData[z][i] * slopesNormalized[z][1] + slopesNormalized[z][0]);
      //      const fitLine = (voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopes[z][7] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopes[z][6] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopes[z][5] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopes[z][4] + voltageData[z][i] * voltageData[z][i] * voltageData[z][i] * slopes[z][3] + voltageData[z][i] * voltageData[z][i] * slopes[z][2] + voltageData[z][i] * slopes[z][1] + slopes[z][0]);
      //      const fitLine = voltageData[z][i] * voltageData[z][i] * slopes[z][2] + voltageData[z][i] * slopes[z][1] + slopes[z][0];
      correctedData[z].push(heatMod * reflectanceData[z][i]);
      predictedData[z].push(fitLine);
    }
    // prepare results to save to device, to be assembled and pushed in another measurement script.
    toDevice_heatcal1 += ledNumber[z] + '+' + MathMore.MathROUND(slopesNormalized[z][0], 12) + '+';
    toDevice_heatcal2 += ledNumber[z] + '+' + MathMore.MathROUND(slopesNormalized[z][1], 12) + '+';
    toDevice_heatcal3 += ledNumber[z] + '+' + MathMore.MathROUND(slopesNormalized[z][2] * 1000, 12) + '+';

    // Error check if r2 is too low + warn if so
    let poorFit = 0;
    if (regFit[z] < 0.93) {
      ui.warning(
        'Poor fit! r2 = ' + MathMore.MathROUND(regFit[z], 3) + ' at ' + wavelengths[z] + 'nm',
        'Evaluate data and consider repeating the calibration',
      );
      poorFit += 1;
    }
    // accumulate the range information to see if the fit is actually making the results more consistent across temperature
    const actualRange = MathMore.MathROUND(MathMore.MathMAX(reflectanceData[z]) - MathMore.MathMIN(reflectanceData[z]), 2);
    const actualError = MathMore.MathROUND(100 * actualRange / wavelengths_range[z], 2);
    const correctedRange = MathMore.MathROUND(MathMore.MathMAX(correctedData[z]) - MathMore.MathMIN(correctedData[z]), 2);
    const correctedError = MathMore.MathROUND(100 * correctedRange / wavelengths_range[z], 2);
    range += `${wavelengths[z]}: ${actualRange}, ${correctedRange}<br>`;
    theError += `${wavelengths[z]}: ${actualError}%, ${correctedError}%<br>`;
  }

  ui.info('Wavelength: Actual error, Corrected error', theError);
  ui.info('Wavelength: Actual range, Corrected range', range);

  // assemble the plots so they show all in one graph
  ui.multixy(
    voltageData,
    reflectanceData,
    'Fits for Wavelengths',
    wavelengths.map(v => 'Fit for ' + v),
  );

  // assemble the plots so they show all in one graph
  ui.multixy(
    voltageData,
    correctedData,
    'Predicted Fits for Wavelengths',
    wavelengths.map(v => 'Fit for ' + v),
  );

  // close out the text to be sent to device, export to CSV (important, also gets picked up by next measurement script), and display for santify check
  toDevice_heatcal1 += '-1+';
  toDevice_heatcal2 += '-1+';
  toDevice_heatcal3 += '-1+';
  app.csvExport('toDevice_heatcal1', toDevice_heatcal1);
  app.csvExport('toDevice_heatcal2', toDevice_heatcal2);
  app.csvExport('toDevice_heatcal3', toDevice_heatcal3); // number can be small so hard to save to EEPROM, this is divided by 1000 on the device in real time.
  app.setEnv('toDevice', toDevice_heatcal1 + toDevice_heatcal2 + toDevice_heatcal3);
  ui.warning(
    'Note on saved values',
    'Heat calibration values can be very small (e-10)... this is not feasible to send or save to eeprom due to limitations to c++ data types and serial communication.  So some values have been multiplied before saving to device.  If you are externally using these values, note that the saved values to the device are larger than the actual model coefficients.  Specifically, heatcal5 is multiplied by 1,000,000,000, heatcal4 is multiplied by 1,000,000, and heatcal3 is multiplied by 1,000 .  heatcal2 and heatcal1 are saved without modification.',
  );
  ui.info('Save to Device, coefficient 1', toDevice_heatcal1);
  ui.info('Save to Device, coefficient 2', toDevice_heatcal2);
  ui.info('Save to Device, coefficient 3', toDevice_heatcal3);
  //  ui.info('Save to Device coefficient 4', toDevice_heatcal4);
  //  ui.info('Save to Device coefficient 5', toDevice_heatcal5);

  // Now print all the graphs and data and stuff
  for (let z = 0; z < wavelengths.length; z++) {
    ui.info(`Data at ${wavelengths[z]}`, `voltage: ${voltageData[z]}<br>reflectance: ${reflectanceData[z]}<br>temperature: ${temperatureData[z]}`);
    ui.info(
      `Heat Calibration at ${wavelengths[z]}`,
      `r2: ${regFit[z]}<br>
      y int: ${slopesNormalized[z][0]}<br>
      slope1: ${slopesNormalized[z][1]}<br>
      slope2: ${slopesNormalized[z][2]}<br>
      y int raw: ${slopes[z][0]}<br>
      slope1 raw: ${slopes[z][1]}<br>
      slope2 raw: ${slopes[z][2]}`
    );
    /*
    ui.info(
      `Heat Calibration at ${wavelengths[z]}`,
      `r2: ${regFit[z]}<br>
      y int: ${slopesNormalized[z][0]}<br>
      slope1: ${slopesNormalized[z][1]}<br>
      slope2: ${slopesNormalized[z][2]}<br>
      slope3: ${slopesNormalized[z][3]}<br>
      slope4: ${slopesNormalized[z][4]}<br>
      slope5: ${slopesNormalized[z][5]}<br>
      slope6: ${slopesNormalized[z][6]}<br>
      slope7: ${slopesNormalized[z][7]}<br>
      y int raw: ${slopes[z][0]}<br>
      slope1 raw: ${slopes[z][1]}<br>
      slope2 raw: ${slopes[z][2]}<br>
      slope3 raw: ${slopes[z][3]}<br>
      slope4 raw: ${slopes[z][4]}<br>
      slope5 raw: ${slopes[z][5]}<br>
      slope6 raw: ${slopes[z][6]}<br>
      slope7 raw: ${slopes[z][7]}`
    );
    */
    // output individual plots for each wavelength
    ui.plotxy(voltageData[z], reflectanceData[z], 'Voltage / Reflectance at ' + wavelengths[z]);
    ui.plotxy(voltageData[z], predictedData[z], 'Voltage / Predicted Reflectance at ' + wavelengths[z]);
    ui.plotxy(voltageData[z], correctedData[z], 'Voltage / Corrected Reflectance at ' + wavelengths[z]);
    ui.plotxy(temperatureData[z], reflectanceData[z], 'Temp / Actual Reflectance at ' + wavelengths[z]);
  }
})();
// make sure this is at the very end!
app.save();

/*
    // run the model
    const regResults = MathMore.MathPOLYREG(regData[z], 1);
    // created normalized outputs so results at 25C == 1
    const slopesNormalized = [];
    const resultAtTwentyFiveC =
      //      regResults.slopes[4] * normalTemp ** 4 +
      //      regResults.slopes[3] * normalTemp ** 3 +
      //      regResults.slopes[2] * normalTemp ** 2 +
      regResults.slopes[1] * normalTemp +
      regResults.slopes[0];
    for (let i = 0; i < regResults.slopes.length; i++) {
      slopesNormalized[i] = regResults.slopes[i] / resultAtTwentyFiveC;
    }
    // sanity check --> this should always be == 1
    //    const normalizedAtTwentyFiveC = slopesNormalized[4] * normalTemp ** 4 + slopesNormalized[3] * normalTemp ** 3 + slopesNormalized[2] * normalTemp ** 2 + slopesNormalized[1] * normalTemp + slopesNormalized[0];
    // save results to csv and display
    regFit.push(regResults.rsquared);
    app.csvExport('heatcal_raw_' + wavelengths[z], regResults.slopes);
    app.csvExport('heatcal_norm_' + wavelengths[z], slopesNormalized);
    app.csvExport('heatcal_r2_' + wavelengths[z], regFit[z]);
    // Error check if r2 is too low

    let poorFit = 0;
    if (regFit[z] < 0.9) {
      ui.warning(
        'Poor fit! r2 = ' + MathMore.MathROUND(regFit[z], 3) + ' at ' + wavelengths[z] + 'nm',
        'Evaluate data and consider repeating the calibration',
      );
      poorFit += 1;
    }
    app.csvExport('poorFit', poorFit);
    ui.info(
      'Heat Calibration at ' + wavelengths[z],
      'r2: ' +
      regFit[z] +
      '<br>' +
      slopesNormalized[0] +
      '<br>' +
      slopesNormalized[1]
      //    '<br>' +
      //    slopesNormalized[2]
      //      '<br>' +
      //      slopesNormalized[3] +
      //      '<br>' +
      //      slopesNormalized[4],
    );
    // prepare results to save to device, to be assembled and pushed in another measurement script.
    toDevice_heatcal1 += z + 1 + '+' + slopesNormalized[0] + '+';
    toDevice_heatcal2 += z + 1 + '+' + slopesNormalized[1] + '+';
    //  toDevice_heatcal3 += z + 1 + '+' + slopesNormalized[2] * 1000 + '+';
    //    toDevice_heatcal4 += z + 1 + '+' + slopesNormalized[3] * 1000000 + '+';
    //    toDevice_heatcal5 += z + 1 + '+' + slopesNormalized[4] * 1000000000 + '+';
    // output individual plots for each wavelength
    ui.plotxy(voltageData[z], reflectanceData[z], 'Fit for ' + wavelengths[z]);
  }
  // assemble the plots so they show all in one graph
  ui.multixy(
    voltageData,
    reflectanceData,
    'Fits for Wavelengths',
    wavelengths.map(v => 'Fit for ' + v),
  );
  // close out the text to be sent to device, and display for santify check
  toDevice_heatcal1 += '-1+';
  toDevice_heatcal2 += '-1+';
  //  toDevice_heatcal3 += '-1+';
  //  toDevice_heatcal4 += '-1+';
  //  toDevice_heatcal5 += '-1+';
  ui.warning('Note on saved values', 'Heat calibration values can be very small (e-10)... this is not feasible to send or save to eeprom due to limitations to c++ data types and serial communication.  So some values have been multiplied before saving to device.  If you are externally using these values, note that the saved values to the device are larger than the actual model coefficients.  Specifically, heatcal5 is multiplied by 1,000,000,000, heatcal4 is multiplied by 1,000,000, and heatcal3 is multiplied by 1,000 .  heatcal2 and heatcal1 are saved without modification.');
  ui.info('Save to Device, coefficient 1', toDevice_heatcal1);
  ui.info('Save to Device, coefficient 2', toDevice_heatcal2);
  //  ui.info('Save to Device coefficient 3', toDevice_heatcal3);
  //  ui.info('Save to Device coefficient 4', toDevice_heatcal4);
  //  ui.info('Save to Device coefficient 5', toDevice_heatcal5);
*/