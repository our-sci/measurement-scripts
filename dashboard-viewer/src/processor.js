/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor, window */

// ///////////////////////////////////////////////////////
// Import libraries and get the 'result' information from sensor.js
import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
import wavelengthProcessor from './lib/process-wavelengths';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  window.location.href = `https://app.our-sci.net/#/dashboard/by-dashboard-id/rfc-milk-taste?sample_id=${
    result.sample_id
  }&metaInstanceID=${result.metaInstanceID}`;
})();
// final save of all app.csvExport calls -- make sure this is always at the very end!
app.save();
