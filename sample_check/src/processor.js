/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/*
import {
  sprintf
} from 'sprintf-js';
import _ from 'lodash';
*/

import {
  app,
} from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {

  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  console.log(result);

  // Show errors for things that require user intervention (multiple entries in the intake survey, multiple returned ID matches in collection survey)
  if (result.more_than_one && result.more_than_one.length) {
    app.error();
    ui.error('Multiple Surveys', `IDs ${result.more_than_one} have had more than one 2019 Sample Collection Survey submitted.  Please go to app.our-sci.net, search for the 2019 Sample Collection Survey, and identify which of the multiple entries are correct.  Archive (select box next to entry + hit archive, requires admin access) the incorrect entry, and re-run this script.`);
  }
  if (result.duplicates && result.duplicates.length) {
    app.error();
    ui.error('Duplicate Entries', `You entered IDs ${result.duplicates} twice... are you sure there are two of the same IDs in this sample packet?  Check the IDs you entered for errors and re-run this script.`);
  }

  let text = `Entered: &nbsp;&nbsp;${result.surveys_entered} of 6`;
  text += '<br>Found: &nbsp;&nbsp;&nbsp;&nbsp;';
  result.surveys_found.forEach((a) => {
    text += `${a},`;
  });
  text += '<br>Survey: &nbsp;&nbsp;&nbsp;&nbsp;';
  result.surveys_found_name.forEach((a) => {
    text += `${a},`;
  });
  text += '<br>Source: &nbsp;&nbsp;&nbsp;&nbsp;';
  result.surveys_found_source.forEach((a) => {
    text += `${a},`;
  });
  text += '<br>Volunteer: &nbsp;&nbsp;&nbsp;&nbsp;';
  result.surveys_found_volunteer_type.forEach((a) => {
    text += `${a},`;
  });
  text += '<br>Not Found: ';
  result.surveys_missing.forEach((a) => {
    text += `<span style=background-color:orange>${a}</span>,`;
  });
  text += '<br><br>Duplicates Found: &nbsp;&nbsp;';
  result.more_than_one.forEach((a) => {
    text += `<span style=background-color:red>${a}</span>,`;
  });
  text += '<br>Duplicates Entered: ';
  result.duplicates.forEach((a) => {
    text += `<span style=background-color:red>${a}</span>,`;
  });

  ui.info('IDs checked in Collection Survey', text);
  if (result.surveys_missing && result.surveys_missing.length) {
    ui.info('Some IDs not found', 'Some IDs were not found among the submitted Sample Collection surveys.  You may send the Data Partner an email in the next screen - click next to send');
  }

  app.csvExport('missing', result.surveys_missing.toString());
  app.csvExport('multiple', result.more_than_one.toString());

  app.save();
})();