/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';
// import serial from './lib/serial';
// import app from './lib/app';
// import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import * as utils from './lib/utils'; // use: await sleep(1000);
// import soilgrid from './lib/soilgrid';
// import weather from './lib/weather';
// import oursci from './lib/oursci';

import {
  app,
  oursci,
} from '@oursci/scripts'; // expose this to android for calling onDataAvailable
//  math

// export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

result.error = {}; // put error information here.
const requiredAnswersText = [
  'shipper_id',
  'sample_1',
  'sample_2',
  'sample_3',
  'sample_4',
  'sample_5',
  'sample_6',
];
// const requiredAnswersNumber = [
// ];

const survey_name = {
  slug: '2019-sample-collection-survey',
  short: 'original',
};
const survey2_name = {
  slug: '2019_non-farm-sample-collection-form-rfc-data-partners',
  short: 'non-farm',
};

(async () => {
  // First - go get collection survey that we're checking our sample IDs against
  const survey = await oursci(survey_name.slug);
  const survey2 = await oursci(survey2_name.slug);
  //  console.log(survey);

  // pull in text and number answers for all our questions
  requiredAnswersText.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        result.error[a] = 'missing';
      }
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  // requiredAnswersNumber.forEach((a) => {
  //   let v;
  //   if (typeof app.getAnswer(a) !== 'undefined') {
  //     v = app.getAnswer(a);
  //     if (app.getAnswer(a) === '') {
  //       result.error[a] = 'missing';
  //     } else if (isNaN(app.getAnswer(a))) {
  //       result.error[a] = 'not a number';
  //     }
  //   } else {
  //     v = 'undefined';
  //     result.error[a] = 'undefined';
  //   }
  //   console.log(v);
  //   result[a] = v;
  // });

  // Make variables to store what we find when we check the collection survey
  const surveys_all = []; // everything that's not empty
  result.surveys_found = []; // yep - we found exactly 1 collection survey with this ID
  result.surveys_found_source = []; // display where the sample came from (farmers market, store, etc.)
  result.surveys_found_volunteer_type = []; // farm or data partner?
  result.surveys_found_name = []; // name of survey
  result.surveys_missing = []; // nope - found no collection survey with this ID
  result.more_than_one = []; // oh no!  found more than one survey with this ID
  result.duplicates = []; // hey, you put in duplicate IDs... which you shouldn't do

  // Loop through the three sample_id questions on the collection survey to find the ID
  let count;
  let ids_entered = 0;

  console.log(survey.partner_type);

  console.log(Object.keys(result.error));
  for (let i = 1; i < 7; i++) {
    count = 0;
    if (!Object.keys(result.error).includes('sample_'.concat(i))) { // First make sure the ID answer isn't empty
      const id = result['sample_'.concat(i)];
      ids_entered++; // count # of ID's since now we know this isn't a missing value
      surveys_all.push(id);
      if (survey.sample_id.includes(id) || survey2.sample_id.includes(id)) {
        let index = survey.sample_id.indexOf(id);
        result.surveys_found.push(id);
        if (survey.sample_id.includes(id)) {
          result.surveys_found_source.push(survey.sample_source[index]);
          result.surveys_found_volunteer_type.push(survey.volunteer_type[index].slice(0, 4));
          result.surveys_found_name.push(survey_name.short);
        } else if (survey2.sample_id.includes(id)) {
          index = survey2.sample_id.indexOf(id);
          result.surveys_found_source.push(survey2.sample_source[index]);
          result.surveys_found_name.push(survey2_name.short);
        }
        count++;
      }
      if (survey.sample_id_2.includes(id) || survey2.sample_id_2.includes(id)) {
        let index = survey.sample_id_2.indexOf(id);
        result.surveys_found.push(id);
        if (survey.sample_id_2.includes(id)) {
          result.surveys_found_source.push(survey.sample_source[index]);
          result.surveys_found_volunteer_type.push(survey.volunteer_type[index].slice(0, 4));
          result.surveys_found_name.push(survey_name.short);
        } else if (survey2.sample_id_2.includes(id)) {
          index = survey2.sample_id_2.indexOf(id);
          result.surveys_found_source.push(survey2.sample_source[index]);
          result.surveys_found_name.push(survey2_name.short);
        }
        count++;
      }
      if (survey.sample_id_3.includes(id) || survey2.sample_id_3.includes(id)) {
        let index = survey.sample_id_3.indexOf(id);
        result.surveys_found.push(id);
        if (survey.sample_id_3.includes(id)) {
          result.surveys_found_source.push(survey.sample_source[index]);
          result.surveys_found_volunteer_type.push(survey.volunteer_type[index].slice(0, 4));
          result.surveys_found_name.push(survey_name.short);
        } else if (survey2.sample_id_3.includes(id)) {
          index = survey2.sample_id_3.indexOf(id);
          result.surveys_found_source.push(survey2.sample_source[index]);
          result.surveys_found_name.push(survey2_name.short);
        }
        count++;
      }
      if (count === 0) {
        result.surveys_missing.push(id);
      }
      if (count > 1) {
        result.more_than_one.push(id);
      }
    }
    //    console.log(count);
  }

  // Now lets check that the user didn't enter duplicate IDs
  function duplicates(array) {
    const sorted = array.slice().sort();
    const duplicates_array = [];
    for (let i = 0; i < sorted.length - 1; i++) {
      if (sorted[i + 1] === sorted[i]) {
        duplicates_array.push(sorted[i]);
      }
    }
    console.log(duplicates_array);
    return duplicates_array;
  }
  result.duplicates = duplicates(surveys_all);

  // Show errors for things that require user intervention (multiple entries in the intake survey, multiple returned ID matches in collection survey)
  if (result.more_than_one && result.more_than_one.length) {
    app.error();
  }
  if (result.duplicates && result.duplicates.length) {
    app.error();
  }


  result.surveys_entered = ids_entered; // save number of IDs entered

  console.log(result);
  app.result(result);
})();