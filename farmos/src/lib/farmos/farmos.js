import moment from 'moment';
import axios from 'axios';
import farmosApi from '../farmosapi/index';
import * as templates from './templates';

async function getTillageCsv() {
  const p = encodeURIComponent('resources/tillage.csv');
  const url = `https://gitlab.com/api/v4/projects/11427416/repository/files/${p}/raw?ref=master`;
  const answer = await axios(url);
  const csv = answer.data;
  const lines = csv.split('\n');
  return lines
    .map((l) => {
      const splitted = l.split('\t');
      if (splitted.length !== 3) {
        console.error('line malformated');
        console.error(l);
        return null;
      }
      return splitted;
    })
    .filter(args => args !== null);
}

const machines = {
  materials: 'farm_materials',
  crops: 'farm_crops',
  units: 'farm_quantity_units',
  soild: 'farm_soil_names',
  log: 'farm_log_categories',
};

async function createTerm(api, token, vocab, termsResponse, machineName, name) {
  if (!Object.values(machines).includes(machineName)) {
    throw Error(`Unable to find vocabulary for ${machineName}`);
  }

  const v = vocab.list.find(it => it.machine_name === machineName);
  if (!v) {
    throw Error(`unable to find vocabulary on farmos for ${machineName}`);
  }

  const materials = termsResponse.list.filter(it => it.vocabulary.id === v.vid);
  const material = materials.find(it => it.name === name);
  let id = material ? material.tid : '';
  if (!id) {
    try {
      const r = await api.term.send(name, machineName, token);
      ({ id } = r);
    } catch (error) {
      throw Error(`unable to create new term on farmos: ${name}, ${error.message}`);
    }
  }

  return id;
}

/**
 *
 * @param {string} url URL of the farmos instance
 * @param {string} username username of the farmos instance
 * @param {string} password password of the farmos instance
 * @param {string} token token provided, if null, authentication will take place in initialization
 * @param {object} config optional initial configuration for the session (fieldId and crop)
 */
export default async function farmos(
  url,
  username,
  password,
  providedToken,
  {
    configFieldId = null,
    configCrop = null,
    configInstanceId = null,
    configClearEntrieswithInstanceId = false,
  } = {},
) {
  /**
   * initialization
   */

  const farmConfig = {
    cropId: null /* not yet defined, need ID */,
    cropName: configCrop,
    fieldId: configFieldId,
    fieldName: null,
    instanceId: configInstanceId,
  };

  const farmapi = farmosApi(url, username, password);

  /**
   * here we are just checking if we can reach the farmos instance
   * and are actually expecting a 403
   */
  try {
    await axios(url);
  } catch (error) {
    if (error.response.status !== 403) {
      throw Error(`unable to reach farmos instance: ${url}, ${error.message}`);
    }
  }

  let token;
  try {
    token = providedToken || (await farmapi.authenticate());
  } catch (error) {
    throw Error(`unable to authenticate with farmos instance: ${url}, ${error.message}`);
  }

  let tillageTypes;

  try {
    tillageTypes = await getTillageCsv();
  } catch (error) {
    throw Error(`unable to fetch tillage types: ${error.message}`);
  }
  const vocab = await farmapi.vocabulary.get();

  let termsResponse;
  try {
    termsResponse = await farmapi.term.get({ token });
  } catch (error) {
    throw Error(`unable to fetch terms from farmos: ${error.message}`);
  }

  async function term(machineName, name) {
    return createTerm(farmapi, token, vocab, termsResponse, machineName, name);
  }

  function getTerms(vocabularyName) {
    return termsResponse.list.filter(t => t.vocabulary.id === vocabularyName);
  }

  const terms = {
    inches: await term(machines.units, 'inches'),
    pct: await term(machines.units, '%'),
    lbsPerAcre: await term(machines.units, 'lbs per acre'),
    tillage: await term(machines.log, 'Tillage'),
    gallonsPerAcre: await term(machines.unit, 'gal per acre'),
  };

  function assertConfiguration() {
    if (
      !farmConfig.cropId ||
      !farmConfig.fieldId ||
      !farmConfig.instanceId ||
      !farmConfig.cropName
    ) {
      throw Error(`error in configuration: ${JSON.stringify(farmConfig, null, 4)}`);
    }
  }

  async function configureFieldAndCrop(
    fieldId,
    crop,
    instanceId,
    clearEntrieswithInstanceId = false,
  ) {
    farmConfig.instanceId = instanceId;
    farmConfig.cropName = crop;

    const field = termsResponse.list
      .filter(t => t.area_type === 'field')
      .find(f => f.tid === fieldId);

    if (!field) {
      throw Error(`unable to find field on farmos instance with id: ${fieldId}`);
    }

    farmConfig.fieldId = fieldId;
    farmConfig.fieldName = field.name;
    farmConfig.cropId = await term(machines.crops, crop);
    assertConfiguration();
    if (!clearEntrieswithInstanceId) {
      return;
    }

    const assets = await farmapi.asset.get({
      type: 'planting',
    });
    console.log(assets);

    const target = assets.find((a) => {
      if (!a.data) {
        return false;
      }

      const d = JSON.parse(a.data);
      if (!d || !d.instanceId === instanceId) {
        return false;
      }

      return true;
    });

    if (!target) {
      return;
    }

    const logs = await farmapi.log.get();
    const related = logs.filter((l) => {
      if (!l.data) {
        return false;
      }

      const d = JSON.parse(l.data);
      if (!d || !d.instanceId === instanceId) {
        return false;
      }

      return true;
    });

    for (let i = 0; i < related.length; i++) {
      try {
        await farmapi.log.delete(related[i].id, token);
      } catch (error) {
        console.error(error);
      }
    }

    try {
      await farmapi.asset.delete(target.id, token);
    } catch (error) {
      console.error(error);
    }
  }

  if (configFieldId && configCrop && configInstanceId) {
    await configureFieldAndCrop(
      configFieldId,
      configCrop,
      configInstanceId,
      configClearEntrieswithInstanceId,
    );
  }

  return {
    configureFieldAndCrop,
    /**
     *
     * @param {string} type type of tillage
     * @param {string} date date in form 2019-11-24
     * @param {number} depth depth of tillage
     * @param {number} [assetId] id of planting asset, optional
     */
    async submitTillage(type, date, depth, assetId) {
      const timestamp = moment(date).unix();

      const tillageData = templates.tillage(
        timestamp,
        type,
        depth,
        farmConfig.fieldId,
        tillageTypes,
        terms.tillage,
        terms.inches,
        assetId,
        farmConfig.instanceId,
      );

      let tillage;

      try {
        tillage = await farmapi.log.send(tillageData, token);
      } catch (error) {
        throw Error(`unable to submit tillage to farmos: ${error.message}`);
      }

      return tillage;
    },
    /**
     *
     * @param {string} material mulch material
     * @param {string} date date in format 2019-11-26
     * @param {number} depth depth in inches
     * @param {string} method method for mulching
     * @param {string} source source for mulching
     * @param {number} [assetId] assetId of planting, optional
     */
    async submitMulch(material, date, depth, method, source, assetId) {
      const timestamp = moment(date).unix();
      const materialId = await term(machines.materials, material);

      const data = templates.input(
        timestamp,
        null,
        `Mulch for ${farmConfig.cropName} planted on ${date}`,
        materialId,
        farmConfig.fieldId,
        method,
        source,
        'mulching',
        assetId,
        [
          {
            measure: 'length',
            value: depth,
            unit: {
              id: terms.inches,
            },
            label: 'Depth',
          },
        ],
        {
          instanceId: farmConfig.instanceId,
        },
      );
      const log = await farmapi.log.send(data, token);
      return log;
    },

    /**
     * @param {string} date date in format 2019-12-25
     */
    async submitPlanting(plantingDate) {
      const plantingData = templates.planting(
        `${moment(plantingDate).format('YYYY')} ${farmConfig.fieldName} ${farmConfig.cropName}`,
        farmConfig.cropName,
        farmConfig.cropId,
        farmConfig.instanceId,
      );

      let planting;
      try {
        console.log(plantingData);
        planting = await farmapi.asset.send(plantingData, '', token);
      } catch (error) {
        throw Error(`unable to create planting on farmos: ${error.message}`);
      }

      const assetId = planting.id;
      if (!assetId) {
        throw Error(`did not receive asset id from farmos: ${JSON.stringify(planting, null, 4)}`);
      }

      return assetId;
    },
    async submitSeeding(timestamp, assetId, movement) {
      const seedingData = templates.seeding(
        farmConfig.cropName,
        assetId,
        movement ? farmConfig.fieldId : null,
        timestamp,
        farmConfig.instanceId,
      );

      let seeding;
      console.log(seedingData);
      try {
        seeding = await farmapi.log.send(seedingData, token);
      } catch (error) {
        throw Error(`unable to create movement on farmos: ${error.message}`);
      }
      return seeding;
    },
    async submitIrrigationSource(irrigation) {},
    async submitIrrigation(rate, type, date) {},
    async submitLime(limeRate, date, assetId) {
      const materialId = await term(machines.materials, 'lime');
      const timestamp = moment(date).unix();
      const limeData = templates.input(
        timestamp,
        null,
        `Lime for ${farmConfig.cropName} planted on ${date}`,
        materialId,
        farmConfig.fieldId,
        null,
        null,
        'lime',
        assetId,
        [
          {
            measure: 'ratio',
            value: limeRate,
            unit: {
              id: terms.lbsPerAcre,
            },
            label: 'Rate',
          },
        ],
        {
          instanceId: farmConfig.instanceId,
        },
      );
      console.log(limeData);
      const log = await farmapi.log.send(limeData, token);
      return log;
    },

    async submitAmendment(name, assetId, timestamp, method, nutrients, n, p, k, nutrientsTrace) {
      const quanitities = [];
      if (n) {
        quanitities.push({
          measure: 'ratio',
          value: n,
          unit: {
            id: terms.lbsPerAcre,
          },
          label: 'N',
        });
      }

      if (p) {
        quanitities.push({
          measure: 'ratio',
          value: p,
          unit: {
            id: terms.lbsPerAcre,
          },
          label: 'P',
        });
      }

      if (k) {
        quanitities.push({
          measure: 'ratio',
          value: k,
          unit: {
            id: terms.lbsPerAcre,
          },
          label: 'K',
        });
      }

      const materialId = await term(machines.materials, 'nutrients_furtilizer');

      const inputData = templates.input(
        timestamp,
        null,
        `${name} for ${farmConfig.cropName}, ${nutrients}`,
        materialId,
        farmConfig.fieldId,
        method,
        null,
        null,
        assetId,
        quanitities,
        {
          instanceId: farmConfig.instanceId,
          name,
          nutrients,
          nutrientsTrace,
        },
      );

      let r;
      try {
        r = await farmapi.log.send(inputData, token);
      } catch (error) {
        throw Error(`cannot send log for amendment ${name}, ${error.message}`);
      }
      return r;
    },
    async submitWeeklyAmendment(name, assetId, timestamp, nutrients, n, p, k, nutrientsTrace) {},
    async submitHistory(use, cashCrop1, cashCrop2, coverCrop, date) {},
    async submitTransplanting(
      pottingSoilBrand,
      seedlingTrayType,
      cellNumber,
      lighting,
      climate,
      seedlingTreatment,
      treatmentDate,
      treatmentName,
      timestamp,
      assetId,
    ) {
      if (pottingSoilBrand) {
        const materialId = await term(machines.materials, 'potting soil');
        const soilInputData = templates.input(
          timestamp,
          null,
          `Potting Soil ${pottingSoilBrand} on ${moment.unix(timestamp).format('YYYY-MM-DD')} for ${
            farmConfig.cropName
          }`,
          materialId,
          null,
          null,
          pottingSoilBrand,
          '',
          assetId,
          null,
          {
            instanceId: farmConfig.instanceId,
          },
        );
        await farmapi.log.send(soilInputData, token);
      }

      if (seedlingTreatment) {
        const seedlingTimestamp = moment(treatmentDate).unix();
        const materialId = await term(machines.materials, 'seedling treatment');
        const seedlingInputData = templates.input(
          seedlingTimestamp,
          null,
          `Seedling Treatment: (${seedlingTreatment}) on ${treatmentDate} for ${
            farmConfig.cropName
          }`,
          materialId,
          null,
          null,
          treatmentName,
          null,
          assetId,
          null,
          {
            instanceId: farmConfig.instanceId,
            seedlingTrayType,
            cellNumber,
            lighting,
            climate,
          },
        );
        await farmapi.log.send(seedlingInputData, token);
      }

      const transplantingData = templates.transplanting(
        farmConfig.fieldId,
        `Transplanting for ${farmConfig.cropName}`,
        assetId,
        farmConfig.instanceId,
        timestamp,
      );

      let transplanting;
      try {
        transplanting = await farmapi.log.send(transplantingData, token);
      } catch (error) {
        throw Error(`unable to run transplanting on farmos ${error.message}`);
      }
      return transplanting;
    },
    async submitManagementPractices(managementPractices) {},
    async submitWeedControl(type, herbicideType, herbicideRate, date) {},
    async submitPestDiseaseScouting(pressure, date) {},
    async submitPestDiseaseControl(
      configuration,
      reason,
      type,
      rate,
      controlled1,
      controlled2,
      controlled3,
      date,
    ) {},
  };
}
