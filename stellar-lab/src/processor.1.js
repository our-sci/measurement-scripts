/* global processor */

import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const result = (() => {
  if (typeof processor === 'undefined') {
    return require('../data/result.json');
  }

  return result.parse(processor.getResult());
})();

const json = result.sample[0];
const data = result.sample[0].data_raw;

if (typeof json.object_type === 'undefined') {
  result.object_type = 'cuvette'; // if no cuvette given
  const warningText =
    '"object_type" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
  ui.warning(warningText);
  app.csvExport('warning', warningText);
}
if (typeof result.device_name === 'undefined') {
  const warningText =
    '"device_name" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
  ui.warning(warningText);
  app.csvExport('warning', warningText);
}
if (result.device_name.toString() !== 'Reflectometer') {
  const warningText = 'This script was intended to be used on the reflectometer!';
  ui.warning(warningText);
  app.csvExport('warning', warningText);
}
if (typeof result.device_version === 'undefined') {
  const warningText =
    '"device_version" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
  ui.warning(warningText);
  app.csvExport('warning', warningText);
}
if (typeof result.device_id === 'undefined') {
  const warningText =
    '"device_id" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
  ui.warning(warningText);
  app.csvExport('warning', warningText);
}
if (typeof result.device_firmware === 'undefined') {
  const warningText =
    '"device_firmware" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
  ui.warning(warningText);
  app.csvExport('warning', warningText);
}
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// Determine if this is a calibration measurement or not.
// create toDevice string which will contain the calibration data to be sent back to the device if its a calibration measurement
let calibration = 0;
let toDevice = '';

if (json.calibration.toString() === 'shiney') {
  calibration = 1;
  toDevice = 'set_user_defined+';
  ui.info('calibration: shiney');
} else if (json.calibration.toString() === 'black') {
  calibration = 2;
  toDevice = 'set_user_defined+';
  ui.info('calibration: black');
}
// ///////////////////////////////////////////////////////

let objectType = 1;
// ///////////////////////////////////////////////////////
// now determine what type of object it is
// flat or solid object
if (json.object_type.toString() === 'object') {
  objectType = 1;
} else if (json.object_type.toString() === 'cuvette') {
  // bulk solid or liquid in the cuvette
  objectType = 2;
} else if (json.object_type.toString() === 'droplet') {
  // droplet of water
  objectType = 3;
}
// TEST
ui.info('object_type', json.object_type);
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// Set the conditions of the measurement - which lights, # pulses, # of pulses to ignore due to heating. etc.
const wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];
const pulses = 60;
const pulseDistance = 1.5;
const partPulses = 2 / 3;
// ///////////////////////////////////////////////////////

let maxReflectanceHigh = [];
let maxReflectanceLow = [];
let maxReflectance = [];

try {
  if (calibration === 0) {
    // get calibration information (min and max reflectance) from device

    if (json.object_type.toString() === 'object' && calibration === 0) {
      maxReflectanceHigh = [
        json.recall['userdef[0]'],
        json.recall['userdef[1]'],
        json.recall['userdef[2]'],
        json.recall['userdef[3]'],
        json.recall['userdef[4]'],
        json.recall['userdef[5]'],
        json.recall['userdef[6]'],
        json.recall['userdef[7]'],
        json.recall['userdef[8]'],
        json.recall['userdef[9]'],
      ];
      maxReflectanceLow = [
        json.recall['userdef[10]'],
        json.recall['userdef[11]'],
        json.recall['userdef[12]'],
        json.recall['userdef[13]'],
        json.recall['userdef[14]'],
        json.recall['userdef[15]'],
        json.recall['userdef[16]'],
        json.recall['userdef[17]'],
        json.recall['userdef[18]'],
        json.recall['userdef[19]'],
      ];
      maxReflectance = [];
    } else if (json.object_type.toString() === 'cuvette' && calibration === 0) {
      maxReflectanceHigh = [
        json.recall['userdef[20]'],
        json.recall['userdef[21]'],
        json.recall['userdef[22]'],
        json.recall['userdef[23]'],
        json.recall['userdef[24]'],
        json.recall['userdef[25]'],
        json.recall['userdef[26]'],
        json.recall['userdef[27]'],
        json.recall['userdef[28]'],
        json.recall['userdef[29]'],
      ];
      maxReflectanceLow = [
        json.recall['userdef[30]'],
        json.recall['userdef[31]'],
        json.recall['userdef[32]'],
        json.recall['userdef[33]'],
        json.recall['userdef[34]'],
        json.recall['userdef[35]'],
        json.recall['userdef[36]'],
        json.recall['userdef[37]'],
        json.recall['userdef[38]'],
        json.recall['userdef[39]'],
      ];
      maxReflectance = [];
    } else if (json.object_type.toString() === 'droplet' && calibration === 0) {
      maxReflectanceHigh = [
        json.recall['userdef[40]'],
        json.recall['userdef[41]'],
        json.recall['userdef[42]'],
        json.recall['userdef[43]'],
        json.recall['userdef[44]'],
        json.recall['userdef[45]'],
        json.recall['userdef[46]'],
        json.recall['userdef[47]'],
        json.recall['userdef[48]'],
        json.recall['userdef[49]'],
      ];
      maxReflectanceLow = [
        json.recall['userdef[50]'],
        json.recall['userdef[51]'],
        json.recall['userdef[52]'],
        json.recall['userdef[53]'],
        json.recall['userdef[54]'],
        json.recall['userdef[55]'],
        json.recall['userdef[56]'],
        json.recall['userdef[57]'],
        json.recall['userdef[58]'],
        json.recall['userdef[59]'],
      ];
      maxReflectance = [];
    }
    if (calibration === 0) {
      for (let j = 0; j < 10; j += 1) {
        maxReflectance[j] = maxReflectanceHigh[j] - maxReflectanceLow[j];
      }
    }
    // TEST
    //     info("maxReflectanceHigh: " + maxReflectanceHigh.toString())
    //     info("maxReflectanceLow: " + maxReflectanceLow.toString())
    //     info("maxReflectance: " + maxReflectance.toString())
    // TEST
    // ///////////////////////////////////////////////////////
  }
} catch (error) {
  console.error('error with getting calibration values');
}
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// get the sample values from the raw trace, put them into an array
const sampleValuesRaw = [];
for (let j = 0; j < wavelengths.length; j++) {
  sampleValuesRaw[j] = data.slice(pulses * j, pulses * (j + 1));
}
// TEST
// info("sampleValuesRaw: " + sampleValuesRaw[0].toString())
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// Then, we choose the last few pulses to use to avoid the heating effect
for (let j = 0; j < wavelengths.length; j++) {
  sampleValuesRaw[j] = sampleValuesRaw[j].slice(pulses * partPulses, pulses);
}
// TEST
// info("sampleValuesRaw: " + json.stringify(sampleValuesRaw));
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// And finally, we're going to straighten out the pulses to reduce our standard deviation using a linear regression and correction
// first we need a time array to plug into the regression formula (the x of y = mx + b)
const timeArray = [];
for (let z = 0; z < pulses - pulses * partPulses; z++) {
  timeArray[z] = z * pulseDistance;
}
// Then we need to create the new letiable to store the flattened values
const sample_values_flat = [];
for (let j = 0; j < wavelengths.length; j++) {
  const tmp_array = [];
  sample_values_flat[j] = tmp_array;
}

// now we can do the straightening via regression + correction
for (let j = 0; j < wavelengths.length; j++) {
  const reg = MathMore.MathLINREG(timeArray, sampleValuesRaw[j]);
  // what is the center point of rotation for the line (halfway through the array) - that's the value from which we will adjust other values
  const centerPoint = reg.m * (pulses - pulses * partPulses) / 2 + reg.b;
  for (let i = 0; i < sampleValuesRaw[j].length; i++) {
    const adjustment = centerPoint - (reg.m * timeArray[i] + reg.b);
    sample_values_flat[j][i] = sampleValuesRaw[j][i] + adjustment;
    // TEST //    output["adjustment_"+wavelengths[j]+"_"+i] = adjustment;
  }
}
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// now convert the data into a percentage (0 - 100) based on the min and max reflectance saved in the device.  (if it's a calibration, don't do that!)
const sample_values_perc = [];
for (let j = 0; j < wavelengths.length; j++) {
  const tmp_array = [];
  sample_values_perc[j] = tmp_array;
}
for (let j = 0; j < wavelengths.length; j++) {
  for (let i = 0; i < sample_values_flat[j].length; i++) {
    if (calibration > 0) {
      sample_values_perc[j][i] = sample_values_flat[j][i];
    } else {
      sample_values_perc[j][i] =
        100 *
        (sample_values_flat[j][i] - maxReflectanceLow[j]) /
        (maxReflectanceHigh[j] - maxReflectanceLow[j]);
    }
  }
}
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// TEST // Compare flattened, unflattened, and percentage values
// info("sample_values_flat: " + json.stringify(sample_values_flat[0]));
// info("sampleValuesRaw: " + json.stringify(sampleValuesRaw[0]));
// info("sample_values_perc: " + json.stringify(sample_values_perc[0]));
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// Now we can pull the median, standard deviation, spad, and bits from these adjusted + corrected values
// Note - to calculate bits, we need to convert the median value back up to a 16 bit value to calculate bits, so there's some math to convert it back to a raw value there
const median = [];
const median_raw = [];
const spad = [];
const absorbance = [];
const stdev = [];
const stdev_raw = [];
const three_stdev = [];
const three_stdev_raw = [];
const bits = [];
const bits_actual = [];
for (let j = 0; j < wavelengths.length; j++) {
  median[j] = MathMore.MathMEDIAN(sample_values_perc[j]);
  median_raw[j] = MathMore.MathMEDIAN(sample_values_flat[j]);
  absorbance[j] = -1 * Math.log(median[j] / 100);
  stdev[j] = MathMore.MathSTDEV(sample_values_perc[j]);
  stdev_raw[j] = MathMore.MathSTDEV(sample_values_flat[j]);
  three_stdev[j] = 3 * stdev[j];
  three_stdev_raw[j] = 3 * stdev_raw[j];
  bits[j] = 15 - MathMore.MathLOG(stdev_raw[j] * 2) / MathMore.MathLOG(2);
  if (calibration === 0) {
    bits_actual[j] =
      15 - MathMore.MathLOG(65536 / maxReflectance[j] * stdev_raw[j] * 2) / MathMore.MathLOG(2);
  }
}
for (let j = 0; j < wavelengths.length; j++) {
  spad[j] = 100 * Math.log(median[9] / median[j]); // because we've already normalized values from 0 (black) to 100 (shiney), the max value is always 100 so just divide by 100
}
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// Time to check for errors and send warnings if values are too close to max 64435 or zero
for (let i = 0; i < median.length; i++) {
  if (median_raw[i] < 100) {
    const error_text = `error: LED number ${i} at wavelength ${
      wavelengths[i]
    }is causing a very low response ( ${
      median[i]
    } ) from the detector.  Something is probably wrong with the protocol or the device`;
    error(error_text);
    app.csvExport('error', error_text);
  }
  if (median_raw[i] > 64434) {
    const error_text = `error: LED number ${i} at wavelength ${
      wavelengths[i]
    }is causing a very high response ( ${
      median[i]
    } ) from the detector.  Something is wrong with the protocol or the device`;
    error(error_text);
    app.csvExport('error', error_text);
  }
  if (bits_actual[i] < 7) {
    const error_text = `warning: LED number ${i} at wavelength ${
      wavelengths[i]
    }is very noisy, with actual bits less than 8 ( ${
      bits[i]
    } ).  Stabilize the sample, check for sources of noise, or check the equipment for problems.`;
    warning(error_text);
    app.csvExport('warning', error_text);
  }
}

// ///////////////////////////////////////////////////////
// Now we can optionally print graphs...
// for all of the final average values at each wavelength...
const all_outputs = [
  median,
  median_raw,
  absorbance,
  stdev,
  stdev_raw,
  three_stdev,
  three_stdev_raw,
  bits,
  bits_actual,
];
const all_outputs_names = [
  'median',
  'median_raw',
  'absorbance',
  'stdev',
  'stdev_raw',
  'three_stdev',
  'three_stdev_raw',
  'bits',
  'bits_actual',
];
const all_outputs_cal = [median_raw, stdev_raw, three_stdev_raw, bits];
const all_outputs_cal_names = ['median_raw', 'stdev_raw', 'three_stdev_raw', 'bits'];

// ///////////////////////////////////////////////////////
// save values to CSV
const medianList = [];
if (calibration === 0) {
  for (let i = 0; i < wavelengths.length; i++) {
    app.csvExport(`median_${i}`, median[i]);
    medianList.push(MathMore.MathROUND(median[i], 3));
  }
  for (let i = 0; i < wavelengths.length; i++) {
    app.csvExport(`median_raw_${i}`, median_raw[i]);
  }
}
ui.info('median', medianList.toString());
if (calibration === 0) {
  for (let i = 0; i < wavelengths.length; i++) {
    app.csvExport(`absorbance_${i}`, absorbance[i]);
  }
}
if (calibration === 0) {
  for (let i = 0; i < wavelengths.length; i++) {
    app.csvExport(`stdev_${i}`, stdev[i]);
    app.csvExport(`reflectance_range_${i}`, maxReflectance[i]);
    app.csvExport(`bits_actual_${i}`, bits_actual[i]);
  }
}
/*
if (calibration == 0) {
  for (let i = 0; i < wavelengths.length; i++) {
              info("median_" + wavelengths[i] + ": " + median[i]);
//                app.csvExport("median_" + i, median[i]);
              info("absorbance_" + wavelengths[i] + ": " + absorbance[i]);
//                app.csvExport("absorbance_" + i, absorbance[i]);
      //        info("stdev_" + wavelengths[i] + ": " + stdev[i]);
//                app.csvExport("stdev_" + i, stdev[i]);
              info("raw_range_" + wavelengths[i] + ": " + maxReflectance[i]);
//                app.csvExport("stdev_" + i, maxReflectance[i]);
      //        info("bits_actual_" + wavelengths[i] + ": " + bits_actual[i]);
//                app.csvExport("bits_actual_" + i, bits_actual[i])
          info("median_raw_" + wavelengths[i] + ": " + median_raw[i]);
      //    info("stdev_raw_" + wavelengths[i] + ": " + stdev_raw[i]);
      //    info("bits_" + wavelengths[i] + ": " + bits[i]);
  }
}
*/

//    info("raw_range: " + raw_range.toString());
//    info("median: " + median.toString());
//    info("median_raw: " + median_raw.toString());
//    info("stdev: " + stdev.toString());
//    info("stdev_raw: " + stdev_raw.toString());
//    info("bits: " + bits.toString());
//    info("bits_actual: " + bits_actual.toString());

// ///////////////////////////////////////////////////////
// TEST
// Output a single set of values for a light
/*
let test_light = 2;
info("raw_range_"+test_light + ": " + MathMore.MathROUND(maxReflectance[test_light],2));
info("median_"+test_light + ": " + MathMore.MathROUND(median[test_light],2));
info("median_raw_"+test_light + ": " + MathMore.MathROUND(median_raw[test_light],2));
info("stdev_"+test_light + ": " + MathMore.MathROUND(stdev[test_light],2));
info("stdev_raw_"+test_light + ": " + MathMore.MathROUND(stdev_raw[test_light],2));
info("bits_"+test_light + ": " + MathMore.MathROUND(bits[test_light],2));
info("bits_actual_"+test_light + ": " + MathMore.MathROUND(bits_actual[test_light],2));
*/
// ///////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////
// If this is a calibration, then determine the calibration type and save in the appropriate place
if (calibration == 1 && object_type == 1) {
  for (let j = 0; j < wavelengths.length; j++) {
    toDevice += `${j}+${MathMore.MathROUND(median_raw[j], 2)}+`;
  }
  toDevice += '-1+';
} else if (calibration == 2 && object_type == 1) {
  for (let j = 0; j < wavelengths.length; j++) {
    toDevice += `${j + 10}+${MathMore.MathROUND(median_raw[j], 2)}+`;
  }
  toDevice += '-1+';
} else if (calibration == 1 && object_type == 2) {
  for (let j = 0; j < wavelengths.length; j++) {
    toDevice += `${j + 20}+${MathMore.MathROUND(median_raw[j], 2)}+`;
  }
  toDevice += '-1+';
} else if (calibration == 2 && object_type == 2) {
  for (let j = 0; j < wavelengths.length; j++) {
    toDevice += `${j + 30}+${MathMore.MathROUND(median_raw[j], 2)}+`;
  }
  toDevice += '-1+';
} else if (calibration == 1 && object_type == 3) {
  for (let j = 0; j < wavelengths.length; j++) {
    toDevice += `${j + 40}+${MathMore.MathROUND(median_raw[j], 2)}+`;
  }
  toDevice += '-1+';
} else if (calibration == 2 && object_type == 3) {
  for (let j = 0; j < wavelengths.length; j++) {
    toDevice += `${j + 50}+${MathMore.MathROUND(median_raw[j], 2)}+`;
  }
  toDevice += '-1+';
}

if (calibration != 0) {
  ui.info(`toDevice: ${toDevice}`);
  app.csvExport('toDevice', toDevice);
}

// ///////////////////////////////////////////////////////
// Output values to CSV for the full data_raw string and all ratios.
// info("data_raw: " + data.toString())
const ratios = [];
const counter = 0;
for (let i = 0; i < wavelengths.length; i++) {
  for (let j = i + 1; j < wavelengths.length; j++) {
    ratios.push(median[i] / median[j]);
    app.csvExport(`ratio_${wavelengths[i]}_${wavelengths[j]}`, median[i] / median[j]);
  }
}
// info("ratios: " + ratios.toString())
app.csvExport('ratios', ratios.toString());

// ///////////////////////////////////////////////////////
// graph the ratios of each median value to each other median value
if (calibration === 0) {
  ui.plot(
    {
      series: [ratios],
    },
    'Reflectance, Ratios',
  );
}

// ///////////////////////////////////////////////////////
// graph the raw trace for the entire measurement (detector response by time)...
const fullTime = [];

for (let i = 0; i < data.length; i++) {
  fullTime.push(i * pulseDistance);
}

ui.plot(
  {
    series: [fullTime.map((ft, idx) => ({ x: ft, y: data[idx] }))],
  },
  'Detector Response (Raw Counts 16bit vs time [ms])',
);

// ///////////////////////////////////////////////////////
// Finally, graph the transmittance values at each wavelength the raw trace for the entire measurement (detector response by time)...

ui.plot(
  {
    series: [
      wavelengths.map((wl, idx) => ({
        x: wl,
        y: calibration === 0 ? median[idx] : median_raw[idx],
      })),
    ],
  },
  'Detector Response (Raw Counts 16bit vs time [ms])',
);

// ///////////////////////////////////////////////////////
// Finally, graph the standard deviation at each wavelength the raw trace for the entire measurement (detector response by time)...

ui.plot(
  {
    series: [
      wavelengths.map((wl, idx) => ({
        x: wl,
        y: calibration === 0 ? three_stdev[idx] : three_stdev_raw[idx],
      })),
    ],
  },
  '3 Std Deviations by Wavelength',
);

app.save();
