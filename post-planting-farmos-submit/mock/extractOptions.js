/**
 * This script extracts the meta data (controls only) of an .odkbuild file
 * downloaded from odk build.
 *
 * name property may deviate from the actual implementation as it contains
 * also "hacked" data such as script id or farmos area / planting types.
 *
 * paste output into the "controls" section of the mocked metadata
 */
const fs = require('fs');

const json = fs.readFileSync('./odkbuild.json');
const controls = [];
const obj = JSON.parse(json);
obj.controls.forEach((c) => {
  if (!c.children) {
    const control = {
      name: `/data/${c.name}:label`,
      label: c.label['0'] ? c.label['0'] : '',
      hint: c.hint['0'] ? c.hint['0'] : '',
      readOnly: c.readOnly,
      required: c.required,
    };
    if (c.options) {
      control.options = [];
      c.options.forEach((o) => {
        control.options.push({ value: o.val, text: o.text['0'] });
      });
    }

    controls.push(control);
  } else {
    const parent = c.name;
    c.children.forEach((child) => {
      const control = {
        name: `/data/${parent}/${child.name}:label`,
        label: child.label['0'] ? child.label['0'] : '',
        hint: child.hint['0'] ? child.hint['0'] : '',
        readOnly: child.readOnly,
        required: child.required,
      };
      if (child.options) {
        control.options = [];
        child.options.forEach((o) => {
          control.options.push({ value: o.val, text: o.text['0'] });
        });
      }
      controls.push(control);
    });
  }
});

console.log(JSON.stringify(controls, null, 2));
