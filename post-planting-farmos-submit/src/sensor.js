/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * !!!IMPORTANT!!! for testing
 * Add crentials to mock/credentials.json (don't commit to repo)
 * format in mock/credentials.demo.json
 */
// https://test.farmos.net/admin/structure/taxonomy/farm_log_categories

import moment from 'moment';

import { app, formparser, serial } from '@oursci/scripts';
import farmos from '@oursci/farmos';
import errorCheck from './errorcheck';

import { survey as surveyHelper } from '../.oursci-surveys/survey';

export default serial; // expose this to android for calling onDataAvailable

// if in dev environment, take this survey result as example
const uuidSnippet = 'a24e5296-0517-42e4-b7e';
const form = 'debug-post-planting-form';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

const result = {};
result.log = [];
result.error = [];

export const survey = surveyHelper();
export function err(msg) {
  result.error.push(msg);
}

function appendResult(title, content) {
  result.log.push({
    title,
    content,
  });
}

function errorExit(error) {
  app.error();
  throw Error(error);
}

export const regex = {
  anyOptional: /^.*$/,
  any: /^.+$/,
  number: /^\d+$/,
  numberOptional: /^\d*$/,
  date: /^\d{4}-\d{2}-\d{2}$/,
  optionalDate: /^(\d{4}-\d{2}-\d{2})|()$/,
  month_year: /^\d{4}-\d{2}$/,
  field_id: /^.*\(server: (\d+), tid: (\d+)\)$/,
};

const matchAnswers = {
  field_id: regex.field_id,
  crop: regex.notEmpty,
  general_management: regex.anyOptional,
  irrigation_source: regex.anyOptional,
  land_prep_method: regex.anyOptional,
  'tillage/type_1': regex.anyOptional,
  'tillage/date_1': regex.dateOptional,
  'tillage/depth_1': regex.numberOptional,
  'tillage/type_2': regex.anyOptional,
  'tillage/date_2': regex.dateOptional,
  'tillage/depth_2': regex.numberOptional,
  'tillage/type_3': regex.anyOptional,
  'tillage/date_3': regex.dateOptional,
  'tillage/depth_3': regex.numberOptional,
  planting_date: regex.optionalDate,
  'transplanting/potting_soil_days': regex.numberOptional,
  'transplanting/potting_soil_brand': regex.anyOptional,
  'transplanting/seedling_tray_type': regex.anyOptional,
  'transplanting/cell_number': regex.anyOptional,
  'transplanting/lighting': regex.anyOptional,
  'transplanting/climate': regex.anyOptional,
  'transplanting/seedling_treatment': regex.anyOptional,
  'transplanting/seedling_treatment_other': regex.anyOptional,
  'transplanting/seedling_treatment_date': regex.dateOptional,
  'transplanting/seedling_treatment_name': regex.anyOptional,
  'lime/rate': regex.anyOptional,
  'lime/lime_date': regex.dateOptional,
  'amendments/name_1': regex.anyOptional,
  'amendments/amendment_date_1': regex.dateOptional,
  'amendments/method_1': regex.anyOptional,
  'amendments/nutrients_1': regex.anyOptional,
  'amendments/n_1': regex.numberOptional,
  'amendments/p_1': regex.numberOptional,
  'amendments/k_1': regex.numberOptional,
  'amendments/nutrients_trace_1': regex.anyOptional,
  'amendments/name_2': regex.anyOptional,
  'amendments/amendment_date_2': regex.dateOptional,
  'amendments/method_2': regex.anyOptional,
  'amendments/nutrients_2': regex.anyOptional,
  'amendments/n_2': regex.numberOptional,
  'amendments/p_2': regex.numberOptional,
  'amendments/k_2': regex.numberOptional,
  'amendments/nutrients_trace_2': regex.anyOptional,
  'history_1/use': regex.anyOptional,
  'history_1/cash_crop1': regex.anyOptional,
  'history_1/cash_crop2': regex.anyOptional,
  'history_1/cover_crop': regex.anyOptional,
  'history_1/pastured': regex.anyOptional,
  'history_1/other': regex.anyOptional,
  'history_2/use_2': regex.anyOptional,
  'history_2/cash_crop1_2': regex.anyOptiona,
  'history_2/cash_crop2_2': regex.anyOptional,
  'history_2/cover_crop_2': regex.anyOptional,
  'history_2/pastured_2': regex.anyOptional,
  'history_2/other_2': regex.anyOptional,
};

function verify() {
  const questions = Object.keys(matchAnswers);
  questions.forEach((q) => {
    const r = matchAnswers[q];
    const answer = app.getAnswer(q) || '';
    if (!answer.match(r)) {
      errorExit(`answer not in expected format: ${q} ${answer}, ${r}`);
    }
  });
}

(async () => {
  errorCheck();
  if (result.error && result.error.length > 0) {
    app.error();
    app.result(result);
    return;
  }

  let tillageAnswer;
  let amendmentAnswer;

  try {
    try {
      verify();
      tillageAnswer = formparser.tillageParser(app);
      amendmentAnswer = formparser.amendmentParser(app);
    } catch (error) {
      errorExit(`error parsing, ${error.message}`);
    }

    app.progress(10);
    const fieldAnswer = app.getAnswer('field_id');
    let cropId;
    let crop;
    cropId = app.getAnswer('crop');
    if (cropId && cropId.toLowerCase() !== 'other') {
      const cropControl = app.getMeta().controls.find(c => c.name === '/data/crop:label');
      if (!cropControl) {
        errorExit('unable to identify crop in survey');
      }

      const option = cropControl.options.find(o => o.value === cropId);
      if (!option) {
        errorExit(`unable to find crop ${cropId} in options`);
      }

      crop = option.text;
    } else {
      cropId = app.getAnswer('other_crop');
      crop = cropId;
    }

    if (!crop || !cropId) {
      errorExit('Crop not specified');
    }

    const [, serverId, fieldId] = regex.field_id.exec(fieldAnswer);
    console.log(`serverId: ${serverId}`);
    console.log(`fieldId: ${fieldId}`);

    const meta = app.getMeta();
    const { url, username, password, farmosCookie, farmosToken } = app.getCredentials(serverId);

    result.url = url;

    // appendResult('Authenticating', `Authenticating on farmos with url ${url}`);
    app.progress(20);

    const farm = await farmos(url, username, password, farmosToken, {
      configFieldId: fieldId,
      configCrop: crop,
      configInstanceId: meta.instanceId,
      configClearEntrieswithInstanceId: true,
    });

    app.progress(40);
    appendResult('Authentication', 'success');

    const date = survey.planting_date || survey.planting_date_grape;
    if (!date) {
      errorExit('No date supplied');
    }

    const plantingDate = moment(date);
    const assetId = await farm.submitPlanting(plantingDate.format('YYYY-MM-DD'));
    result.plantingId = assetId;
    let seeding;

    const transplantingAnswer = app.getAnswer('transplanting_question');
    if (transplantingAnswer && transplantingAnswer.toLowerCase() === 'yes') {
      const days = Number.parseFloat(app.getAnswer('transplanting/potting_soil_days'));
      const seeded = plantingDate.clone().subtract(days, 'day');

      seeding = await farm.submitSeeding(seeded.unix(), assetId, false);

      const transplanting = await farm.submitTransplanting(
        app.getAnswer('transplanting/potting_soil_brand'),
        app.getAnswer('transplanting/seedling_tray_type'),
        app.getAnswer('transplanting/cell_number'),
        app.getAnswer('transplanting/lighting'),
        app.getAnswer('transplanting/climate'),
        app.getAnswer('transplanting/seedling_treatment'),
        app.getAnswer('transplanting/seedling_treatment_date'),
        app.getAnswer('transplanting/seedling_treatment_name'),
        plantingDate.unix(),
        assetId,
      );
    } else {
      seeding = await farm.submitSeeding(plantingDate.unix(), assetId, true);
    }

    appendResult('Planting', `submitted planting to farmos for ${crop} with id ${assetId}`);
    app.progress(60);

    const otherPrep = app.getAnswer('land_prep_other');
    if (otherPrep) {
      const a = await farm.submitSimpleActivity(
        `Other land preparation: ${otherPrep}`,
        assetId,
        plantingDate.unix(),
      );
      appendResult(
        'Land Preparation',
        `submitted land preparation to farmos ${otherPrep}, with id ${a.id}`,
      );
    }

    for (let i = 0; i < tillageAnswer.length; i++) {
      const t = tillageAnswer[i];
      const tillage = await farm.submitTillage(t.type, t.date, t.depth, assetId);
      appendResult(`Tillage ${i + 1}`, `submitted tillage to farmos, with id ${tillage.id}`);
    }

    app.progress(65);

    const mulchAnswer = app.getAnswer('mulch_question');
    if (mulchAnswer && mulchAnswer.toLowerCase() === 'yes') {
      if (survey['mulch/mulch_type_1']) {
        if (!survey['mulch/mulch_type_1']) {
          errorExit('select mulch type 1');
        }
        if (!survey['mulch/mulch_date_1']) {
          errorExit('select mulch date 1');
        }

        let type = survey['mulch/mulch_type_1'];
        if (type === 'other') {
          type = survey['mulch/mulch_other_1'];
        } else {
          type = formparser.getItemLabels('mulch/mulch_type_1')[0];
        }

        const r = await farm.submitMulch(
          type,
          survey['mulch/mulch_date_1'],
          survey['mulch/mulch_depth_1'],
          null,
          null,
          assetId,
        );
        appendResult('Mulch', `submitted mulch to farmos, with id ${r.id}`);
      }
      if (survey['mulch/mulch_2'] === 'yes') {
        if (!survey['mulch/mulch_type_2']) {
          errorExit('select mulch type 2');
        }
        if (!survey['mulch/mulch_date_2']) {
          errorExit('select mulch date 2');
        }

        let type = survey['mulch/mulch_type_2'];
        if (type === 'other') {
          type = survey['mulch/mulch_other_2'];
        } else {
          type = formparser.getItemLabels('mulch/mulch_type_2')[0];
        }

        const r = await farm.submitMulch(
          type,
          survey['mulch/mulch_date_2'],
          survey['mulch/mulch_depth_2'],
          null,
          null,
          assetId,
        );
        appendResult('Mulch', `submitted mulch to farmos, with id ${r.id}`);
      }
    }
    app.progress(70);

    const lime = app.getAnswer('lime_question');
    if (lime && lime.toLowerCase() === 'yes') {
      const r = await farm.submitLime(
        app.getAnswer('lime/rate'),
        app.getAnswer('lime/lime_date'),
        assetId,
      );
    }

    app.progress(75);

    for (let i = 0; i < amendmentAnswer.length; i++) {
      const a = amendmentAnswer[i];
      let type = a.type;
      const amendmentControl = app
        .getMeta()
        .controls.find(c => c.name === '/data/amendments/amendment_type_1:label');

      if (amendmentControl) {
        const c = amendmentControl.options.find(o => o.value === type);
        type = c ? c.text : type;
      }

      const amendment = await farm.submitAmendment(
        a.name,
        assetId,
        moment(a.date).unix(),
        a.method,
        type,
        a.nutrients,
        a.n,
        a.p,
        a.k,
        a.trace,
      );
      appendResult(
        `Amendment ${i + 1}`,
        `submitted amendment (${a.name}) to farmos,  with id ${amendment.id}`,
      );
    }
    app.progress(95);
    // terms.tillage;

    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.result(result);
  }
})();
