/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { app } from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.log) {
    result.log.forEach((log) => {
      ui.info(log.title, log.content);
    });
  }

  if (result.error.length > 0) {
    result.error.forEach((msg) => {
      ui.error('Error', msg);
    });

    ui.error('Redo', 'Please fix the errors and press the Redo Button');
    app.csvExport('errors', JSON.stringify(result.error));
    app.save();
    return;
  }
  app.csvExport('planting', result.plantingId);
  app.csvExport('url', result.url);
  app.save();
})();
