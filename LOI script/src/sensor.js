/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import moment from 'moment';
import _ from 'lodash';
import mathjs from 'mathjs';

import serial from './lib/serial';
import app from './lib/app';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import * as utils from './lib/utils'; // use: await sleep(1000);
import soilgrid from './lib/soilgrid';
import weather from './lib/weather';
import oursci from './lib/oursci';

export default serial; // expose this to android for calling onDataAvailable

const res = {};

function errorExit(error) {
  app.result({
    error,
  });
}

const requiredAnswers = ['loi_crucible', 'loi_pre', 'loi_post'];

(async () => {
  const errors = {};
  /*
  const result = {};
  result.loi_pre = app.getAnswer(Number(loi_pre));
  result.loi_crucible = app.getAnswer(Number(loi_crucible));
  result.loi_post = app.getAnswer(Number(loi_post));
*/
  requiredAnswers.forEach((a) => {
    const v = app.getAnswer(a);
    if (!v) {
      errorExit(`Answer for ${a} missing`);
    }
    res[a] = v;
  });

  app.result(res);
})();
