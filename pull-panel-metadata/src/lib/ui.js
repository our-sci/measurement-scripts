import 'chartist/dist/chartist.min.css';
import 'chartist-plugin-fill-donut';
import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/js/materialize';

import '../css/style.css';

const Chartist = require('chartist');
const $ = require('jquery');

const container = $('<div></div>');
container.attr('id', 'message-container');
container.prependTo($('body'));

const percentColors = [
  { pct: 0.0, color: { r: 0xff, g: 0x00, b: 0 } },
  { pct: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
  { pct: 1.0, color: { r: 0x00, g: 0xff, b: 0 } },
];

function getColorForPercentage(pct) {
  let i = 0;
  for (i = 1; i < percentColors.length - 1; i++) {
    if (pct < percentColors[i].pct) {
      break;
    }
  }
  const lower = percentColors[i - 1];
  const upper = percentColors[i];
  const range = upper.pct - lower.pct;
  const rangePct = (pct - lower.pct) / range;
  const pctLower = 1 - rangePct;
  const pctUpper = rangePct;
  const color = {
    r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
    g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
    b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper),
  };
  return `rgb(${[color.r, color.g, color.b].join(',')})`;
  // or output as hex if preferred
}

function message(title, text, color, icon) {
  const card = $(`<div style='vertical-align: top; margin: 16px; margin-bottom: 0; background-color: ${color};' class='card wide'><div>`);
  const inner = $("<div class='card-content white-text'></div>");
  card.append(inner);
  inner.append(`<span class='card-title' style='display: flex; '><i class='material-icons' style='font-size: 32px; margin-right: 12px;'>${icon}</i>${title}</span>`);
  inner.append(`<p>${text}</p>`);
  $('body').append(card);
}

module.exports.info = (title, text) => {
  message(title, text, '#03A9F4', 'info');
};

module.exports.warning = (title, text) => {
  message(title, text, '#FFC107', 'warning');
};

module.exports.error = (title, text) => {
  message(title, text, '#F44336', 'error');
};

let donutCount = 0;
let plotCount = 0;

function createCard(title, text, innerId) {
  const card = $("<div style='margin: 16px; margin-bottom: 0;' class='card primary-bg wide'><div>");
  const inner = $("<div class='card-content white-text'></div>");
  card.append(inner);
  inner.append(`<span class='card-title'>${title}</span>`);
  inner.append(`<p>${text}</p>`);
  inner.append(`<div id='${innerId}' class='donut'></div>`);
  inner.append("<div class='desc'></div>");
  return card;
}

module.exports.linspace = (start, stop, n) => {
  const arr = [];
  const increment = (stop - start) / n;

  for (let i = 0; i < n; i += 1) {
    arr.push(i * increment + start);
  }

  return arr;
};

module.exports.plot = (
  data,
  title,
  {
    min = null,
    max = null,
    axisX = {
      labelInterpolationFnc() {
        return null;
      },
    },
  } = {},
) => {
  const id = `plot_${plotCount}`;
  plotCount += 1;

  const card = createCard(title, '', id);
  $('body').append(card);

  $(`#${id}`).addClass('ct-chart ct-golden-section plot');

  Chartist.Line(`#${id}`, data, {
    axisX,
    low: min == null ? undefined : min,
    high: max == null ? undefined : max,
  });
};

module.exports.barchart = (
  data,
  title,
  {
    min = null,
    max = null,
    axisX = {
      labelInterpolationFnc() {
        return null;
      },
    },
  } = {},
) => {
  const id = `plot_${plotCount}`;
  plotCount += 1;

  const card = createCard(title, '', id);
  $('body').append(card);

  $(`#${id}`).addClass('bar-width ct-chart ct-golden-section plot');

  Chartist.Bar(`#${id}`, data, {
    seriesBarDistance: 20,
    axisX,
    low: min == null ? undefined : min,
    high: max == null ? undefined : max,
  });
};

module.exports.donut = (text, value, pct, desc, icon) => {
  const id = `donut_${donutCount}`;
  donutCount += 1;

  const card = createCard(text, '', id);
  $('body').append(card);
  $('.desc', card).text(desc);

  let actualPct = pct;

  const color = getColorForPercentage(actualPct);

  if (actualPct > 1) {
    actualPct = 1;
  }

  if (actualPct < 0) {
    actualPct = 0;
  }

  const x1 = 220 * actualPct;
  const x2 = 220 * (1 - actualPct);

  const chart = Chartist.Pie(
    `#${id}`,
    {
      series: [x1, x2],
      labels: ['', ''],
    },
    {
      donut: true,
      donutWidth: 20,
      startAngle: 210,
      total: 260,
      showLabel: false,
      plugins: [
        Chartist.plugins.fillDonut({
          items: [
            {
              content: `<i class="material-icons">${icon}</i>`,
              position: 'bottom',
              offsetY: 10,
              offsetX: -2,
            },
            {
              content: value,
            },
          ],
        }),
      ],
    },
  );

  chart.on('created', () => {
    console.log(`using color: ${color}`);
    $(`#${id} .ct-series-a>path`).css('stroke', color);
    // $(`#${id} .ct-series-b>path`).css('stroke', color);
  });
};

module.exports.button = (text, cb = () => {}) => {
  const button = $(`<a style="margin: 1.5em" class="waves-effect waves-light btn primary-bg wide">${text}</a>`);
  $('body').append(button);
  button.click(cb);
};

module.exports.chunkArray = (myArray, size) => {
  const target = myArray.slice(0);
  const results = [];

  while (target.length) {
    results.push(target.splice(0, size));
  }

  return results;
};
