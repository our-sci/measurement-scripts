/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import moment from 'moment';
import _ from 'lodash';
import mathjs from 'mathjs';

import serial from './lib/serial';
import app from './lib/app';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import * as utils from './lib/utils'; // use: await sleep(1000);
import soilgrid from './lib/soilgrid';
import weather from './lib/weather';
import oursci from './lib/oursci';

export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

// normal answers
result.location = app.getAnswer('location_group/Location');

// measurement scripts only

(async () => {
  /*
  requiredAnswers.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = JSON.parse(app.getAnswer(a));
    } else {
      v = 'undefined';
    }
    console.log(v);
    result[a] = v;
*/

  if (!result.location) {
    errors[result.location] = 'Location missing, cannot run APIs';
    result.errors = errors;
    app.result(result);
    return;
  }

  const [lat, lon] = result.location.split(';');
  if (!lat || !lon) {
    errors['format'] = `lat / lon not formated properly<br>${lat} / ${lon} `;
    result.errors = errors;
    app.result(result);
    return;
  }

  if (Number.isNaN(lat) || Number.isNaN(lon)) {
    errors['format'] = `lat / lon not formated properly<br>${lat} / ${lon} `;
    result.errors = errors;
    app.result(result);
    return;
  }

  try {
    result.soilGrid = await soilgrid(lat, lon);
  } catch (error) {
    errors['soilgrid'] = error;
    result.errors = errors;
    app.result(result);
    return;
  }

  console.log(result);
  app.result(result);
})();
