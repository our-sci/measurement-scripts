/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import {
  sleep
} from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  // ///////////////////////////////////////////////////////
  // Multiple measurements are saved from this single measurement script
  // Select a serial.write command below based on what you are updating to the Our Sci database.
  // Then copy paste the associated manifest (listed at the very bottom) and save it to the manifest.json file in src folder
  // Finally hit ctrl-b "compile and upload" and check the website app.our-sci.net to confirm.

  // standard measurement for user device on sample showing relative values (0 - 100)
  //  serial.write(JSON.stringify(sendDevice().standard));
  //  serial.write(JSON.stringify(sendDevice().standardBlank));
  //  serial.write(JSON.stringify(sendDevice().standardRaw));
  //  console.log(JSON.stringify(sendDevice().standardRawNoHeat));
  serial.write(JSON.stringify(sendDevice().standardRawNoHeat));

  // white and black calibrations
  // the lights are in numerical order, rather than wavelength order...
  //  serial.write(JSON.stringify(sendDevice().calibrateAllWhite));
  //  serial.write(JSON.stringify(sendDevice().calibrateAllBlack)); // FOR MASTER MASTER, UNCOMMENT SECTION FORCING 0 / 100 VALUES BELOW
  //  console.log(JSON.stringify(sendDevice().calibrateAllBlack));
  //  serial.write(JSON.stringify(sendDevice().calibrateAllBlank));
  //  serial.write(JSON.stringify(sendDevice().calibrateAllWhiteMaster));
  //  serial.write(JSON.stringify(sendDevice().calibrateAllBlackMaster));

  //  other legacy protocols
  //  serial.write(JSON.stringify(sendDevice().oldReflectometer2_flat));

  // ///////////////////////////////////////////////////////
  // Mark the progress of pulling in the data
  let count = 0;
  try {
    app.progress(0.1);
    const result = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1;
      app.progress(0.1 + count / 617.0 * 100.0 * 0.9);
    });
    app.progress(100);
    // if this is just a single measurement, then define meas as the measurement.  If there are multiple, note that they also need to be pulled out here.
    const meas = result.sample[0];
    /*
    console.log(result);
    console.log("and the measurement: ");
    console.log(meas);
    */

    // ///////////////////////////////////////////////////////
    // create a space in result.json to enter processed data
    meas.processed = {};

    // ///////////////////////////////////////////////////////
    // Determine what the wavelengths for each pulse set are and add it to the result.
    // This is passed to the script to process the results and to generate the averages at each pulse set.
    let wavelengths = [];
    if (
      meas.calibration === 1 ||
      meas.calibration === 2 ||
      meas.calibration === 3 ||
      meas.calibration === 4 ||
      meas.calibration === 5
    ) {
      wavelengths = [
        850,
        880,
        370,
        395,
        420,
        605,
        650,
        730,
        530,
        940,
        850,
        880,
        370,
        395,
        420,
        605,
        650,
        730,
        530,
        940,
      ];
    } else {
      // includes if it's undefined
      wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];
    }
    //    console.log(meas.wavelengths);

    // ///////////////////////////////////////////////////////
    // Process the results - clean up outliers, straighten due to heating, and produce medians, etc. for each wavelength and see if it's a calibration script
    meas.processed = wavelengthProcessor(meas, wavelengths);
    console.log(meas.processed);

    // ///////////////////////////////////////////////////////
    // If it's a calibration script, figure out which and save data back to device accordingly
    // calibration using white card (max signal)
    // Assemble the data to save to device and save back to device
    if (meas.calibration === 1) {
      let set_ir_high = 'set_ir_high+';
      for (let i = 0; i < 10; i++) {
        set_ir_high += i + 1 + '+';
        set_ir_high += meas.processed.median[i] + '+';
      }
      set_ir_high += -1 + '+';
      let set_vis_high = 'set_vis_high+';
      for (let i = 0; i < 10; i++) {
        set_vis_high += i + 1 + '+';
        set_vis_high += meas.processed.median[i + 10] + '+';
      }
      set_vis_high += -1 + '+';
      console.log(set_ir_high);
      console.log(set_vis_high);
      // Send the data to the device
      serial.write(set_ir_high);
      serial.write(set_vis_high);
    } else if (meas.calibration === 2) {
      // calibration using black card (min signal)
      // Assemble the data to save to device and save back to device
      let set_ir_low = 'set_ir_low+';
      for (let i = 0; i < 10; i++) {
        set_ir_low += i + 1 + '+';
        set_ir_low += meas.processed.median[i] + '+';
      }
      set_ir_low += -1 + '+';
      let set_vis_low = 'set_vis_low+';
      for (let i = 0; i < 10; i++) {
        set_vis_low += i + 1 + '+';
        set_vis_low += meas.processed.median[i + 10] + '+';
      }
      set_vis_low += -1 + '+';
      console.log(set_ir_low);
      console.log(set_vis_low);
      // Send the data to the device
      serial.write(set_ir_low);
      serial.write(set_vis_low);
      // Also identify the QR code which contains the card's master values, and save them to the device
      const qr = app.getAnswer('cardQr');
      if (qr) {
        serial.write(qr);
      }
    } else if (meas.calibration === 3) {
      // blank used for cuvettes
      // Assemble the data to save to device and save back to device
      let set_ir_blank = 'set_ir_blank+';
      for (let i = 0; i < 10; i++) {
        set_ir_blank += i + 1 + '+';
        set_ir_blank += meas.processed.median[i] + '+';
      }
      set_ir_blank += -1 + '+';
      let set_vis_blank = 'set_vis_blank+';
      for (let i = 0; i < 10; i++) {
        set_vis_blank += i + 1 + '+';
        set_vis_blank += meas.processed.median[i + 10] + '+';
      }
      set_vis_blank += -1 + '+';
      console.log(set_ir_blank);
      console.log(set_vis_blank);
      // Send the data to the device
      serial.write(set_ir_blank);
      serial.write(set_vis_blank);
    } else if (meas.calibration === 4 || meas.calibration === 5) {
      // master calibrations only for creating calibration cards
      // master for white card
      if (meas.calibration === 4) {
        let set_ir_high_master = 'set_ir_high_master+';
        for (let i = 0; i < 10; i++) {
          set_ir_high_master += i + 1 + '+';
          set_ir_high_master += MathMore.MathROUND(meas.processed.median[i], 3) + '+';
        }
        set_ir_high_master += -1 + '+';
        let set_vis_high_master = 'set_vis_high_master+';
        for (let i = 0; i < 10; i++) {
          set_vis_high_master += i + 1 + '+';
          set_vis_high_master += MathMore.MathROUND(meas.processed.median[i + 10], 3) + '+';
        }
        set_vis_high_master += -1 + '+';
        console.log(set_ir_high_master);
        console.log(set_vis_high_master);
        // Save data to result.processed so is displayed an can be printed as QR codes
        meas.processed.masterCard = set_ir_high_master + set_vis_high_master;
      } else if (meas.calibration === 5) {
        // master calibration using black card (min signal)
        let set_ir_low_master = 'set_ir_low_master+';
        for (let i = 0; i < 10; i++) {
          set_ir_low_master += i + 1 + '+';
          set_ir_low_master += MathMore.MathROUND(meas.processed.median[i], 3) + '+';
        }
        set_ir_low_master += -1 + '+';
        let set_vis_low_master = 'set_vis_low_master+';
        for (let i = 0; i < 10; i++) {
          set_vis_low_master += i + 1 + '+';
          set_vis_low_master += MathMore.MathROUND(meas.processed.median[i + 10], 3) + '+';
        }
        set_vis_low_master += -1 + '+';
        console.log(set_ir_low_master);
        console.log(set_vis_low_master);
        // Save data to result.processed so is displayed an can be printed as QR codes
        meas.processed.masterCard = set_ir_low_master + set_vis_low_master;
      }
    }

    /*
    // FOR BLACK MASTER MASTER ONLY
    // manually set device to master (0 - 100 for low - high)
    // set_ir_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+
    const set_ir_high_master =
      'set_ir_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+';
    const set_ir_low_master = 'set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+';
    const set_vis_high_master =
      'set_vis_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+';
    const set_vis_low_master = 'set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+';
    serial.write(set_ir_high_master);
    serial.write(set_ir_low_master);
    serial.write(set_vis_high_master);
    serial.write(set_vis_low_master);
        */

    // Display memory in console to confirm saved values
    // currently causing issues with the black calibration, for now just leaving memory blank.
    result.memory = {};
    /*
    if (meas.calibration === 1 || meas.calibration === 2 || meas.calibration === 3) {
      sleep(100);
      serial.write('print_memory+');
      try {
        const device_result = await serial.readStreamingJson('[*]', () => {});
        result.memory = device_result;
      } catch (error) {
        console.log(error);
      }
    }
    */

    // check to see if this is a chlorophyll measurement
    const is_chlorophyll = app.getAnswer('is_chlorophyll');
    if (is_chlorophyll === '1') {
      result.is_chlorophyll = 1;
      console.log('Found is_chlorophyll');
    } else {
      console.log('did not find is_chlorophyll');
    }

    /*
    try {
      const verify = await serial.readStreamingJson('[*]', () => {});
             console.log('verify:');
             console.log(JSON.stringify(verify, null, 2));
    } catch (error) {
             console.log(error);
    }
    */
    // DEBUG console.log(`res ${JSON.stringify(res)}`);
    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();

/*
"id": "standard-2-1",
"name": "Reflectometer standard 2.1",
"description": "Standard reflectometer measurement for field and lab use",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
  */

/*
"id": "standardBlank-2-1",
"name": "Reflectometer blank2-1",
"description": "Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRaw-2-1",
"name": "Reflectometer raw 2.1",
"description": "outputs raw data only, use standard reflectometer measurement for normal use",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRawNoHeat-2-1",
"name": "Reflectometer raw no heat calibration 2.1",
"description": "outputs raw data only, use standard reflectometer measurement for normal use, heat calibration not applied",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhite-2-1",
"name": "Reflectometer calibration white 2.1 ",
"description": "Calibrate a device to user calibration card, using the white square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlack-2-1",
"name": "Reflectometer calibration black 2.1",
"description": "Calibrate a device to user calibration card, using the black square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlank-2-1",
"name": "Reflectometer calibration blank 2.1",
"description": "Create and save a blank value, to be applied as an offset when using the standard_blank measurement on samples.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhiteMaster-2-1",
"name": "Reflectometer calibration white master 2.1",
"description": "Calibrate the master device to master calibration card, using the white square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMaster-2-1",
"name": "Reflectometer calibration black master 2.1",
"description": "Calibrate the master device to master calibration card, using the black square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMasterMaster-2-1",
"name": "Reflectometer Master device - Maste card calibration 2.1",
"description": "ONLY used to calibrate the master card.  Force saves 0 / 100 values to the device.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standard-firmware-1-18",
"name": "Reflectometer measurement firmware 1.18",
"description": "only for beta devices on old firmware 1.18",
"version": "2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/














/*
"id": "standard-chlorophyll-2-1",
"name": "Chlorophyll standard 2.1",
"description": "Chlorophyll content using reflectometer for field and lab use",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
  */