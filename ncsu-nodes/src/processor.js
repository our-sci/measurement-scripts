var $ = require("jquery");
sprintf = require("sprintf-js").sprintf;
app = require("./lib/app.js").App;
ui = require("./lib/ui.js");
math = require("./lib/math.js");
moment = require("moment");

require("materialize-css/dist/css/materialize.min.css");
require("chartist/dist/chartist.min.css");
require("chartist-plugin-fill-donut");
require("materialize-css");
require("./css/style.css");

// the result is loaded from the file system, allowing to debug actual data from the device
if (typeof result === "undefined") {
  try {
    result = require("../data/result.json");
  } catch (err) {
    result = {
      data: [1, 3, 2, 1, 3, 2, 2, 2, 5]
    };
  }
}

sheet = "";

for (node in result) {

  if(node == "raw"){
    continue;
  }

  if (!result.hasOwnProperty(node)) {
    continue;
  }

  var temp = [];
  var timestamp = [];
  var vwc = [];

  // From the Serial data with data we pulled in sensor.js and saved in raw.txt, we're now creating arrays of sensor data in JSON.
  // The resulting JSON is saved in 'result.json' in the 'data' folder
var all_things = ""

  for (var i = 0; i < result[node].length; i++) {
    timestamp.push(result[node][i][0]);
    temp.push(result[node][i][2]);
    vwc.push(result[node][i][4]);
    all_things += result[node][i][0] + ","

    sheet += node + ", " + result[node][i][0] + ", " + result[node][i][2] + ", " + result[node][i][4] + "<br>";
  }

  sheet += "<br>"

  // Now we plot the results to the display.
  // First, plot the date on the X, and a list of parameters on the Y.
  ui.plotDate(timestamp, [temp, vwc], -4, 40, "Node " + node + "<br>Residue Temp (C) vs<br>Volumetric Water Content", ["Temparature in C"]);
  //ui.plotDate(timestamp, vwc, -1, 50, "Node " + node + "<br>VWC (%)");

  var thresh = 2;
  var count = 0;

  // for each node, check if vwc below thresh two times in a row.
  // and print message.  This could be any warning we want to set.
  for (var i = 0; i < vwc.length; i++) {
    if (vwc[i] <= thresh) {
      count++;
      if (count > 2) {
        ui.error(
          "Warning, VWC value VERY VERY low",
          "Volumetric Water Content below 3% two measurements in a row<br>" +
          "Date " +
          moment(timestamp[i]).format("dddd, MMMM Do YYYY, h:mm:ss a")
        );
        break;
      }
    } else {
      count = 0;
    }
  }
}


if (result.hasOwnProperty("raw")) {
  raw = result["raw"];
  ui.info("raw input", "<textarea rows='20'>"+raw+"</textarea>");
}

// We can display the csv values on the phone.  
// However, we can also expert the values as app.csvExport()
// Anything pused to app.csvExport() is saved to the database
// So the database can contain more or less data than is displayed to the user, in addition to the raw data.  
ui.info("csv values", sheet);

app.save();
