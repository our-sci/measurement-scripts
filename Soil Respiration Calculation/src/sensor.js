/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';

// import serial from './lib/serial';
import app from './lib/app';
// import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import * as utils from './lib/utils'; // use: await sleep(1000);
// import sheets from './lib/sheets';
import oursci from './lib/oursci';

// export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

result.error = {}; // put error information here.
result.error.id = [];
// we're going to pull in each answer, but also put them into an array to pass to processor via results.json
result.lab_id = [];
result.respiration_baseline = []; // co2 measurements at start
result.respiration_24 = []; // co2 measurements at end
result.t1 = []; // time at start
result.t2 = []; // time at end

const items = 37; // total samples per tray + 1
const split = 38; // this tray isn't split (like LOI) so make split > items... then it will never get triggered

const requiredMeasurements = [];
const requiredAnswersText = [];
const requiredAnswersNumber = [];
for (let i = 0; i < items; i++) {
  let rack2 = ''; // first 18, there's no prefix
  if (i > split) rack2 = 'rack_2.'; // add prefix for last 18
  requiredMeasurements.push(`${rack2}respiration_24_${i}`);
  //  requiredMeasurements.push(`${rack2}respiration_baseline_${i}`);
  requiredAnswersText.push(`${rack2}lab_id_${i}`);
}
requiredAnswersText.push('tray_id');
requiredAnswersNumber.push('temp_average');
requiredAnswersNumber.push('temp_max');
requiredAnswersNumber.push('temp_min');

console.log(requiredAnswersText);
console.log(requiredAnswersNumber);


(async () => {

  requiredMeasurements.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined' && app.getAnswer(a) !== '') {
      v = JSON.parse(app.getAnswer(a));
    } else {
      v = 'undefined';
      //      result.error[a] = 'undefined';
    }
    if (a.includes('loi_post_ohaus')) {
      result.loi_post_ohaus.push(v);
    } else {
      result[a] = v;
    }
  });

  requiredAnswersNumber.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        //        result.error[a] = 'missing';
      } else if (isNaN(app.getAnswer(a))) {
        //        result.error[a] = 'not a number';
      }
    } else {
      v = 'undefined';
      //      result.error[a] = 'undefined';
    }
    if (a.includes('loi_post_manual')) {
      result.loi_post_manual.push(v);
    } else {
      result[a] = v;
    }
  });

  requiredAnswersText.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        //        result.error[a] = 'missing';
      }
    } else {
      v = 'undefined';
      //      result.error[a] = 'undefined';
    }
    if (a.includes('lab_id')) {
      result.lab_id.push(v);
    } else {
      result[a] = v;
    }
  });

  // Now we need to go get the data from the survey '2020-rfc-baseline-respiration' and put them into arrays also...

  let data;
  try {
    // data = await oursci('2020-rfc-baseline-respiration');
    data = await oursci('2020-RFC-baseline-respiration-new-draft');
  } catch (error) {
    result.error.oursciFetch = `Unable to fetch oursci survey results: ${error}`;
    console.log(result.error.oursciFetch);
  }
  //  console.log(data);

  // filter the data to that index
  const trayData = [];
  data.forEach((row) => {
    if (row.tray_id === result.tray_id) trayData.push(row);
  });
  if (trayData.length > 1) {
    console.log(`multiple tray_ids found for ${result.tray_id}`);
    result.error.multiple_tray_ids = true;
  } else if (trayData.length === 0) {
    console.log(`no tray_id ${result.tray_id} found`);
    result.error.no_tray_id = true;
  }

  //  console.log(`tray_id: ${result.tray_id}, tray_id: ${data.tray_id[trayIndex]}, tray trayIndex: ${trayIndex}`);
  // now... we make an array for the pre and post measures based on the trayData...
  // pass undefined and identify those which do not have ID's
  // note we search the entire tray for the ID, not just the same position that the current ID is in.
  result.lab_id.forEach((id) => { // loop thru IDs in current surveys
    if (id === 'undefined') {
      result.respiration_baseline.push('undefined');
      result.respiration_24.push('undefined');
      result.t1.push('undefined');
      result.t2.push('undefined');
      result.error.id.push('undefined');
    } else { // if not undefined...
      let found = 0;
      const thisTray = trayData[0];
      Object.keys(thisTray).forEach((columnName) => { // check against the pre-survey data (trayData) for the same ID.  If there are multiple tray_id's, used the latest one [0]
        console.log(`looking: ${id} in ${columnName}, found: ${thisTray[columnName]}`);
        if (columnName.includes('lab_id_') && thisTray[columnName] === id) { // if the column name is a lab_id_XXX column and it's the id then...
          console.log(`found it: ${columnName}`);
          let position = columnName.split('_');
          position = position[position.length - 1];
          let rack2 = '';
          if (Number(position) > split) rack2 = 'rack_2.';
          result.respiration_baseline.push(thisTray[`${rack2}respiration_baseline_${position}`]);
          result.respiration_24.push(result[`${rack2}respiration_24_${position}`]);
          // check to see if the timestamps are there (ie the measurement exists)
          if (typeof thisTray[`${rack2}respiration_baseline_${position}`].meta !== 'undefined' &&
            typeof result[`${rack2}respiration_24_${position}`].meta !== 'undefined') {
            result.t1.push(Date.parse(thisTray[`${rack2}respiration_baseline_${position}`].meta.date));
            result.t2.push(Date.parse(result[`${rack2}respiration_24_${position}`].meta.date));
            result.error.id.push('');
          } else { // time stamps were missing... so add the error
            result.t1.push('measurement missing');
            result.t2.push('measurement missing');
            result.error.id.push('measurement missing');
          }
          found++;
        }
      });
      if (found === 0) {
        result.respiration_baseline.push('id not found');
        result.respiration_24.push('id not found');
        result.t1.push('id not found');
        result.t2.push('id not found');
        result.error.id.push('id not found');
      } else if (found > 1) {
        result.respiration_baseline.push('multiple matched ids');
        result.respiration_24.push('multiple matched ids');
        result.t1.push('multiple matched ids');
        result.t2.push('multiple matched ids');
        result.error.id.push('multiple matched ids');
      }
    }
  });

  app.result(result);
})();