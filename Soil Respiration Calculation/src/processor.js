/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

// import { sprintf } from 'sprintf-js';
// import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  // Find + display an errors from sensor
  Object.keys(result.error).forEach((error) => {
    if (error === 'id') { // these are the sample-by-sample errors
      result.error[error].forEach((sample, index) => {
        if (index !== 0 && sample !== '') { // skip index 0, otherwise if an error is marked, then display it
          ui.error(`id: ${result.lab_id[index]}, pos: ${index}`, `error: ${sample}`);
        }
      });
    } else { // all other errors
      ui.error('Something went wrong', error);
    }
  });

  const labId = [];
  const ugCgsoil = [];
  const co2increase = [];
  const co2post = [];
  const co2pre = [];
  const hoursdiff = [];

  // add 5g soil to 0.7 ml water
  //  1000ml Syringe setup 5 g only
  // ///////////////////////////////////////////////////////
  // Define the experimental parameters for the CO2 method
  const sample_time = 5; // time between samples in seconds
  const Headspace = 85;
  const Temp = 298;
  const Rconstant = 82.05;
  const conversionfactor = 5000; // conversion factor is assuming 5 g soil (soil g x 1000)
  const conversionfactor2 = 12000; //  converts to weight of C only and not of CO2
  // ///////////////////////////////////////////////////////
  //  */

  result.lab_id.forEach((id, index) => {
    if (index !== 0 && id !== 'undefined' && result.error.id[index] === '') { // skip position 0... otherwise if this is a good measurement... calculate + display...

      const hours = MathMore.MathROUND((result.t2[index] - result.t1[index]) / 1000 / 60 / 60);
      const co2final = Number(result.respiration_24[index].data.max_co2);
      const co2baseline = Number(result.respiration_baseline[index].data.max_co2);
      const co2inc = MathMore.MathROUND((co2final - co2baseline) * (24 / hours));
      labId.push(id);
      hoursdiff.push(hours);
      co2increase.push(co2inc);
      co2pre.push(co2baseline);
      co2post.push(co2final);
      ugCgsoil.push(MathMore.MathROUND(((co2inc * Headspace) / conversionfactor / (Temp * Rconstant)) * conversionfactor2));

      // ui.info('Hours difference', (result.t2 - result.t1) / 1000 / 60 / 60);
      /*
  if (co2max === 10000) {
    // if the device is working correctly, it'll max out at exactly 10,000.  If there is a noisy spike that needs to be removed, it's often > 10,000
    const warningText = 'CO2 too high';
    ui.warning(
      warningText,
      'CO2 reading on the device maxed sensors range at 10,000 ppm.  Check the experimental setup for other sources of CO2, or if the soil is extremely active, consider changing the protocol to account for high CO2 activity.',
    );

    warnings.push(warningText);
    ui.info('CO2 increase', null);
    ui.info('ugC per g soil', null);
    app.csvExport('co2_increase', null);
    ui.csvExport('ugc_gsoil', null);
*/
      // } else {
      // ui.info('ugC per g soil', ugCgsoil[index]);
      // ui.info('CO2 increase', co2increase[index]);
      // ui.info('Max CO2', co2final);
      // ui.info('Min CO2', co2baseline);
      // ui.info('Hours difference', hoursdiff[index]);
      app.csvExport(`lab_id_${index}`, result.lab_id[index]);
      app.csvExport(`ugc_gsoil_${index}`, ugCgsoil[index]);
      app.csvExport(`co2_increase_${index}`, co2increase[index]);
      app.csvExport(`max_co2_${index}`, co2final);
      app.csvExport(`min_co2_${index}`, co2baseline);
      app.csvExport(`Hours_difference_${index}`, hoursdiff[index]);
    } else if (index !== 0) {
      ui.error(
        `id "${id}" ${result.error.id[index]}`,
        'There was a problem finding the ID from the previous survey.  Manually check and correct.',
      );
      labId.push(id);
      ugCgsoil.push(result.error.id[index]);
      co2increase.push(result.error.id[index]);
      co2pre.push(result.error.id[index]);
      co2post.push(result.error.id[index]);
      hoursdiff.push(result.error.id[index]);
    } else { // always push something!  If empty, push nothing then...
      labId.push(id);
      ugCgsoil.push('');
      co2increase.push('');
      co2pre.push('');
      co2post.push('');
      hoursdiff.push('');
    }
  });


  function spaces(text, len) {
    const extraSpaces = len - text.toString().trim().length;
    if (extraSpaces > 0) {
      for (let i = 0; i < extraSpaces; i++) {
        text += '&nbsp;';
      }
    }
    //    console.log(`|${text}|`);
    return text;
  }
  const len = 15; // length between rows

  let allResults = `<b>${spaces("Lab ID", len)} ${spaces("ugC/g soil", len)} ${spaces("co2 pre", len)} ${spaces("co2 post", len)} ${spaces("time (hrs)", len)}</b><br>`;
  // let ugCgsoilResults = '';
  // let co2increaseResults = '';
  // let co2preResults = '';
  // let co2postResults = '';
  // let hoursdiffResults = '';

  for (let i = 1; i < result.lab_id.length; i++) { // skip zero-ith object
    allResults += `${spaces(labId[i], len)} ${spaces(ugCgsoil[i], len)} ${spaces(co2pre[i], len)} ${spaces(co2post[i], len)} ${spaces(hoursdiff[i], len)}<br>`;
    // ugCgsoilResults += `${ugCgsoil[i]}<br>`;
    // co2increaseResults += `${co2increase[i]}<br>`;
    // co2preResults += `${co2pre[i]}<br>`;
    // co2postResults += `${co2post[i]}<br>`;
    // hoursdiffResults += `${hoursdiff[i]}<br>`;
  }

  ui.info("All Results", allResults);
  // ui.info('ugC per g', ugCgsoilResults);
  // ui.info('CO2 increase', co2increaseResults);
  // ui.info('CO2 pre', co2preResults);
  // ui.info('CO2 post', co2postResults);
  // ui.info('Total hours', hoursdiffResults);

  app.save();
})();