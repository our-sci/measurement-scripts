import tabletop from 'tabletop';

export default id =>
  new Promise((resolve, reject) => {
    const docsUrl = `https://docs.google.com/spreadsheet/pub?hl=en_US&hl=en_US&key=${id}&output=html`;

    try {
      tabletop.init({
        key: docsUrl,
        callback: (data, ta) => {
          resolve({ data, ta });
        },
      });
    } catch (error) {
      reject(error);
    }
  });
