/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

import moment from 'moment';

import app from './lib/app';
import serial from './lib/serial';
import sheets from './lib/sheets';

export default serial; // expose this tot android for calling onDataAvailable

const result = {};

(async () => {
  try {
    if (!app.hasQuestion('measurement_1') || !app.hasQuestion('measurement_1')) {
      throw new Error('questions measurement_1 and measurment_2 are missing');
    }

    if (!app.isAnswered('measurement_1') || !app.isAnswered('measurement_1')) {
      throw new Error('questions measurement_1 and measurment_2 not answered');
    }

    const m1 = JSON.parse(app.getAnswer('measurement_1'));
    const m2 = JSON.parse(app.getAnswer('measurement_2'));

    const t1 = Date.parse(m1.meta.date);
    const t2 = Date.parse(m2.meta.date);
    console.log(`t1 is: ${t1}\nt2 is: ${t2}`);
    result.t1 = t1;
    result.t2 = t2;
  } catch (e) {
    result.error = e;
    console.log(e);
  }

  app.result(result);
})();
